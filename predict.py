import os
import cv2
import time
import argparse
import matplotlib.pyplot as plt
from tqdm import tqdm
from datetime import timedelta

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' if __name__ == '__main__' else '0'
import tensorflow as tf

tf.config.optimizer.set_jit(False)

import logging
import logging.config

logging.config.fileConfig('logging.conf')

from utils.constants import IMAGE_EXTENSIONS
from utils.exceptions import InvalidImageFormatError
from data_pipeline.factory.toolset import get_dataset_size
from models.factory.component_serialization import load_component
from utils.image_processor import tensor2image, float2uint8, image_path_to_tensor


def parse_arguments():
    parser = argparse.ArgumentParser(
        description='Process configurations and execute operations'
    )
    parser.add_argument('-m', '--model_path', help='path to the model checkpoint folder')
    parser.add_argument(
        '-d',
        '--data_path',
        help='path to the data: '
        + 'it can be either an image or a folder containing images',
    )
    parser.add_argument(
        '-w',
        '--condition_weights',
        help='conditional weights '
        + 'for controlling the style (only for models with conditional input)',
        nargs='+',
        type=float,
        default=None,
    )
    parser.add_argument(
        '-s',
        '--saving_path',
        type=str,
        default=None,
        help='path to the location where the outputs will be stored',
    )
    parser.add_argument(
        '-v',
        '--visualize',
        action='store_true',
        default=False,
        help='display the results interactively using pyplot',
    )

    args = parser.parse_args()
    return args


def load_data(data_path):
    """
    Create a dataset of images names and their contents

    Args:
        data_path (str): path to an image or a folder containing images
    Return:
        dataset (tf.data.Dataset): zipped names and contents
    """

    def get_image_name(image_path):
        parts = tf.strings.split(image_path, os.path.sep)
        return parts[-1]

    if data_path.endswith(IMAGE_EXTENSIONS):
        image_paths = tf.data.Dataset.list_files(data_path)
    elif '.' not in os.path.basename(data_path):
        image_paths = tf.data.Dataset.list_files(
            [os.path.join(data_path, '*' + ext) for ext in IMAGE_EXTENSIONS],
            shuffle=False,
        )
    else:
        raise InvalidImageFormatError

    image_names = image_paths.map(get_image_name)
    image_contents = image_paths.map(image_path_to_tensor)
    return tf.data.Dataset.zip((image_names, image_contents))


def display_transformation(original, generated, image_name):
    """
    Display the transformation process done by the generator

    Args:
        original (tf.Tensor): original image
        generated (tf.Tensor): the result of passing the original image
                               through the generator
        image_name (str): the name of the image file
    """
    fig = plt.figure(image_name, figsize=(15, 7))
    ax_left = fig.add_subplot(
        1, 2, 1, xticklabels=[], yticklabels=[], xticks=[], yticks=[]
    )
    ax_right = fig.add_subplot(
        1, 2, 2, xticklabels=[], yticklabels=[], xticks=[], yticks=[]
    )
    ax_left.set_title('Original')
    ax_left.imshow(original)
    ax_right.set_title('Generated')
    ax_right.imshow(generated)
    plt.show()


def main(args):

    logger = logging.getLogger(__name__)

    logger.info('Loading the generator...')
    model = load_component(args.model_path)

    logger.info('Loading the data...')
    dataset = load_data(args.data_path).batch(1)
    dataset_size = get_dataset_size(dataset)

    if args.saving_path is not None:
        os.makedirs(args.saving_path, exist_ok=True)

    logger.info('Starting the prediction...')
    prediction_start = time.time()

    for image_name, image_tensor in tqdm(
        dataset, desc='Prediction Progress', total=dataset_size
    ):
        try:
            image_name = image_name[0].numpy().decode('ascii')

            if args.condition_weights is None:
                inputs = image_tensor
            else:
                condition_weights = tf.expand_dims(args.condition_weights, axis=0)
                inputs = dict(
                    input_image=image_tensor, condition_weights=condition_weights
                )

            image = tensor2image(image_tensor[0])
            prediction_tensor = model(inputs)
            prediction = tensor2image(prediction_tensor[0])

            if args.visualize:
                display_transformation(image, prediction, image_name)
            if args.saving_path is not None:
                prediction_uint8 = float2uint8(prediction.numpy())
                cv2.imwrite(
                    os.path.join(args.saving_path, image_name),
                    cv2.cvtColor(prediction_uint8, cv2.COLOR_BGR2RGB),
                )
        except Exception as e:
            logger.debug('Failed to predict on {}: {}'.format(image_name, e))

    total_prediction_time = time.time() - prediction_start
    logging.info(
        '\nTotal prediction time: {}'.format(
            str(timedelta(seconds=total_prediction_time)).split('.')[0]
        )
    )


if __name__ == '__main__':
    args = parse_arguments()
    main(args)
