from collections import namedtuple

ModuleTuple = namedtuple('ModuleTuple', 'entity_name module_path')


def get_custom_layer_paths_dict():
    """
    Maintain a lookup table of the paths to all custom layers as implemented
    so far in the application

    Return:
        (dict) maps unique custom layer names (str) to a tuple (ModuleTuple) that
        contains a path to the module and the name of the class
    """
    custom_layers_path = '.'.join(['models', 'components', 'auxiliaries', 'layers'])
    return dict(
        residual_block=ModuleTuple(
            entity_name='ResidualBlock', module_path=custom_layers_path
        ),
        dynamic_padding=ModuleTuple(
            entity_name='DynamicPadding', module_path=custom_layers_path
        ),
        dynamic_cropping=ModuleTuple(
            entity_name='DynamicCropping', module_path=custom_layers_path
        ),
        channel_normalization=ModuleTuple(
            entity_name='ChannelNormalization', module_path=custom_layers_path
        ),
        instance_normalization=ModuleTuple(
            entity_name='InstanceNormalization', module_path=custom_layers_path
        ),
        conditional_instance_normalization=ModuleTuple(
            entity_name='ConditionalInstanceNormalization', module_path=custom_layers_path
        ),
        local_normalization=ModuleTuple(
            entity_name='LocalNormalization', module_path=custom_layers_path
        ),
        depth_normalization=ModuleTuple(
            entity_name='DepthNormalization', module_path=custom_layers_path
        ),
        conditional_depth_normalization=ModuleTuple(
            entity_name='ConditionalDepthNormalization', module_path=custom_layers_path
        ),
        addons_instance_normalization=ModuleTuple(
            entity_name='InstanceNormalization',
            module_path='.'.join(['tensorflow_addons', 'layers']),
        ),
        spectral_norm=ModuleTuple(
            entity_name='SpectralNorm', module_path=custom_layers_path,
        ),
    )


def get_component_paths_dict():
    """
    Maintain a lookup table of the paths to components as implemented
    so far in the application

    Return:
        (dict) maps unique component names (str) to a tuple (ModuleTuple) that
        contains a path to the module and the name of the class
    """
    components_path = '.'.join(['models', 'components'])
    return dict(
        convolutional=ModuleTuple(
            entity_name='Convolutional',
            module_path='.'.join([components_path, 'convolutional']),
        ),
        convolutional_sn=ModuleTuple(
            entity_name='ConvolutionalSn',
            module_path='.'.join([components_path, 'convolutional_sn']),
        ),
        convolutional_in=ModuleTuple(
            entity_name='ConvolutionalIn',
            module_path='.'.join([components_path, 'convolutional_in']),
        ),
        convolutional_cin=ModuleTuple(
            entity_name='ConvolutionalCin',
            module_path='.'.join([components_path, 'convolutional_cin']),
        ),
        convolutional_cdn=ModuleTuple(
            entity_name='ConvolutionalCdn',
            module_path='.'.join([components_path, 'convolutional_cdn']),
        ),
        residual_encoder_decoder=ModuleTuple(
            entity_name='ResidualEncoderDecoder',
            module_path='.'.join([components_path, 'residual_encoder_decoder']),
        ),
        residual_encoder_decoder_sn=ModuleTuple(
            entity_name='ResidualEncoderDecoderSn',
            module_path='.'.join([components_path, 'residual_encoder_decoder_sn']),
        ),
        residual_encoder_decoder_in=ModuleTuple(
            entity_name='ResidualEncoderDecoderIn',
            module_path='.'.join([components_path, 'residual_encoder_decoder_in']),
        ),
        residual_encoder_decoder_cin=ModuleTuple(
            entity_name='ResidualEncoderDecoderCin',
            module_path='.'.join([components_path, 'residual_encoder_decoder_cin']),
        ),
        residual_encoder_decoder_cdn=ModuleTuple(
            entity_name='ResidualEncoderDecoderCdn',
            module_path='.'.join([components_path, 'residual_encoder_decoder_cdn']),
        ),
        unet=ModuleTuple(
            entity_name='Unet', module_path='.'.join([components_path, 'unet'])
        ),
        unet_sn=ModuleTuple(
            entity_name='UnetSn', module_path='.'.join([components_path, 'unet_sn'])
        ),
        unet_in=ModuleTuple(
            entity_name='UnetIn', module_path='.'.join([components_path, 'unet_in'])
        ),
        unet_cin=ModuleTuple(
            entity_name='UnetCin', module_path='.'.join([components_path, 'unet_cin'])
        ),
        unet_cdn=ModuleTuple(
            entity_name='UnetCdn', module_path='.'.join([components_path, 'unet_cdn'])
        ),
    )


def get_gan_paths_dict():
    """
    Maintain a lookup table of the paths to gans as implemented
    so far in the application

    Return:
        (dict) maps unique gan names (str) to a tuple (ModuleTuple) that
        contains a path to the module and the name of the class
    """
    gans_path = '.'.join(['models', 'gans'])
    return dict(
        cycle_gan=ModuleTuple(
            entity_name='CycleGan',
            module_path='.'.join([gans_path, 'cycle_gan', 'model']),
        ),
        cycle_noise_gan=ModuleTuple(
            entity_name='CycleNoiseGan',
            module_path='.'.join([gans_path, 'cycle_noise_gan', 'model']),
        ),
        artistic_gm=ModuleTuple(
            entity_name='ArtisticGm',
            module_path='.'.join([gans_path, 'artistic_gm', 'model']),
        ),
        artistic_cin_gm=ModuleTuple(
            entity_name='ArtisticCinGm',
            module_path='.'.join([gans_path, 'artistic_cin_gm', 'model']),
        ),
        artistic_dual_cin_gm=ModuleTuple(
            entity_name='ArtisticDualCinGm',
            module_path='.'.join([gans_path, 'artistic_dual_cin_gm', 'model']),
        ),
        artistic_noise_gm=ModuleTuple(
            entity_name='ArtisticNoiseGm',
            module_path='.'.join([gans_path, 'artistic_noise_gm', 'model']),
        ),
        artistic_noise_gan=ModuleTuple(
            entity_name='ArtisticNoiseGan',
            module_path='.'.join([gans_path, 'artistic_noise_gan', 'model']),
        ),
        paint_gan=ModuleTuple(
            entity_name='PaintGan',
            module_path='.'.join([gans_path, 'paint_gan', 'model']),
        ),
    )
