from os import makedirs
from os.path import join, isfile
from tensorflow.keras.utils import plot_model
from tensorflow.keras.models import load_model

from utils.filesystem import import_dynamically
from utils.json_processor import json2dict, dict2json
from models.components.auxiliaries.toolset import get_custom_objects


def load_component(component_path):
    """
    Load a component (keras Functional model)

    Args:
        component_path (str): path to the folder where the component is stored
    Return:
        component (keras.Model): the component object
    """
    component_weights_path = join(component_path, 'model_weights.h5')
    custom_objects_path = join(component_path, 'custom_objects.json')

    custom_objects = json2dict(custom_objects_path)
    for key, value in custom_objects.items():
        custom_objects[key] = import_dynamically(value, key)

    return load_model(component_weights_path, custom_objects=custom_objects)


def save_component(component, component_config, component_path):
    """
    Save a component (keras functional model) by serializing its weights,
    custom object dependencies, and an architecture diagram

    Args:
        component (tf.keras.Model): the model to save
        component_config (namesapce): the config file for the component
        component_path (str): path to the folder where the component is stored
    """
    makedirs(component_path, exist_ok=True)

    # weights
    component_weights_path = join(component_path, 'model_weights.h5')
    component.save(component_weights_path)

    # custom objects
    custom_objects_path = join(component_path, 'custom_objects.json')
    if not isfile(custom_objects_path):
        custom_objects = get_custom_objects(component_config['type'])
        dict2json(custom_objects, custom_objects_path)

    # config file
    component_config_path = join(component_path, 'model_config.json')
    if not isfile(component_config_path):
        dict2json(component_config, component_config_path)

    # model diagram
    component_diagram_path = join(component_path, 'model_diagram.png')
    if not isfile(component_diagram_path):
        plot_model(component, component_diagram_path, show_shapes=True)
