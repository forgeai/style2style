from utils.filesystem import import_dynamically
from utils.exceptions import ModelTypeDoesNotExistException
from models.factory.entity_lookup import get_gan_paths_dict, get_component_paths_dict


def get_model_type(model_type, is_component=False):
    """
    Return a functor representing the constructor for the demanded model

    Args:
        model_type (str): unique identifier name for the model
        is_component (bool): whether the module is a component and not a gan
    Return:
        the model class object
    """
    model_paths_dict = (
        get_component_paths_dict() if is_component else get_gan_paths_dict()
    )
    try:
        module_tuple = model_paths_dict[model_type]
    except KeyError:
        raise ModelTypeDoesNotExistException(model_type)

    return import_dynamically(module_tuple.module_path, module_tuple.entity_name)
