import os
import shutil
from copy import copy

from utils.json_processor import dict2json


def copy_config(config, config_name, model_path, version):
    """
    Copy the data or training config file for a new session
    in the model's folder

    Args:
        config (namespace): the config object
        config_name (str): the name of the config file
        model_path (str): the location of the model
        version (int): the version of the model
                       or the index of the training session
    """
    new_config = copy(config)
    dict2json(new_config, os.path.join(model_path, str(version), config_name))


def copy_model_config(model_config, model_path):
    """
    Copy the model config of a new model to its path and set
    the "new" flag to False

    Args:
        model_config (namespace): the model config
        model_path (str): the location of the model
    """
    new_model_config = copy(model_config)
    new_model_config.new = False
    dict2json(new_model_config, os.path.join(model_path, 'model_config.json'))


def get_latest_version(model_path):
    """
    Get the most recent version of the model.

    Args:
        model_path (str): directory to look for version numbers

    Returns:
        The highest version number, or None if no version exists
    """
    network_ids = list(filter(lambda x: x.isdigit(), os.listdir(model_path)))
    if len(network_ids):
        return max(map(int, network_ids))
    return None


def create_model_paths(model_name, project, new_version=False):
    """
    Get the paths to all model storage folders and create them if they
    don't exist already

    Args:
        model_name (str): the name of the model
        project (str): the path to the project where the model will sit
        new_version (bool): whether to create a new version folder or load
                            the most recent existing one
    Return:
        model_path (str): path to the model directory
        summaries_path (str): path to the tensorboard summaries
        checkpoints_path (str): path to the serialized checkpoints
    """
    model_path = os.path.join('projects', project, model_name)
    os.makedirs(model_path, exist_ok=True)
    latest_version = get_latest_version(model_path)

    # some current versions exist
    if latest_version is not None:
        version = str(latest_version + new_version)

    # this is the first version of this model
    else:
        if new_version:
            version = '1'
        else:
            return model_path, None, None

    summaries_path = os.path.join(model_path, version, 'summaries')
    checkpoints_path = os.path.join(model_path, version, 'checkpoints')
    os.makedirs(summaries_path, exist_ok=True)
    os.makedirs(checkpoints_path, exist_ok=True)

    return model_path, summaries_path, checkpoints_path


def copy_checkpoint_folder(model_path, checkpoints_path, epoch):
    """
    Create a copy of the current checkpoint folder

    Args:
        model_path (str): path to model's folder
        checkpoints_path (str): path to the checkpoints folder
        epoch (int): the epoch of the current checkpoint
    """
    checkpoint_copies = os.path.join(model_path, 'checkpoint_storage')
    os.makedirs(checkpoint_copies, exist_ok=True)
    shutil.copytree(
        checkpoints_path, os.path.join(checkpoint_copies, 'checkpoint_{}'.format(epoch))
    )
