import tensorflow as tf
import tensorflow.keras as keras

from models.components.convolutional import Convolutional
from models.components.auxiliaries.layers import ConditionalDepthNormalization


class ConvolutionalCdn(Convolutional):
    def __init__(self, config, name):
        super().__init__(config, name)
        self.num_styles = config.num_styles

    def _create_layers(self, config):
        """
        Create the model layers based on the configurations

        Args:
            config (namespace): configurations for the layers
        """
        super()._create_layers(config)

        # conditional depth normalization layers
        self.cdn = {
            str(i): ConditionalDepthNormalization(config.num_styles)
            for i in range(len(config.filters))
        }

    def create_model(self):
        """
        Create the keras model following the functional API where:
            'x' denotes the current feature map in the network

        Return:
            (tf.keras.Model): the model object
        """
        seed = keras.Input(shape=(None, None, 1), dtype=tf.float32, name='seed')
        mask = keras.Input(
            shape=(None, None, self.num_styles), dtype=tf.float32, name='mask'
        )
        x = seed

        for i in range(len(self.convolutions) - 1):
            x = self.convolutions[str(i)](x)
            x = self.cdn[str(i)](x, mask)
            x = self.dropout[str(i)](x)

        last_layer = str(len(self.convolutions) - 1)
        x = self.convolutions[last_layer](x)
        x = self.cdn[last_layer](x, mask)

        if self.output_activation is not None:
            x = self.output_activation(x)
        outputs = x

        return keras.Model(inputs=[seed, mask], outputs=outputs, name=self.name)
