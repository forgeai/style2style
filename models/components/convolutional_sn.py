import tensorflow.keras as keras

from utils.constants import SN_POWER_ITERATIONS
from models.components.convolutional import Convolutional
from models.components.auxiliaries.layers import SpectralNorm


class ConvolutionalSn(Convolutional):
    def __init__(self, config, name):
        super().__init__(config, name)

    def _create_layers(self, config):
        """
        Create the model layers based on the configurations

        Args:
            config (namespace): configurations for the layers
        """
        dropout_rate = getattr(config, 'dropout', 0)
        l2_reg = getattr(config, 'l2_reg', 0)
        power_iterations = getattr(config, 'power_iterations', SN_POWER_ITERATIONS)

        # convolutional layers
        self.convolutions = {}
        self.dropout = {}

        for i, (kernel, filters) in enumerate(zip(config.kernels, config.filters)):
            self.convolutions[str(i)] = keras.layers.Conv2D(
                filters=filters,
                kernel_size=kernel,
                padding='same',
                activation='elu',
                bias_regularizer=keras.regularizers.l2(l2_reg),
                kernel_regularizer=keras.regularizers.l2(l2_reg),
                bias_constraint=SpectralNorm(power_iterations),
                kernel_constraint=SpectralNorm(power_iterations),
            )
            if i < len(config.kernels) - 1:
                self.dropout[str(i)] = keras.layers.Dropout(dropout_rate)

        # last activation function
        if config.output_activation is None:
            self.output_activation = None
        elif config.output_activation == 'sigmoid':
            self.output_activation = keras.activations.sigmoid
        elif config.output_activation == 'tanh':
            self.output_activation = keras.activations.tanh
