import tensorflow.keras as keras

from utils.constants import SN_POWER_ITERATIONS
from models.components.unet import Unet
from models.components.auxiliaries.layers import (
    DynamicPadding,
    DynamicCropping,
    SpectralNorm,
)


class UnetSn(Unet):
    def __init__(self, config, name):
        super().__init__(config, name)

    def _create_layers(self, config):
        """
        Create the model layers based on the configurations

        Args:
            config (namespace): configurations for the layers
        """
        filters = config.filters[:-1]
        kernels = config.kernels[:-1]
        self.LEVELS = len(filters)

        dropout_rate = getattr(config, 'dropout', 0)
        l2_reg = getattr(config, 'l2_reg', 0)
        power_iterations = getattr(config, 'power_iterations', SN_POWER_ITERATIONS)

        # shape integrity ensuring layers
        self.prepadding = DynamicPadding()
        self.postcropping = DynamicCropping()

        # contraction layers
        self.contraction_conv = {}
        self.contraction_dropout = {}

        for level in range(self.LEVELS):
            self.contraction_conv[str(level)] = [
                keras.layers.Conv2D(
                    filters=filters[level],
                    kernel_size=kernels[level],
                    padding='same',
                    activation='elu',
                    bias_regularizer=keras.regularizers.l2(l2_reg),
                    kernel_regularizer=keras.regularizers.l2(l2_reg),
                    bias_constraint=SpectralNorm(power_iterations),
                    kernel_constraint=SpectralNorm(power_iterations),
                )
                for _ in range(2)
            ]
            self.contraction_dropout[str(level)] = [
                keras.layers.Dropout(dropout_rate) for _ in range(2)
            ]

        # expansion layers
        self.expansion_conv = {}
        self.expansion_dropout = {}

        for level in range(self.LEVELS - 1):
            self.expansion_conv[str(level)] = [
                keras.layers.Conv2D(
                    filters=filters[level],
                    kernel_size=kernels[level],
                    padding='same',
                    activation='elu',
                    bias_regularizer=keras.regularizers.l2(l2_reg),
                    kernel_regularizer=keras.regularizers.l2(l2_reg),
                    bias_constraint=SpectralNorm(power_iterations),
                    kernel_constraint=SpectralNorm(power_iterations),
                )
                for _ in range(2)
            ]
            self.expansion_dropout[str(level)] = [
                keras.layers.Dropout(dropout_rate) for _ in range(2)
            ]

        # output layer
        self.output_conv = keras.layers.Conv2D(
            filters=config.filters[-1],
            kernel_size=config.kernels[-1],
            padding='same',
            bias_constraint=SpectralNorm(power_iterations),
            kernel_constraint=SpectralNorm(power_iterations),
        )

        # downsampling layers
        self.downsampling = {
            str(level): keras.layers.AvgPool2D(pool_size=2, strides=2, padding='same')
            for level in range(self.LEVELS - 1)
        }

        # upsampling layers
        self.upsampling = {
            str(level): [
                keras.layers.Conv2DTranspose(
                    filters=filters[level - 1],
                    kernel_size=kernels[level - 1],
                    strides=2,
                    padding='same',
                    activation='elu',
                    bias_regularizer=keras.regularizers.l2(l2_reg),
                    kernel_regularizer=keras.regularizers.l2(l2_reg),
                    bias_constraint=SpectralNorm(power_iterations),
                    kernel_constraint=SpectralNorm(power_iterations),
                )
            ]
            if config.upsampling == 'transposed_convolution'
            else [
                keras.layers.UpSampling2D(size=(2, 2), interpolation=config.upsampling),
                keras.layers.Conv2D(
                    filters=config.filters[level - 1],
                    kernel_size=config.kernels[level - 1],
                    padding='same',
                    activation='elu',
                    bias_regularizer=keras.regularizers.l2(l2_reg),
                    kernel_regularizer=keras.regularizers.l2(l2_reg),
                    bias_constraint=SpectralNorm(power_iterations),
                    kernel_constraint=SpectralNorm(power_iterations),
                ),
            ]
            for level in range(1, self.LEVELS)
        }

        # concatenation layers
        self.concatenation = {
            str(level): keras.layers.Concatenate(axis=-1)
            for level in range(self.LEVELS - 1)
        }

        # last activation function
        if config.output_activation is None:
            self.output_activation = None
        elif config.output_activation == 'sigmoid':
            self.output_activation = keras.activations.sigmoid
        elif config.output_activation == 'tanh':
            self.output_activation = keras.activations.tanh
