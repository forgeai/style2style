import tensorflow as tf
import tensorflow_addons as tfa
import tensorflow.keras as keras

from models.components.auxiliaries.toolset import compute_input_paddings
from models.components.residual_encoder_decoder import ResidualEncoderDecoder


class ResidualEncoderDecoderIn(ResidualEncoderDecoder):
    def __init__(self, config, name):
        super().__init__(config, name)

    def _create_layers(self, config):
        """
        Create the model layers based on the configurations

        Args:
            config (namespace): configurations for the layers
        """
        super()._create_layers(config)

        # conditional instance normalization layers
        self.encoder_normalization = {}
        for i in range(self.LEVELS):
            self.encoder_normalization[str(i)] = tfa.layers.InstanceNormalization()

        self.decoder_normalization = {}
        for i in range(self.LEVELS - 1):
            self.decoder_normalization[str(i)] = tfa.layers.InstanceNormalization()

        self.residual_normalization = {}
        for i in range(len(self.residual_conv)):
            self.residual_normalization[str(i)] = tfa.layers.InstanceNormalization()

    def create_model(self):
        """
        Create the keras model following the functional API where:
            'x' denotes the current feature map in the network

        Return:
            (tf.keras.Model): the model object
        """
        inputs = keras.Input(shape=(None, None, 3), dtype=tf.float32, name='input_image')
        input_shape = tf.shape(inputs)

        # input pre-padding
        paddings = compute_input_paddings(input_shape, self.LEVELS)
        x = self.prepadding(inputs, paddings)

        # ====================[ encoder ]==================== #

        for i in range(len(self.encoder_conv)):
            x = self.encoder_conv[str(i)](x)
            x = self.encoder_normalization[str(i)](x)
            x = self.encoder_dropout[str(i)](x)

        # ===================[ residuals ]=================== #

        for i in range(len(self.residual_conv)):
            x = self.residual_conv[str(i)](x)
            x = self.residual_normalization[str(i)](x)
            x = self.residual_dropout[str(i)](x)

        # ====================[ decoder ]==================== #

        for i in range(len(self.decoder_conv) - 1):
            for operation in self.decoder_conv[str(i)]:
                x = operation(x)
            x = self.decoder_normalization[str(i)](x)
            x = self.decoder_dropout[str(i)](x)

        last_layer = str(len(self.decoder_conv) - 1)
        for operation in self.decoder_conv[last_layer]:
            x = operation(x)

        if self.output_activation is not None:
            x = self.output_activation(x)

        # output post-cropping
        outputs = self.postcropping(
            inputs=x,
            offsets=(paddings[1][0], paddings[2][0]),
            output_shape=input_shape[1:3],
        )

        return keras.Model(inputs=inputs, outputs=outputs, name=self.name)
