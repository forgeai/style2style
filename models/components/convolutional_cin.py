import tensorflow as tf
import tensorflow.keras as keras

from models.components.convolutional import Convolutional
from models.components.auxiliaries.layers import ConditionalInstanceNormalization


class ConvolutionalCin(Convolutional):
    def __init__(self, config, name):
        super().__init__(config, name)
        self.num_styles = config.num_styles

    def _create_layers(self, config):
        """
        Create the model layers based on the configurations

        Args:
            config (namespace): configurations for the layers
        """
        super()._create_layers(config)

        # conditional instance normalization layers
        self.cin = {
            str(i): ConditionalInstanceNormalization(config.num_styles)
            for i in range(len(config.filters))
        }

    def create_model(self):
        """
        Create the keras model following the functional API where:
            'x' denotes the current feature map in the network

        Return:
            (tf.keras.Model): the model object
        """
        inputs = keras.Input(shape=(None, None, 3), dtype=tf.float32, name='input_image')
        condition_weights = keras.Input(
            shape=(self.num_styles,), dtype=tf.float32, name='condition_weights'
        )
        x = inputs

        for i in range(len(self.convolutions) - 1):
            x = self.convolutions[str(i)](x)
            x = self.cin[str(i)](x, condition_weights)
            x = self.dropout[str(i)](x)

        last_layer = str(len(self.convolutions) - 1)
        x = self.convolutions[last_layer](x)
        x = self.cin[last_layer](x, condition_weights)

        if self.output_activation is not None:
            x = self.output_activation(x)
        outputs = x

        return keras.Model(
            inputs=[inputs, condition_weights], outputs=outputs, name=self.name
        )
