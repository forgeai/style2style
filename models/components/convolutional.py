import tensorflow as tf
import tensorflow.keras as keras

from models.components.base_component import BaseComponent


class Convolutional(BaseComponent):
    def __init__(self, config, name):
        super().__init__(config, name)

    def _create_layers(self, config):
        """
        Create the model layers based on the configurations

        Args:
            config (namespace): configurations for the layers
        """
        dropout_rate = getattr(config, 'dropout', 0)
        l2_reg = getattr(config, 'l2_reg', 0)

        # convolutional layers
        self.convolutions = {}
        self.dropout = {}

        for i, (kernel, filters) in enumerate(zip(config.kernels, config.filters)):
            self.convolutions[str(i)] = keras.layers.Conv2D(
                filters=filters,
                kernel_size=kernel,
                padding='same',
                activation='elu',
                bias_regularizer=keras.regularizers.l2(l2_reg),
                kernel_regularizer=keras.regularizers.l2(l2_reg),
            )
            if i < len(config.kernels) - 1:
                self.dropout[str(i)] = keras.layers.Dropout(dropout_rate)

        # last activation function
        if config.output_activation is None:
            self.output_activation = None
        elif config.output_activation == 'sigmoid':
            self.output_activation = keras.activations.sigmoid
        elif config.output_activation == 'tanh':
            self.output_activation = keras.activations.tanh

    def create_model(self):
        """
        Create the keras model following the functional API where:
            'x' denotes the current feature map in the network

        Return:
            (tf.keras.Model): the model object
        """
        inputs = keras.Input(shape=(None, None, 3), dtype=tf.float32, name='input_image')
        x = inputs

        for i in range(len(self.convolutions) - 1):
            x = self.convolutions[str(i)](x)
            x = self.dropout[str(i)](x)

        x = self.convolutions[str(len(self.convolutions) - 1)](x)
        if self.output_activation is not None:
            x = self.output_activation(x)
        outputs = x

        return keras.Model(inputs=inputs, outputs=outputs, name=self.name)
