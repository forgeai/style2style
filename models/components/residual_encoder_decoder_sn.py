import tensorflow.keras as keras

from utils.constants import SN_POWER_ITERATIONS
from models.components.residual_encoder_decoder import ResidualEncoderDecoder
from models.components.auxiliaries.layers import (
    ResidualBlock,
    DynamicPadding,
    DynamicCropping,
    SpectralNorm,
)


class ResidualEncoderDecoderSn(ResidualEncoderDecoder):
    def __init__(self, config, name):
        super().__init__(config, name)

    def _create_layers(self, config):
        """
        Create the model layers based on the configurations

        Args:
            config (namespace): configurations for the layers
        """
        self.LEVELS = config.levels
        encoder_filters = config.filters[: self.LEVELS]
        encoder_kernels = config.kernels[: self.LEVELS]
        decoder_filters = config.filters[-self.LEVELS :]
        decoder_kernels = config.kernels[-self.LEVELS :]
        residual_filters = config.filters[self.LEVELS : -self.LEVELS]
        residual_kernels = config.kernels[self.LEVELS : -self.LEVELS]
        assert encoder_filters[-1] == residual_filters[0], 'Residual block mismatch'
        assert len(set(residual_filters)) == 1, 'Residual filter numbers must be equal'

        dropout_rate = getattr(config, 'dropout', 0)
        l2_reg = getattr(config, 'l2_reg', 0)
        power_iterations = getattr(config, 'power_iterations', SN_POWER_ITERATIONS)

        # shape integrity ensuring layers
        self.prepadding = DynamicPadding()
        self.postcropping = DynamicCropping()

        # encoder layers
        self.encoder_conv = {}
        self.encoder_dropout = {}

        for i in range(self.LEVELS):
            self.encoder_conv[str(i)] = keras.layers.Conv2D(
                filters=encoder_filters[i],
                kernel_size=encoder_kernels[i],
                padding='same',
                activation='elu',
                bias_regularizer=keras.regularizers.l2(l2_reg),
                kernel_regularizer=keras.regularizers.l2(l2_reg),
                bias_constraint=SpectralNorm(power_iterations),
                kernel_constraint=SpectralNorm(power_iterations),
                strides=2 if i < self.LEVELS - 1 else 1,
            )
            self.encoder_dropout[str(i)] = keras.layers.Dropout(dropout_rate)

        # residual layers
        self.residual_conv = {}
        self.residual_dropout = {}

        for i in range(len(residual_filters)):
            self.residual_conv[str(i)] = ResidualBlock(
                kernel_size=residual_kernels[i],
                activation='elu',
                l2_regularizer=l2_reg,
                sn_power_iterations=power_iterations,
            )
            self.residual_dropout[str(i)] = keras.layers.Dropout(dropout_rate)

        # decoder layers
        self.decoder_conv = {}
        self.decoder_dropout = {}

        for i in range(self.LEVELS):
            self.decoder_conv[str(i)] = []

            if config.upsampling == 'transposed_convolution':
                self.decoder_conv[str(i)].append(
                    keras.layers.Conv2DTranspose(
                        filters=decoder_filters[i],
                        kernel_size=decoder_kernels[i],
                        padding='same',
                        activation='elu',
                        bias_regularizer=keras.regularizers.l2(l2_reg),
                        kernel_regularizer=keras.regularizers.l2(l2_reg),
                        bias_constraint=SpectralNorm(power_iterations),
                        kernel_constraint=SpectralNorm(power_iterations),
                        strides=2 if i < self.LEVELS - 1 else 1,
                    )
                )
            else:
                if i < self.LEVELS - 1:
                    self.decoder_conv[str(i)].append(
                        keras.layers.UpSampling2D(
                            size=(2, 2), interpolation=config.upsampling
                        )
                    )
                self.decoder_conv[str(i)].append(
                    keras.layers.Conv2D(
                        filters=decoder_filters[i],
                        kernel_size=decoder_kernels[i],
                        padding='same',
                        activation='elu',
                        bias_regularizer=keras.regularizers.l2(l2_reg),
                        kernel_regularizer=keras.regularizers.l2(l2_reg),
                        bias_constraint=SpectralNorm(power_iterations),
                        kernel_constraint=SpectralNorm(power_iterations),
                    )
                )
            if i < self.LEVELS - 1:
                self.decoder_dropout[str(i)] = keras.layers.Dropout(dropout_rate)

        # last activation function
        if config.output_activation is None:
            self.output_activation = None
        elif config.output_activation == 'sigmoid':
            self.output_activation = keras.activations.sigmoid
        elif config.output_activation == 'tanh':
            self.output_activation = keras.activations.tanh
