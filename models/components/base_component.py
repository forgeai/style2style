from abc import ABC, abstractmethod


class BaseComponent(ABC):
    def __init__(self, config, name):
        self.name = name
        self._create_layers(config)

    @abstractmethod
    def _create_layers(self, config):
        pass

    @abstractmethod
    def create_model(self):
        pass
