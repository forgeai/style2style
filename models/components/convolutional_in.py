import tensorflow as tf
import tensorflow_addons as tfa
import tensorflow.keras as keras

from models.components.convolutional import Convolutional


class ConvolutionalIn(Convolutional):
    def __init__(self, config, name):
        super().__init__(config, name)

    def _create_layers(self, config):
        """
        Create the model layers based on the configurations

        Args:
            config (namespace): configurations for the layers
        """
        super()._create_layers(config)

        # conditional instance normalization layers
        self.normalizations = {
            str(i): tfa.layers.InstanceNormalization() for i in range(len(config.filters))
        }

    def create_model(self):
        """
        Create the keras model following the functional API where:
            'x' denotes the current feature map in the network

        Return:
            (tf.keras.Model): the model object
        """
        inputs = keras.Input(shape=(None, None, 3), dtype=tf.float32, name='input_image')
        x = inputs

        for i in range(len(self.convolutions) - 1):
            x = self.convolutions[str(i)](x)
            x = self.normalizations[str(i)](x)
            x = self.dropout[str(i)](x)

        last_layer = str(len(self.convolutions) - 1)
        x = self.convolutions[last_layer](x)
        x = self.normalizations[last_layer](x)

        if self.output_activation is not None:
            x = self.output_activation(x)
        outputs = x

        return keras.Model(inputs=inputs, outputs=outputs, name=self.name)
