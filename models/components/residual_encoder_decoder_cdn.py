import tensorflow as tf
import tensorflow.keras as keras

from models.components.auxiliaries.toolset import compute_input_paddings
from models.components.auxiliaries.layers import ConditionalDepthNormalization
from models.components.residual_encoder_decoder import ResidualEncoderDecoder


class ResidualEncoderDecoderCdn(ResidualEncoderDecoder):
    def __init__(self, config, name):
        super().__init__(config, name)
        self.num_styles = config.num_styles

    def _create_layers(self, config):
        """
        Create the model layers based on the configurations

        Args:
            config (namespace): configurations for the layers
        """
        super()._create_layers(config)

        # conditional instance normalization layers
        self.encoder_cdn = {}
        for i in range(self.LEVELS):
            self.encoder_cdn[str(i)] = ConditionalDepthNormalization(config.num_styles)

        self.decoder_cdn = {}
        for i in range(self.LEVELS - 1):
            self.decoder_cdn[str(i)] = ConditionalDepthNormalization(config.num_styles)

        self.residual_cdn = {}
        for i in range(len(self.residual_conv)):
            self.residual_cdn[str(i)] = ConditionalDepthNormalization(config.num_styles)

        # mask downsampling layers
        self.mask_downsampling = {}

        for level in range(1, self.LEVELS):
            self.mask_downsampling[str(level)] = keras.layers.AveragePooling2D((2, 2))

    def create_model(self):
        """
        Create the keras model following the functional API where:
            'x' denotes the current feature map in the network

        Return:
            (tf.keras.Model): the model object
        """
        seed = keras.Input(shape=(None, None, 1), dtype=tf.float32, name='seed')
        mask = keras.Input(
            shape=(None, None, self.num_styles), dtype=tf.float32, name='mask'
        )
        input_shape = tf.shape(seed)

        # input pre-padding
        paddings = compute_input_paddings(input_shape, self.LEVELS)
        x = self.prepadding(seed, paddings)

        # =====================[ masks ]===================== #

        level_mask = {0: self.prepadding(mask, paddings)}

        for level in range(1, self.LEVELS):
            level_mask[level] = self.mask_downsampling[str(level)](level_mask[level - 1])

        # ====================[ encoder ]==================== #

        for i in range(len(self.encoder_conv)):
            x = self.encoder_conv[str(i)](x)
            x = self.encoder_cdn[str(i)](x, level_mask[min(i + 1, self.LEVELS - 1)])
            x = self.encoder_dropout[str(i)](x)

        # ===================[ residuals ]=================== #

        for i in range(len(self.residual_conv)):
            x = self.residual_conv[str(i)](x)
            x = self.residual_cdn[str(i)](x, level_mask[self.LEVELS - 1])
            x = self.residual_dropout[str(i)](x)

        # ====================[ decoder ]==================== #

        for i in range(len(self.decoder_conv) - 1):
            for operation in self.decoder_conv[str(i)]:
                x = operation(x)
            x = self.decoder_cdn[str(i)](x, level_mask[self.LEVELS - 2 - i])
            x = self.decoder_dropout[str(i)](x)

        last_layer = str(len(self.decoder_conv) - 1)
        for operation in self.decoder_conv[last_layer]:
            x = operation(x)

        if self.output_activation is not None:
            x = self.output_activation(x)

        # output post-cropping
        outputs = self.postcropping(
            inputs=x,
            offsets=(paddings[1][0], paddings[2][0]),
            output_shape=input_shape[1:3],
        )

        return keras.Model(inputs=[seed, mask], outputs=outputs, name=self.name)
