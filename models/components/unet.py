import tensorflow as tf
import tensorflow.keras as keras

from models.components.base_component import BaseComponent
from models.components.auxiliaries.toolset import compute_input_paddings
from models.components.auxiliaries.layers import DynamicPadding, DynamicCropping


class Unet(BaseComponent):
    def __init__(self, config, name):
        super().__init__(config, name)

    def _create_layers(self, config):
        """
        Create the model layers based on the configurations

        Args:
            config (namespace): configurations for the layers
        """
        filters = config.filters[:-1]
        kernels = config.kernels[:-1]
        self.LEVELS = len(filters)

        dropout_rate = getattr(config, 'dropout', 0)
        l2_reg = getattr(config, 'l2_reg', 0)

        # shape integrity ensuring layers
        self.prepadding = DynamicPadding()
        self.postcropping = DynamicCropping()

        # contraction layers
        self.contraction_conv = {}
        self.contraction_dropout = {}

        for level in range(self.LEVELS):
            self.contraction_conv[str(level)] = [
                keras.layers.Conv2D(
                    filters=filters[level],
                    kernel_size=kernels[level],
                    padding='same',
                    activation='elu',
                    bias_regularizer=keras.regularizers.l2(l2_reg),
                    kernel_regularizer=keras.regularizers.l2(l2_reg),
                )
                for _ in range(2)
            ]
            self.contraction_dropout[str(level)] = [
                keras.layers.Dropout(dropout_rate) for _ in range(2)
            ]

        # expansion layers
        self.expansion_conv = {}
        self.expansion_dropout = {}

        for level in range(self.LEVELS - 1):
            self.expansion_conv[str(level)] = [
                keras.layers.Conv2D(
                    filters=filters[level],
                    kernel_size=kernels[level],
                    padding='same',
                    activation='elu',
                    bias_regularizer=keras.regularizers.l2(l2_reg),
                    kernel_regularizer=keras.regularizers.l2(l2_reg),
                )
                for _ in range(2)
            ]
            self.expansion_dropout[str(level)] = [
                keras.layers.Dropout(dropout_rate) for _ in range(2)
            ]

        # output layer
        self.output_conv = keras.layers.Conv2D(
            filters=config.filters[-1], kernel_size=config.kernels[-1], padding='same'
        )

        # downsampling layers
        self.downsampling = {
            str(level): keras.layers.AvgPool2D(pool_size=2, strides=2, padding='same')
            for level in range(self.LEVELS - 1)
        }

        # upsampling layers
        self.upsampling = {
            str(level): [
                keras.layers.Conv2DTranspose(
                    filters=filters[level - 1],
                    kernel_size=kernels[level - 1],
                    strides=2,
                    padding='same',
                    activation='elu',
                    bias_regularizer=keras.regularizers.l2(l2_reg),
                    kernel_regularizer=keras.regularizers.l2(l2_reg),
                )
            ]
            if config.upsampling == 'transposed_convolution'
            else [
                keras.layers.UpSampling2D(size=(2, 2), interpolation=config.upsampling),
                keras.layers.Conv2D(
                    filters=config.filters[level - 1],
                    kernel_size=config.kernels[level - 1],
                    padding='same',
                    activation='elu',
                    bias_regularizer=keras.regularizers.l2(l2_reg),
                    kernel_regularizer=keras.regularizers.l2(l2_reg),
                ),
            ]
            for level in range(1, self.LEVELS)
        }

        # concatenation layers
        self.concatenation = {
            str(level): keras.layers.Concatenate(axis=-1)
            for level in range(self.LEVELS - 1)
        }

        # last activation function
        if config.output_activation is None:
            self.output_activation = None
        elif config.output_activation == 'sigmoid':
            self.output_activation = keras.activations.sigmoid
        elif config.output_activation == 'tanh':
            self.output_activation = keras.activations.tanh

    def create_model(self):
        """
        Create the keras model following the functional API where:
            'x' denotes the current feature map in the network
            'level_output' maps each level to the output of the conv pair

        Return:
            (tf.keras.Model): the model object
        """
        inputs = keras.Input(shape=(None, None, 3), dtype=tf.float32, name='input_image')
        input_shape = tf.shape(inputs)

        # input pre-padding
        paddings = compute_input_paddings(input_shape, self.LEVELS)
        x = self.prepadding(inputs, paddings)

        level_output = {}

        # ====================[ contraction ]==================== #

        for level in range(self.LEVELS):
            x = self.contraction_conv[str(level)][0](x)
            x = self.contraction_dropout[str(level)][0](x)
            level_output[level] = self.contraction_conv[str(level)][1](x)

            if level < self.LEVELS - 1:  # last level doesn't perform pooling
                x = self.downsampling[str(level)](level_output[level])
            else:
                x = level_output[level]
            x = self.contraction_dropout[str(level)][1](x)

        # =====================[ expansion ]===================== #

        for level in range(self.LEVELS - 1, 0, -1):
            for operation in self.upsampling[str(level)]:
                x = operation(x)
            x = self.concatenation[str(level - 1)]([x, level_output[level - 1]])

            for i in range(2):
                x = self.expansion_conv[str(level - 1)][i](x)
                x = self.expansion_dropout[str(level - 1)][i](x)

        x = self.output_conv(x)
        if self.output_activation is not None:
            x = self.output_activation(x)

        # output post-cropping
        outputs = self.postcropping(
            inputs=x,
            offsets=(paddings[1][0], paddings[2][0]),
            output_shape=input_shape[1:3],
        )

        return keras.Model(inputs=inputs, outputs=outputs, name=self.name)
