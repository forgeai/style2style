from models.factory.entity_lookup import get_custom_layer_paths_dict


def compute_input_paddings(input_shape, levels):
    """
    Compute the amount required for padding the input so that the
    contraction-expansion process does not lose pixels on the way

    Args:
        input_shape (tuple): contains the shape of the tensor
        levels (int): the number of resolution levels
    Returns:
        paddings (list): number of pixels to pad in each dimension
    """
    paddings = [[0, 0], [0, 0], [0, 0], [0, 0]]

    for d in range(1, 3):
        # both width and height have to be a multiple of
        # 2 ^ (number of poolings) in order for the downsampling
        # to not deteriorate the shape
        divisor = 2 ** (levels - 1)
        full_paddings = (divisor - input_shape[d]) % divisor
        side_paddings = full_paddings // 2
        paddings[d] = [side_paddings, full_paddings - side_paddings]

    return paddings


def get_custom_layers_dict(layer_names):
    """
    Create a dictionary that maps custom layer class names to their module path

    Args:
        layer_names (list of str): the identifiers for the custom layers
    Return:
        custom_layers_dict (dict)
    """
    custom_layer_paths_dict = get_custom_layer_paths_dict()
    custom_layers_dict = {}

    for layer_name in layer_names:
        module_tuple = custom_layer_paths_dict[layer_name]
        custom_layers_dict[module_tuple.entity_name] = module_tuple.module_path

    return custom_layers_dict


def get_component_hparams_dict(component_config):
    """
    Create a dictionary that maps hparam names to their value based on the
    component config file

    Args:
        component_config (namespace): configurations for the component
    Return:
        hparams_dict (dict): maps hparam to value
    """
    hparams_dict = {}

    hparams_dict['type'] = component_config.type
    hparams_dict['dropout'] = getattr(component_config, 'dropout', 0)
    hparams_dict['l2_reg'] = getattr(component_config, 'l2_reg', 0)
    if hasattr(component_config, 'upsampling'):
        hparams_dict['upsampling'] = component_config.upsampling

    return hparams_dict


def get_custom_objects(component_type):
    """
    Identify the custom layers used by the given component type

    Args:
        component_type (str): the string that identifies the component type
    Return:
        custom_objects_dict (dict of dicts): the outer dict maps component names
                                             to inner dicts
                                             the inner dicts map layer class names
                                             to the path to their module
    """
    custom_layers_lookup = dict(
        unet=get_custom_layers_dict(['dynamic_padding', 'dynamic_cropping']),
        unet_sn=get_custom_layers_dict(
            ['dynamic_padding', 'dynamic_cropping', 'spectral_norm']
        ),
        unet_in=get_custom_layers_dict(
            ['dynamic_padding', 'dynamic_cropping', 'addons_instance_normalization']
        ),
        unet_cin=get_custom_layers_dict(
            ['dynamic_padding', 'dynamic_cropping', 'conditional_instance_normalization']
        ),
        unet_cdn=get_custom_layers_dict(
            ['dynamic_padding', 'dynamic_cropping', 'conditional_depth_normalization']
        ),
        residual_encoder_decoder=get_custom_layers_dict(
            ['dynamic_padding', 'dynamic_cropping', 'residual_block']
        ),
        residual_encoder_decoder_sn=get_custom_layers_dict(
            ['dynamic_padding', 'dynamic_cropping', 'residual_block', 'spectral_norm']
        ),
        residual_encoder_decoder_in=get_custom_layers_dict(
            [
                'dynamic_padding',
                'dynamic_cropping',
                'residual_block',
                'addons_instance_normalization',
            ]
        ),
        residual_encoder_decoder_cin=get_custom_layers_dict(
            [
                'dynamic_padding',
                'dynamic_cropping',
                'residual_block',
                'conditional_instance_normalization',
            ]
        ),
        residual_encoder_decoder_cdn=get_custom_layers_dict(
            [
                'dynamic_padding',
                'dynamic_cropping',
                'residual_block',
                'conditional_depth_normalization',
            ]
        ),
        convolutional=get_custom_layers_dict([]),
        convolutional_sn=get_custom_layers_dict(['spectral_norm']),
        convolutional_in=get_custom_layers_dict(['addons_instance_normalization']),
        convolutional_cin=get_custom_layers_dict(['conditional_instance_normalization']),
        convolutional_cdn=get_custom_layers_dict(['conditional_depth_normalization']),
    )
    return custom_layers_lookup[component_type]
