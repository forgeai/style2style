import tensorflow as tf
from tensorflow.keras import layers, regularizers, constraints


class ResidualBlock(layers.Layer):
    def __init__(
        self,
        kernel_size,
        dilation_rate=(1, 1),
        activation=None,
        l2_regularizer=None,
        sn_power_iterations=None,
        **kwargs,
    ):
        super().__init__(**kwargs)
        self.kernel_size = kernel_size
        self.dilation_rate = dilation_rate
        self.activation = activation
        self.l2_regularizer = l2_regularizer
        self.sn_power_iterations = sn_power_iterations

    def get_config(self):
        config = super().get_config().copy()
        config.update(
            dict(
                kernel_size=self.kernel_size,
                dilation_rate=self.dilation_rate,
                activation=self.activation,
                l2_regularizer=self.l2_regularizer,
                sn_power_iterations=self.sn_power_iterations,
            )
        )
        return config

    def build(self, input_shape):
        self.convolution_1 = layers.Conv2D(
            filters=input_shape[-1],
            kernel_size=self.kernel_size,
            padding='same',
            activation=self.activation,
            dilation_rate=self.dilation_rate,
            bias_regularizer=regularizers.l2(self.l2_regularizer),
            kernel_regularizer=regularizers.l2(self.l2_regularizer),
            bias_constraint=None
            if self.sn_power_iterations is None
            else SpectralNorm(self.sn_power_iterations),
            kernel_constraint=None
            if self.sn_power_iterations is None
            else SpectralNorm(self.sn_power_iterations),
        )
        self.convolution_2 = layers.Conv2D(
            filters=input_shape[-1],
            kernel_size=self.kernel_size,
            padding='same',
            activation=self.activation,
            dilation_rate=self.dilation_rate,
            bias_regularizer=regularizers.l2(self.l2_regularizer),
            kernel_regularizer=regularizers.l2(self.l2_regularizer),
            bias_constraint=None
            if self.sn_power_iterations is None
            else SpectralNorm(self.sn_power_iterations),
            kernel_constraint=None
            if self.sn_power_iterations is None
            else SpectralNorm(self.sn_power_iterations),
        )

    def call(self, inputs):
        x = self.convolution_1(inputs)
        x = self.convolution_2(x)
        return layers.add([inputs, x])


class DynamicPadding(layers.Layer):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def build(self, input_shape):
        pass

    def call(self, inputs, paddings):
        return tf.pad(inputs, paddings, 'REFLECT')


class DynamicCropping(layers.Layer):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def build(self, input_shape):
        pass

    def call(self, inputs, offsets, output_shape):
        return tf.image.crop_to_bounding_box(
            image=inputs,
            offset_height=offsets[0],
            offset_width=offsets[1],
            target_height=output_shape[0],
            target_width=output_shape[1],
        )


class ChannelNormalization(layers.Layer):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def build(self, input_shape):
        pass

    def call(self, inputs):
        mean, var = tf.nn.moments(inputs, axes=[1, 2])

        shape = tf.shape(inputs)
        broadcasting_shape = (shape[1], shape[2], shape[0], shape[3])

        broadcasted_mean = tf.broadcast_to(mean, broadcasting_shape)
        mean_tensor = tf.transpose(broadcasted_mean, [2, 0, 1, 3])
        broadcasted_var = tf.broadcast_to(var, broadcasting_shape)
        var_tensor = tf.transpose(broadcasted_var, [2, 0, 1, 3])

        return tf.divide(tf.subtract(inputs, mean_tensor), tf.sqrt(var_tensor))


class InstanceNormalization(layers.Layer):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def build(self, input_shape):
        self.channel_normalization = ChannelNormalization()

        param_shape = input_shape[3]
        self.gamma = tf.Variable(tf.ones(param_shape), trainable=True, dtype=tf.float32)
        self.beta = tf.Variable(tf.zeros(param_shape), trainable=True, dtype=tf.float32)

    def call(self, inputs):
        normalized = self.channel_normalization(inputs)

        shape = tf.shape(inputs)
        broadcasting_shape = (shape[1], shape[2], shape[0], shape[3])

        broadcasted_gamma = tf.broadcast_to(self.gamma, broadcasting_shape)
        gamma_tensor = tf.transpose(broadcasted_gamma, [2, 0, 1, 3])
        broadcasted_beta = tf.broadcast_to(self.beta, broadcasting_shape)
        beta_tensor = tf.transpose(broadcasted_beta, [2, 0, 1, 3])

        return tf.add(tf.multiply(gamma_tensor, normalized), beta_tensor)


class ConditionalInstanceNormalization(layers.Layer):
    def __init__(self, num_conditions, **kwargs):
        super().__init__(**kwargs)
        self.num_conditions = num_conditions

    def get_config(self):
        config = super().get_config().copy()
        config.update(dict(num_conditions=self.num_conditions))
        return config

    def build(self, input_shape):
        self.channel_normalization = ChannelNormalization()

        self.param_shape = (input_shape[3], self.num_conditions)
        self.gamma = tf.Variable(
            tf.ones(self.param_shape), trainable=True, dtype=tf.float32, name='gamma'
        )
        self.beta = tf.Variable(
            tf.zeros(self.param_shape), trainable=True, dtype=tf.float32, name='beta'
        )

    def call(self, inputs, condition_weights):
        normalized = self.channel_normalization(inputs)

        broadcasted_weights = tf.broadcast_to(condition_weights, self.param_shape)
        conditioned_gamma = tf.reduce_sum(
            tf.multiply(broadcasted_weights, self.gamma), axis=-1
        )
        conditioned_beta = tf.reduce_sum(
            tf.multiply(broadcasted_weights, self.beta), axis=-1
        )

        shape = tf.shape(inputs)
        broadcasting_shape = (shape[1], shape[2], shape[0], shape[3])

        broadcasted_gamma = tf.broadcast_to(conditioned_gamma, broadcasting_shape)
        gamma_tensor = tf.transpose(broadcasted_gamma, [2, 0, 1, 3])
        broadcasted_beta = tf.broadcast_to(conditioned_beta, broadcasting_shape)
        beta_tensor = tf.transpose(broadcasted_beta, [2, 0, 1, 3])

        return tf.add(tf.multiply(gamma_tensor, normalized), beta_tensor)


class LocalNormalization(layers.Layer):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def build(self, input_shape):
        pass

    def call(self, inputs):
        mean_tensor, var_tensor = tf.nn.moments(inputs, axes=[3], keepdims=True)
        return tf.divide(tf.subtract(inputs, mean_tensor), tf.sqrt(var_tensor))


class DepthNormalization(layers.Layer):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def build(self, input_shape):
        self.local_normalization = LocalNormalization()

        param_shape = input_shape[3]
        self.gamma = tf.Variable(tf.ones(param_shape), trainable=True, dtype=tf.float32)
        self.beta = tf.Variable(tf.zeros(param_shape), trainable=True, dtype=tf.float32)

    def call(self, inputs):
        normalized = self.local_normalization(inputs)

        shape = tf.shape(inputs)
        broadcasting_shape = (shape[1], shape[2], shape[0], shape[3])

        broadcasted_gamma = tf.broadcast_to(self.gamma, broadcasting_shape)
        gamma_tensor = tf.transpose(broadcasted_gamma, [2, 0, 1, 3])
        broadcasted_beta = tf.broadcast_to(self.beta, broadcasting_shape)
        beta_tensor = tf.transpose(broadcasted_beta, [2, 0, 1, 3])

        return tf.add(tf.multiply(gamma_tensor, normalized), beta_tensor)


class ConditionalDepthNormalization(layers.Layer):
    def __init__(self, num_conditions, **kwargs):
        super().__init__(**kwargs)
        self.num_conditions = num_conditions

    def get_config(self):
        config = super().get_config().copy()
        config.update(dict(num_conditions=self.num_conditions))
        return config

    def build(self, input_shape):
        self.local_normalization = LocalNormalization()

        param_shape = (input_shape[3], self.num_conditions)
        self.gamma = tf.Variable(
            tf.ones(param_shape), trainable=True, dtype=tf.float32, name='gamma'
        )
        self.beta = tf.Variable(
            tf.zeros(param_shape), trainable=True, dtype=tf.float32, name='beta'
        )

    def call(self, inputs, condition_mask):
        normalized = self.local_normalization(inputs)

        broadcasted_mask = tf.expand_dims(condition_mask, axis=-2)
        broadcasted_gamma = self.gamma[tf.newaxis, tf.newaxis, tf.newaxis, ...]
        broadcasted_beta = self.beta[tf.newaxis, tf.newaxis, tf.newaxis, ...]

        gamma_tensor = tf.reduce_sum(
            tf.multiply(broadcasted_mask, broadcasted_gamma), axis=-1
        )
        beta_tensor = tf.reduce_sum(
            tf.multiply(broadcasted_mask, broadcasted_beta), axis=-1
        )

        return tf.add(tf.multiply(gamma_tensor, normalized), beta_tensor)


class SpectralNorm(constraints.Constraint):
    """
    Uses power iteration method to calculate a fast approximation of the
    spectral norm (Golub & Van der Vorst). The weights are then scaled by
    the inverse of the spectral norm
    """

    def __init__(self, power_iterations):
        self.power_iterations = power_iterations

    def get_config(self):
        return dict(power_iterations=self.power_iterations)

    @staticmethod
    def l2_normalize(x, eps=1e-12):
        """
        Scale input by the inverse of it's euclidean norm
        """
        return x / tf.linalg.norm(x + eps)

    def __call__(self, w):
        flattened_w = tf.reshape(w, [w.shape[0], -1])
        u = tf.random.normal([flattened_w.shape[0]])
        v = tf.random.normal([flattened_w.shape[1]])

        for _ in range(self.power_iterations):
            v = tf.linalg.matvec(tf.transpose(flattened_w), u)
            v = SpectralNorm.l2_normalize(v)
            u = tf.linalg.matvec(flattened_w, v)
            u = SpectralNorm.l2_normalize(u)

        sigma = tf.tensordot(u, tf.linalg.matvec(flattened_w, v), axes=1)
        return w / sigma
