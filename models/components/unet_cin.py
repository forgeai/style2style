import tensorflow as tf
import tensorflow.keras as keras

from models.components.unet import Unet
from models.components.auxiliaries.toolset import compute_input_paddings
from models.components.auxiliaries.layers import ConditionalInstanceNormalization


class UnetCin(Unet):
    def __init__(self, config, name):
        super().__init__(config, name)
        self.num_styles = config.num_styles

    def _create_layers(self, config):
        """
        Create the model layers based on the configurations

        Args:
            config (namespace): configurations for the layers
        """
        super()._create_layers(config)

        # conditional instance normalization layers
        self.contraction_cin = {}
        self.expansion_cin = {}

        for level in range(self.LEVELS):
            self.contraction_cin[str(level)] = [
                ConditionalInstanceNormalization(config.num_styles) for _ in range(2)
            ]
            if level < self.LEVELS - 1:
                self.expansion_cin[str(level)] = [
                    ConditionalInstanceNormalization(config.num_styles) for _ in range(2)
                ]

    def create_model(self):
        """
        Create the keras model following the functional API where:
            'x' denotes the current feature map in the network
            'level_output' maps each level to the output of the conv pair

        Return:
            (tf.keras.Model): the model object
        """
        inputs = keras.Input(shape=(None, None, 3), dtype=tf.float32, name='input_image')
        condition_weights = keras.Input(
            shape=(self.num_styles,), dtype=tf.float32, name='condition_weights'
        )
        input_shape = tf.shape(inputs)

        # input pre-padding
        paddings = compute_input_paddings(input_shape, self.LEVELS)
        x = self.prepadding(inputs, paddings)

        level_output = {}

        # ====================[ contraction ]==================== #

        for level in range(self.LEVELS):
            x = self.contraction_conv[str(level)][0](x)
            x = self.contraction_cin[str(level)][0](x, condition_weights)
            x = self.contraction_dropout[str(level)][0](x)

            x = self.contraction_conv[str(level)][1](x)
            level_output[level] = self.contraction_cin[str(level)][1](
                x, condition_weights
            )

            if level < self.LEVELS - 1:  # last level doesn't perform pooling
                x = self.downsampling[str(level)](level_output[level])
            else:
                x = level_output[level]
            x = self.contraction_dropout[str(level)][1](x)

        # =====================[ expansion ]===================== #

        for level in range(self.LEVELS - 1, 0, -1):
            for operation in self.upsampling[str(level)]:
                x = operation(x)
            x = self.concatenation[str(level - 1)]([x, level_output[level - 1]])

            for i in range(2):
                x = self.expansion_conv[str(level - 1)][i](x)
                x = self.expansion_cin[str(level - 1)][i](x, condition_weights)
                x = self.expansion_dropout[str(level - 1)][i](x)

        x = self.output_conv(x)
        if self.output_activation is not None:
            x = self.output_activation(x)

        # output post-cropping
        outputs = self.postcropping(
            inputs=x,
            offsets=(paddings[1][0], paddings[2][0]),
            output_shape=input_shape[1:3],
        )

        return keras.Model(
            inputs=[inputs, condition_weights], outputs=outputs, name=self.name
        )
