import cv2
import logging
import numpy as np
import tensorflow as tf
import tensorflow_probability as tfp
from functools import partial
from os import listdir, makedirs
from os.path import join, basename

from data_pipeline.processing.load import optimize_loading
from data_pipeline.processing.extract import tfrecords2dataset
from data_pipeline.processing.transform import (
    arrange_single_dataset_for_training,
    arrange_single_dataset_for_validation,
)
from utils.image_processor import image_path_to_tensor
from data_pipeline.factory.image_processing import resize_to_fixed_dimensions
from data_pipeline.factory.data_creation_pipeline import perform_entire_pipeline


def get_summary_attributes():
    """
    Return all the attributes that are part of the summarization
    for the ArtisticBaseGan

    Return:
        attributes (tuple of str)
    """
    return (
        'graph',
        'hparams',
        'image_samples',
        'image_samples_frequency',
        'colour_distributions_frequency',
    )


def store_style_images(data_config, storing_path):
    """
    Process the style images and store them (presumably in the model folder)

    Args:
        data_config (namespace): contains data configurations and paths
        storing_path (str): location where the images will be saved
    """
    makedirs(storing_path)
    fixed_dim = data_config.fixed_size
    fixed_mode = data_config.fixed_mode

    if isinstance(fixed_dim, int):
        fixed_dim = (fixed_dim, fixed_dim)
    else:
        assert len(fixed_dim) == 2, 'Image size must have a maximum rank of 2'
        fixed_dim = tuple(fixed_dim)

    for i, image_name in enumerate(listdir(data_config.style_images_path)):
        image = cv2.imread(
            join(data_config.style_images_path, image_name), cv2.IMREAD_COLOR
        )
        refined_image = resize_to_fixed_dimensions(image, fixed_dim, fixed_mode)
        cv2.imwrite(join(storing_path, '{}_{}'.format(i, image_name)), refined_image)


def load_style_images(style_images_path):
    """
    Load the images used as style references

    Args:
        style_images_path (str): path to the folder that contains the images
    Return:
        style_images (dict of tf.Tensors): maps index to style image tensor
    """
    style_images = {}
    for image_name in listdir(style_images_path):

        image_index = int(image_name.split('_')[0])
        image_path = join(style_images_path, image_name)

        image_tensor = image_path_to_tensor(image_path)
        style_images[image_index] = image_tensor

    return style_images


def load_noise_reconstructor(noise_reconstructors_path):
    """
    Load the noise reconstructor and return it as a np.array

    Args:
        noise_reconstructors_path (str): path to the folder that contains the images
    Return:
        noise_reconstructor (np.array): the reconstructor image
    """
    for filename in listdir(noise_reconstructors_path):
        if filename.endswith('.npy'):
            image_path = join(noise_reconstructors_path, filename)
            noise_reconstructor = np.load(image_path)

    return noise_reconstructor


def gram_matrix(input_tensor):
    """
    Compute the gram matrix for the correlations between activations in a set of
    feature maps

    Args:
        input_tensor (tf.Tensor): feature maps
    Return:
        the gram matrix for the feature maps (tf.Tensor)
    """
    input_tensor.set_shape((None, None, None, None))
    corr_matrix = tf.linalg.einsum('bijc,bijd->cd', input_tensor, input_tensor)

    input_shape = tf.shape(input_tensor)
    num_locations = tf.cast(input_shape[0] * input_shape[1] * input_shape[2], tf.float32)

    return corr_matrix / num_locations


def extracted_features_generator(dataset, feature_extractor, layers, include_inputs):
    """
    Generate features extracted from the given layers of a pretrained classifer

    Args:
        dataset (tf.data.Dataset): original dataset with input images only
        feature_extractor (tf.keras.Models): pretrained model (normally Vgg16)
        layers (list of ints): the levels in the network to extract from
        include_inputs (bool): whether to include the original dataset too or not
    Yield:
        features (tf.Tensor): one element at a time
    """
    for sample in dataset:
        features = tuple([feature_extractor(sample, layer) for layer in layers])

        if include_inputs:
            yield (sample,) + features
        else:
            yield features


def get_extracted_features_generator(dataset, feature_extractor, layers, include_inputs):
    """
    Wraps extracted_features_generator in a partial function

    Args:
        dataset (tf.data.Dataset): original dataset with input images only
        feature_extractor (tf.keras.Models): pretrained model (normally Vgg16)
        layers (list of ints): the levels in the network to extract from
        include_inputs (bool): whether to include the original dataset too or not
    Return:
        extracted_features_generator (functor): the features generator
    """
    return partial(
        extracted_features_generator,
        dataset=dataset,
        feature_extractor=feature_extractor,
        layers=layers,
        include_inputs=include_inputs,
    )


def create_features_dataset(dataset, feature_extractor, layers, include_inputs=False):
    """
    Create a new dataset by extracting features from the original samples
    (using a pretrained classifier like Vgg)

    Args:
        dataset (tf.data.Dataset): original dataset with input images only
        feature_extractor (FeatureExtractor): the feature extracting instance
        layers (list of ints): the levels in the network to extract from
        include_inputs (bool): whether to include the original dataset too or not
    Return:
        features_dataset (tf.data.Dataset): every element is a tuple of
                                            inputs and features from different levels
    """
    return tf.data.Dataset.from_generator(
        get_extracted_features_generator(
            dataset, feature_extractor, layers, include_inputs
        ),
        output_types=tuple((len(layers) + include_inputs) * [tf.float32]),
    )


def _flatten_except_channels(image):
    """
    Flatten an image on batch, width and height dimensions

    Args:
        image (np.array/tf.Tensor): a tensor of rank 3 or 4
                                    (with or without batch dimension)
    Return:
        flattened_image (tf.Tensor): a tensor of rank 2
    """
    new_image_shape = (tf.reduce_prod(image.shape[:-1]), image.shape[-1])
    return tf.reshape(image, new_image_shape)


def get_channel_eigenvectors(image):
    """
    Compute the eigenvectors of the channels dimension of an image

    Args:
        image (np.array/tf.Tensor): the input image
    Return:
        eigenvectors (tf.Tensor): a matrix (of order 3x3 if used for RGB)
    """
    flattened_image = _flatten_except_channels(image)
    covariance = tfp.stats.covariance(flattened_image, event_axis=-1)
    _, eigenvectors = tf.linalg.eigh(covariance)

    return eigenvectors


def project_image_to_featurespace(image, projection_vector):
    """
    Flatten an image and project its values to a different space
    by means of a dot product

    Args:
        image (np.array/tf.Tensor): the input image
        projection_vector (np.array/tf.Tensor): the vector used for projection
    Return:
        projected_image (tf.Tensor): the flattened image as projected in the new space
    """
    flattened_image = _flatten_except_channels(image)
    return tf.matmul(flattened_image, projection_vector)


def prepare_datasets(data_config, training_config):
    """
    Construct the datasets for training

    Args:
        data_config (namespace): contains data configurations and paths
        training_config (namespace): contains training configurations for the dataset
    Return:
        training_set, validation_set (tf.data.Dataset objects)
    """
    logger = logging.getLogger(__name__)

    # create/refine data
    tfrecords_content_path = perform_entire_pipeline(
        data_config.content_images_path,
        data_config.splits,
        None,
        None,
        data_config.fixed_size,
        data_config.fixed_mode,
    )

    # extract data
    logger.info('Loading datasets...')
    dataset_x = tfrecords2dataset(
        tfrecords_content_path,
        ('training', 'validation'),
        training_config.overlapping_datasets,
    )
    logger.info('\tLoaded {}'.format(basename(tfrecords_content_path)))

    # transform data
    logger.info('Preparing datasets for training...')
    training_set = arrange_single_dataset_for_training(
        dataset_x['training'], training_config
    )
    validation_set = arrange_single_dataset_for_validation(
        dataset_x['validation'], training_config
    )

    # load data
    training_set = optimize_loading(training_set)
    validation_set = optimize_loading(validation_set)

    return training_set, validation_set
