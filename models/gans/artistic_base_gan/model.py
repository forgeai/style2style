import shutil
import tensorflow as tf
from os.path import join
from abc import abstractmethod

from utils.image_processor import tensor2image
from models.gans.base_gan.model import BaseGan
from models.gans.artistic_base_gan.toolset import (
    prepare_datasets,
    load_style_images,
    store_style_images,
)


class ArtisticBaseGan(BaseGan):

    EMPTY_STYLE_VALUE = 0.1

    def __init__(self, model_config, data_config=None):
        super().__init__(model_config)
        self.style_images_path = join(self.model_path, 'style_images')

        if self.model_config.new:
            assert (
                data_config is not None
            ), 'Creating a new ArtisticBaseGan requires a data config'
            assert data_config.fixed_size, 'ArtisticBaseGan requires fixed size images'
            store_style_images(data_config, self.style_images_path)

    def load_style_images(self):
        """
        Load the images used as style references

        Return:
            style_images (dict of tf.Tensors): maps index to style image tensor
        """
        return load_style_images(self.style_images_path)

    def get_style_images_datasets(self):
        """
        Create a couple of datasets from the style images and their indices

        Return:
            style_indices, style_dataset (tuple of tf.data.Datasets)
        """
        style_images = self.load_style_images()
        style_indices = tf.data.Dataset.from_tensor_slices(list(style_images.keys()))
        style_dataset = tf.data.Dataset.from_tensor_slices(list(style_images.values()))

        return style_indices.batch(1), style_dataset.batch(1)

    def get_summary_style_images(self, style_indices, style_dataset):
        """
        Summarize the style images

        Args:
            style_indices (tf.data.Dataset): a dataset of indices for the style images
            style_dataset (tf.data.Dataset): a dataset of style images
        Return:
            summaries_dict: dict mapping summary names to image tensors
        """
        summaries_dict = {
            f'Style {index.numpy()[0]}': tensor2image(image)
            for index, image in tf.data.Dataset.zip((style_indices, style_dataset))
        }
        return summaries_dict

    @abstractmethod
    def get_summary_colour_distributions(self, dataset):
        pass

    def remove_cached_data(self):
        """
        Clean up the cached data from the disk
        """
        shutil.rmtree(join(self.model_path, 'cached_data'), ignore_errors=True)

    def prepare_datasets(self, data_config, training_config):
        """
        Delegate the construction of the datasets for training

        Args:
            data_config (namespace): contains data configurations and paths
            training_config (namespace): contains some training configurations
                                         for the dataset
        Returns:
            training_set, validation_set (tf.data.Dataset objects)
        """
        return prepare_datasets(data_config, training_config)
