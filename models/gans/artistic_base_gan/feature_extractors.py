from tensorflow.keras.layers import Input
from tensorflow.keras.models import Model
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input

from utils.image_processor import tensor2image


class InvalidFeatureLayerIndex(Exception):
    """
    Exception to be raised when the requested feature is unavailable
    due to the particular construction of the feature extractor
    """

    pass


class FeatureExtractor:
    def __init__(self, model, layer_indices, preprocess=lambda x: x):
        self.features = {}
        self.preprocess = preprocess
        self.build(model, layer_indices)

    def build(self, model, layer_indices):
        """
        Build the feature extractors by creating multiple keras functional models
        that share the same base layers

        Args:
            model (tf.keras.Model): the pretrained classifier
            layer_indices (list of int): the layers from which features will be extracted
        """
        inputs = Input(shape=(None, None, 3))
        x = inputs

        for i, layer in enumerate(model.layers[1 : max(layer_indices) + 1], start=1):
            x = layer(x)
            if i in layer_indices:
                self.features[i] = Model(inputs=inputs, outputs=x)

    def __call__(self, images, layer_index):
        """
        Preprocesses the input data and performs the actual feature extraction

        Args:
            images (tf.Tensor): a batch of input images
            layer_index (int): layer level for feature extraction
        Return:
            the corresponding extracted features (tf.Tensor)
        Raise:
            InvalidFeatureLayerIndex, if the level requested is not part of the structure
        """
        try:
            preprocessed_images = self.preprocess(images)
            return self.features[layer_index](preprocessed_images)

        except KeyError:
            raise InvalidFeatureLayerIndex


class VggFeatureExtractor(FeatureExtractor):
    def __init__(self, layer_indices):

        model = VGG16(weights='imagenet', include_top=False)
        model.trainable = False
        preprocess = lambda x: preprocess_input(255.0 * tensor2image(x))

        super().__init__(model, layer_indices, preprocess)
