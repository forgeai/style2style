import numpy as np
import tensorflow as tf
import tensorflow_gan as tfgan
from os.path import join
from tqdm import tqdm, trange
from collections import OrderedDict

from models.gans.cycle_gan.toolset import (
    get_hparams,
    prepare_datasets,
    get_summary_attributes,
)
from models.gans.cycle_gan.generator import (
    cycle_loss,
    identity_loss,
    create_generator,
    get_loss_schedule,
    get_loss_config_from_schedule,
    get_generator_loss_function,
)
from models.gans.cycle_gan.discriminator import (
    create_discriminator,
    discriminator_accuracy,
    get_discriminator_threshold,
    get_discriminator_loss_function,
    discriminator_gradient_penalty,
)
from models.gans.base_gan.model import BaseGan
from models.gans.base_gan.toolset import (
    print_metrics,
    create_optimizer,
    get_dataset_samples_for_summaries,
)
from models.factory.file_handling import copy_checkpoint_folder
from models.factory.component_serialization import save_component, load_component
from models.components.auxiliaries.toolset import get_component_hparams_dict
from utils.image_processor import tensor2image
from utils.json_processor import dict2namespace as d2n
from data_pipeline.factory.toolset import get_dataset_size


class CycleGan(BaseGan):
    def __init__(self, model_config):
        model_config['generator']['objective'] = model_config.objective
        model_config['discriminator']['objective'] = model_config.objective
        super().__init__(model_config)

    def build_model(self):
        """
        Build a new model using the architectural configurations
        """
        self.G_X2Y = create_generator(d2n(self.model_config.generator))
        self.G_Y2X = create_generator(d2n(self.model_config.generator))
        self.D_X = create_discriminator(d2n(self.model_config.discriminator))
        self.D_Y = create_discriminator(d2n(self.model_config.discriminator))

    def load_model(self):
        """
        Load the architecture and the weights of the model's components
        """
        for component_name in ('G_X2Y', 'G_Y2X', 'D_X', 'D_Y'):
            component_path = join(self.checkpoints_path, component_name)
            setattr(self, component_name, load_component(component_path))

    def save_model(self):
        """
        Save all components independently
        """
        for component_name in ('G_X2Y', 'G_Y2X', 'D_X', 'D_Y'):
            component = getattr(self, component_name)
            component_path = join(self.checkpoints_path, component_name)
            if component_name.startswith('G'):
                component_config = self.model_config['generator']
            else:
                component_config = self.model_config['discriminator']

            save_component(component, component_config, component_path)

    def compute_fid(self, dataset):
        """
        Computes the Fréchet Inception Distance between a given collection
        of real images and a given collection of fake images

        Args:
            dataset (tf.data.Dataset): a dataset containing samples from
                                       both distributions
        Return:
            fid dict: maps generator name ('G_X2Y_fid' or 'G_Y2X_fid')
                      to the fid value for its generated samples (float32)
        """

        def tf_batch_appender(tf_list, tf_element):
            if tf_list is None:
                return tf_element
            return tf.concat([tf_list, tf_element], axis=0)

        real_collection_X = None
        real_collection_Y = None
        fake_collection_X = None
        fake_collection_Y = None

        for X, Y in dataset:
            fake_X = self.G_Y2X(Y)
            fake_Y = self.G_X2Y(X)
            real_collection_X = tf_batch_appender(real_collection_X, X)
            real_collection_Y = tf_batch_appender(real_collection_Y, Y)
            fake_collection_X = tf_batch_appender(fake_collection_X, fake_X)
            fake_collection_Y = tf_batch_appender(fake_collection_Y, fake_Y)

        return dict(
            G_Y2X_fid=tfgan.eval.frechet_inception_distance(
                real_collection_X, fake_collection_X
            ).numpy(),
            G_X2Y_fid=tfgan.eval.frechet_inception_distance(
                real_collection_Y, fake_collection_Y
            ).numpy(),
        )

    def get_summary_hparams(self, training_config):
        """
        Create a dictionary of hparams to summarize

        Args:
            training_config (namespace): config file for training
        Return:
            hparams_dict (OrderedDict): maps hparams objects to actual values
        """
        gan_hparams = get_hparams()
        hparams_dict = OrderedDict()

        # cycle_gan general hparams
        hparams_dict[gan_hparams['objective']] = self.model_config['objective']

        # generator hparams
        generator_config = d2n(self.model_config.generator)
        generator_hparams = get_component_hparams_dict(generator_config)
        for hparam, value in generator_hparams.items():
            hparams_dict[gan_hparams[f'g_{hparam}']] = value

        # discriminator hparams
        discriminator_config = d2n(self.model_config.discriminator)
        discriminator_hparams = get_component_hparams_dict(discriminator_config)
        for hparam, value in discriminator_hparams.items():
            hparams_dict[gan_hparams[f'd_{hparam}']] = value

        # training hparams
        for param in ('lambda_adversarial', 'lambda_cycle', 'lambda_identity'):
            hparams_dict[gan_hparams[param]] = training_config.generator[param]
        hparams_dict[
            gan_hparams['lambda_gradient_penalty']
        ] = training_config.discriminator['lambda_gradient_penalty']

        return hparams_dict

    def get_summary_images(self, samples_dataset, include_inputs):
        """
        Create a dictionary of images to summarize

        Args:
            samples_dataset (tf.data.Dataset): a dataset of image pairs
                                               from both distributions
            include_inputs (bool): whether to include images that do not
                                   depend on the model evolution and thus
                                   do not change over the course of epochs
        Return:
            summaries_dict (dict): maps summary names to image tensors
        """
        summaries_dict = {}

        for X, Y in samples_dataset:
            fake_X = self.G_Y2X(Y)
            fake_Y = self.G_X2Y(X)
            cycle_X = self.G_Y2X(fake_Y)
            cycle_Y = self.G_X2Y(fake_X)
            identity_X = self.G_Y2X(X)
            identity_Y = self.G_X2Y(Y)
            heat_map_X, _ = tf.linalg.normalize(self.D_X(X), ord=np.inf)
            heat_map_Y, _ = tf.linalg.normalize(self.D_Y(Y), ord=np.inf)
            heat_map_fake_X, _ = tf.linalg.normalize(self.D_X(fake_X), ord=np.inf)
            heat_map_fake_Y, _ = tf.linalg.normalize(self.D_Y(fake_Y), ord=np.inf)

            X_image = tensor2image(X)
            Y_image = tensor2image(Y)
            fake_X_image = tensor2image(fake_X)
            fake_Y_image = tensor2image(fake_Y)

            if include_inputs:
                summaries_dict['X'] = X_image
                summaries_dict['Y'] = Y_image
            summaries_dict['Fake X'] = fake_X_image
            summaries_dict['Fake Y'] = fake_Y_image
            summaries_dict['Cycle X'] = tensor2image(cycle_X)
            summaries_dict['Cycle Y'] = tensor2image(cycle_Y)
            summaries_dict['Identity X'] = tensor2image(identity_X)
            summaries_dict['Identity Y'] = tensor2image(identity_Y)

            summaries_dict['Heat Map X'] = heat_map_X
            summaries_dict['Heat Map Y'] = heat_map_Y
            summaries_dict['Heat Map Fake X'] = heat_map_fake_X
            summaries_dict['Heat Map Fake Y'] = heat_map_fake_Y
            summaries_dict['Heat Masked X'] = tf.where(
                heat_map_X > self.discriminator_threshold, X_image, 0.0
            )
            summaries_dict['Heat Masked Y'] = tf.where(
                heat_map_Y > self.discriminator_threshold, Y_image, 0.0
            )
            summaries_dict['Heat Masked Fake X'] = tf.where(
                heat_map_fake_X > self.discriminator_threshold, fake_X_image, 0.0
            )
            summaries_dict['Heat Masked Fake Y'] = tf.where(
                heat_map_fake_Y > self.discriminator_threshold, fake_Y_image, 0.0
            )

        return summaries_dict

    def run_through_generators(self, X, Y, training):
        """
        Pass data samples through the pair of generators and evaluate the losses

        Args:
            X (tf.Tensor): a batch of samples from distribution X
            Y (tf.Tensor): a batch of samples from distribution Y
            training (bool): indicates the forward pass is done for training
        Return:
            fake_X (tf.Tensor): the generated X
            fake_Y (tf.Tensor): the generated Y
            loss_dict (dict): contains specific loss values
        """
        loss_dict = {}

        # extract feature maps
        fake_X = self.G_Y2X(Y, training=training)
        fake_Y = self.G_X2Y(X, training=training)
        cycle_X = self.G_Y2X(fake_Y, training=training)
        cycle_Y = self.G_X2Y(fake_X, training=training)
        identity_X = self.G_Y2X(X, training=training)
        identity_Y = self.G_X2Y(Y, training=training)
        fake_X_D_pred = self.D_X(fake_X, training=training)
        fake_Y_D_pred = self.D_Y(fake_Y, training=training)

        # obtain loss components
        loss_dict['X2Y'] = self.generator_loss(fake_Y_D_pred)
        loss_dict['Y2X'] = self.generator_loss(fake_X_D_pred)
        loss_dict['X_cycle'] = cycle_loss(X, cycle_X)
        loss_dict['Y_cycle'] = cycle_loss(Y, cycle_Y)
        loss_dict['X_identity'] = identity_loss(X, identity_X)
        loss_dict['Y_identity'] = identity_loss(Y, identity_Y)

        return fake_X, fake_Y, loss_dict

    def train_generators(self, X, Y, optimizer, loss_config, metrics):
        """
        Train the generators on the samples provided

        Args:
            X (tf.Tensor): a batch of samples from distribution X
            Y (tf.Tensor): a batch of samples from distribution Y
            optimizer (keras.optimizers.Optimizer): the optimizer
            loss_config (namespace): configurations for the generators' loss weighting
            metrics (dict): contains the accumulators for the metrics' evaluation
        Return:
            fake_X (tf.Tensor): the style transferred version of Y
            fake_Y (tf.Tensor): the style transferred version of X
        """
        with tf.GradientTape() as tape:
            fake_X, fake_Y, loss_dict = self.run_through_generators(X, Y, training=True)

            cycle_loss = (loss_dict['X_cycle'] + loss_dict['Y_cycle']) / 2
            identity_loss = (loss_dict['X_identity'] + loss_dict['Y_identity']) / 2
            G_X2Y_loss = (
                loss_config.lambda_adversarial * loss_dict['X2Y']
                + loss_config.lambda_cycle * cycle_loss
                + loss_config.lambda_identity * loss_dict['X_identity']
            )
            G_Y2X_loss = (
                loss_config.lambda_adversarial * loss_dict['Y2X']
                + loss_config.lambda_cycle * cycle_loss
                + loss_config.lambda_identity * loss_dict['Y_identity']
            )
            G_loss = G_X2Y_loss + G_Y2X_loss

        # apply gradients
        trainable_variables = (
            self.G_X2Y.trainable_variables + self.G_Y2X.trainable_variables
        )
        gradients = tape.gradient(G_loss, trainable_variables)
        optimizer.apply_gradients(zip(gradients, trainable_variables))

        metrics['trn_G_cycle'](cycle_loss)
        metrics['trn_G_identity'](identity_loss)
        metrics['trn_G_adversarial'](loss_dict['X2Y'] + loss_dict['Y2X'])
        metrics['trn_G_X2Y'](G_X2Y_loss)
        metrics['trn_G_Y2X'](G_Y2X_loss)
        metrics['trn_G_total'](G_loss)
        return fake_X, fake_Y

    def validate_generators(self, X, Y, loss_config, metrics):
        """
        Validate the generators on the development set

        Args:
            X (tf.Tensor): a batch of samples from distribution X
            Y (tf.Tensor): a batch of samples from distribution Y
            loss_config (namespace): configurations for the generators' loss weighting
            metrics (dict): contains the accumulators for the metrics' evaluation
        Return:
            fake_X (tf.Tensor): the style transferred version of Y
            fake_Y (tf.Tensor): the style transferred version of X
        """
        fake_X, fake_Y, loss_dict = self.run_through_generators(X, Y, training=False)

        cycle_loss = (loss_dict['X_cycle'] + loss_dict['Y_cycle']) / 2
        identity_loss = (loss_dict['X_identity'] + loss_dict['Y_identity']) / 2
        G_X2Y_loss = (
            loss_config.lambda_adversarial * loss_dict['X2Y']
            + loss_config.lambda_cycle * cycle_loss
            + loss_config.lambda_identity * loss_dict['X_identity']
        )
        G_Y2X_loss = (
            loss_config.lambda_adversarial * loss_dict['Y2X']
            + loss_config.lambda_cycle * cycle_loss
            + loss_config.lambda_identity * loss_dict['Y_identity']
        )
        G_loss = G_X2Y_loss + G_Y2X_loss

        metrics['val_G_cycle'](cycle_loss)
        metrics['val_G_identity'](identity_loss)
        metrics['val_G_adversarial'](loss_dict['X2Y'] + loss_dict['Y2X'])
        metrics['val_G_X2Y'](G_X2Y_loss)
        metrics['val_G_Y2X'](G_Y2X_loss)
        metrics['val_G_total'](G_loss)
        return fake_X, fake_Y

    def run_through_discriminators(
        self, real_X, real_Y, fake_X, fake_Y, compute_interpolated_gradients, training
    ):
        """
        Pass data samples through the pair of discriminators and evaluate the losses

        Args:
            real_X (tf.Tensor): a batch of samples from the real distribution X
            real_Y (tf.Tensor): a batch of samples from the real distribution Y
            fake_X (tf.Tensor): a batch of samples from the generated X
            fake_Y (tf.Tensor): a batch of samples from the generated Y
            compute_interpolated_gradients (bool): whether to compute the gradients on
                                                   real-fake interpolated samples
            training (bool): indicates the forward pass is done for training
        Return:
            loss_dict (dict): contains specific loss values
        """
        loss_dict = {}

        # extract feature maps
        real_X_pred = self.D_X(real_X, training=training)
        fake_X_pred = self.D_X(fake_X, training=training)
        real_Y_pred = self.D_Y(real_Y, training=training)
        fake_Y_pred = self.D_Y(fake_Y, training=training)

        # obtain loss and accuracy components
        loss_dict['D_X'] = self.discriminator_loss(real_X_pred, fake_X_pred)
        loss_dict['D_Y'] = self.discriminator_loss(real_Y_pred, fake_Y_pred)
        loss_dict['D_X_acc'] = discriminator_accuracy(
            real_X_pred, fake_X_pred, self.discriminator_threshold
        )
        loss_dict['D_Y_acc'] = discriminator_accuracy(
            real_Y_pred, fake_Y_pred, self.discriminator_threshold
        )

        # interpolated samples gradients
        if compute_interpolated_gradients:
            alpha = tf.random.uniform((1,), dtype=tf.float32)
            interpolated_X = alpha * real_X + (1.0 - alpha) * fake_X
            interpolated_Y = alpha * real_Y + (1.0 - alpha) * fake_Y

            inter_X_pred = self.D_X(interpolated_X, training=training)
            inter_Y_pred = self.D_Y(interpolated_Y, training=training)
            gradients_D_X = tf.gradients(inter_X_pred, interpolated_X)
            gradients_D_Y = tf.gradients(inter_Y_pred, interpolated_Y)

            loss_dict['D_X_gp'] = discriminator_gradient_penalty(gradients_D_X[0])
            loss_dict['D_Y_gp'] = discriminator_gradient_penalty(gradients_D_Y[0])

        return loss_dict

    def train_discriminators(
        self, real_X, real_Y, fake_X, fake_Y, optimizer, loss_config, metrics
    ):
        """
        Train the discriminators on the samples provided

        Args:
            real_X (tf.Tensor): a batch of samples from the real distribution X
            real_Y (tf.Tensor): a batch of samples from the real distribution Y
            fake_X (tf.Tensor): a batch of samples from the generated X
            fake_Y (tf.Tensor): a batch of samples from the generated Y
            optimizer (keras.optimizers.Optimizer): the optimizer
            loss_config (namespace): configurations for the discriminators' loss weighting
            metrics (dict): contains the accumulators for the metrics' evaluation
        """
        do_gp = loss_config.lambda_gradient_penalty > 0

        with tf.GradientTape() as tape:
            loss_dict = self.run_through_discriminators(
                real_X, real_Y, fake_X, fake_Y, do_gp, training=True
            )
            D_loss = loss_dict['D_X'] + loss_dict['D_Y']
            D_total_loss = D_loss
            if do_gp:
                D_gp = loss_dict['D_X_gp'] + loss_dict['D_Y_gp']
                D_total_loss += loss_config.lambda_gradient_penalty * D_gp

        D_acc = (loss_dict['D_X_acc'] + loss_dict['D_Y_acc']) / 2

        # apply gradients
        trainable_variables = self.D_X.trainable_variables + self.D_Y.trainable_variables
        gradients = tape.gradient(D_total_loss, trainable_variables)
        optimizer.apply_gradients(zip(gradients, trainable_variables))

        metrics['trn_D_X_loss'](loss_dict['D_X'])
        metrics['trn_D_Y_loss'](loss_dict['D_Y'])
        metrics['trn_D_total_loss'](D_total_loss)
        metrics['trn_D_X_acc'](loss_dict['D_X_acc'])
        metrics['trn_D_Y_acc'](loss_dict['D_Y_acc'])
        metrics['trn_D_total_acc'](D_acc)
        if do_gp:
            metrics['trn_D_loss'](D_loss)
            metrics['trn_D_X_gp'](loss_dict['D_X_gp'])
            metrics['trn_D_Y_gp'](loss_dict['D_Y_gp'])
            metrics['trn_D_gp'](D_gp)

    def validate_discriminators(
        self, real_X, real_Y, fake_X, fake_Y, loss_config, metrics
    ):
        """
        Validate the discriminators on the development set

        Args:
            real_X (tf.Tensor): a batch of samples from the real distribution X
            real_Y (tf.Tensor): a batch of samples from the real distribution Y
            fake_X (tf.Tensor): a batch of samples from the generated X
            fake_Y (tf.Tensor): a batch of samples from the generated Y
            loss_config (namespace): configurations for the discriminators' loss weighting
            metrics (dict): contains the accumulators for the metrics' evaluation
        """
        do_gp = loss_config.lambda_gradient_penalty > 0

        loss_dict = self.run_through_discriminators(
            real_X, real_Y, fake_X, fake_Y, do_gp, training=False
        )
        D_loss = loss_dict['D_X'] + loss_dict['D_Y']
        D_total_loss = D_loss
        if do_gp:
            D_gp = loss_dict['D_X_gp'] + loss_dict['D_Y_gp']
            D_total_loss += loss_config.lambda_gradient_penalty * D_gp

        D_acc = (loss_dict['D_X_acc'] + loss_dict['D_Y_acc']) / 2

        metrics['val_D_X_loss'](loss_dict['D_X'])
        metrics['val_D_Y_loss'](loss_dict['D_Y'])
        metrics['val_D_total_loss'](D_total_loss)
        metrics['val_D_X_acc'](loss_dict['D_X_acc'])
        metrics['val_D_Y_acc'](loss_dict['D_Y_acc'])
        metrics['val_D_total_acc'](D_acc)
        if do_gp:
            metrics['val_D_loss'](D_loss)
            metrics['val_D_X_gp'](loss_dict['D_X_gp'])
            metrics['val_D_Y_gp'](loss_dict['D_Y_gp'])
            metrics['val_D_gp'](D_gp)

    @tf.function
    def train_step(
        self, X, Y, G_optimizer, D_optimizer, G_loss_config, D_loss_config, metrics
    ):
        """
        Perform an iteration of training by first doing an optimization step
        for the generators and then for the discriminators

        Args:
            X (tf.Tensor): a batch of samples from distribution X
            Y (tf.Tensor): a batch of samples from distribution Y
            G_optimizer (keras.optimizers.Optimizer): the optimizer for generators
            D_optimizer (keras.optimizers.Optimizer): the optimizer for discriminators
            G_loss_config (namespace): configs for the generators' loss weighting
            D_loss_config (namespace): configs for the discriminators' loss weighting
            metrics (dict): contains the accumulators for the metrics' evaluation
        """
        fake_X, fake_Y = self.train_generators(X, Y, G_optimizer, G_loss_config, metrics)
        self.train_discriminators(
            X, Y, fake_X, fake_Y, D_optimizer, D_loss_config, metrics
        )

    @tf.function
    def validation_step(self, X, Y, G_loss_config, D_loss_config, metrics):
        """
        Perform an iteration of validation on the development data

        Args:
            X (tf.Tensor): a batch of samples from distribution X
            Y (tf.Tensor): a batch of samples from distribution Y
            G_loss_config (namespace): configs for the generators' loss weighting
            D_loss_config (namespace): configs for the discriminators' loss weighting
            metrics (dict): contains the accumulators for the metrics' evaluation
        """
        fake_X, fake_Y = self.validate_generators(X, Y, G_loss_config, metrics)
        self.validate_discriminators(X, Y, fake_X, fake_Y, D_loss_config, metrics)

    def train(self, training_config, training_set, validation_set):
        """
        Perform the training process

        Args:
            training_config (namespace): contains configurations for training
            training_set (tf.data.Dataset): the training set
            validation_set (tf.data.Dataset): the validation set
        """
        G_config = d2n(training_config.generator)
        D_config = d2n(training_config.discriminator)
        G_optimizer = create_optimizer(G_config, training_config.epochs)
        D_optimizer = create_optimizer(D_config, training_config.epochs)
        G_loss_schedule = get_loss_schedule(G_config)

        summary = d2n(training_config.summary)
        checkpoint_copy = d2n(training_config.checkpoint_copy)

        # set default (null) values for summary configs if not provided
        for attribute in get_summary_attributes():
            setattr(summary, attribute, getattr(summary, attribute, 0))

        if summary.graph:
            tf.summary.trace_on(graph=True, profiler=True)
            summarized_graph_already = False
        if summary.hparams:
            self.initialize_hparams(
                list(self.get_summary_hparams(training_config).keys()),
                metric='G_fid',
                display_name='FID',
            )
        if summary.fid_frequency:
            trn_fid = {}
            val_fid = {}

        # collect data samples for summaries
        training_summary_set = get_dataset_samples_for_summaries(
            training_set, ([None, None, 3], [None, None, 3]), summary.image_samples
        )
        validation_summary_set = get_dataset_samples_for_summaries(
            validation_set, ([None, None, 3], [None, None, 3]), summary.image_samples
        )

        self.generator_loss = get_generator_loss_function(self.model_config.objective)
        self.discriminator_loss = get_discriminator_loss_function(
            self.model_config.objective
        )
        self.discriminator_threshold = get_discriminator_threshold(
            self.model_config.objective
        )
        D_loss_config = d2n(
            dict(lambda_gradient_penalty=D_config.lambda_gradient_penalty)
        )

        training_data_size = get_dataset_size(training_set)
        validation_data_size = get_dataset_size(validation_set)

        metrics_to_monitor = [
            'trn_G_cycle',
            'trn_G_identity',
            'trn_G_adversarial',
            'trn_G_X2Y',
            'trn_G_Y2X',
            'trn_G_total',
            'trn_D_X_loss',
            'trn_D_Y_loss',
            'trn_D_total_loss',
            'trn_D_X_acc',
            'trn_D_Y_acc',
            'trn_D_total_acc',
            'val_G_cycle',
            'val_G_identity',
            'val_G_adversarial',
            'val_G_X2Y',
            'val_G_Y2X',
            'val_G_total',
            'val_D_X_loss',
            'val_D_Y_loss',
            'val_D_total_loss',
            'val_D_X_acc',
            'val_D_Y_acc',
            'val_D_total_acc',
        ]
        if D_config.lambda_gradient_penalty > 0:
            metrics_to_monitor.extend(
                [
                    'trn_D_loss',
                    'trn_D_X_gp',
                    'trn_D_Y_gp',
                    'trn_D_gp',
                    'val_D_loss',
                    'val_D_X_gp',
                    'val_D_Y_gp',
                    'val_D_gp',
                ]
            )
        metrics = {
            metric_name: tf.keras.metrics.Mean(metric_name, dtype=tf.float32)
            for metric_name in metrics_to_monitor
        }

        # start the training iterations
        starting_epoch = self.epoch.value()
        for epoch in trange(
            starting_epoch,
            starting_epoch + training_config.epochs,
            desc='{:^21}'.format('Training Progress'),
        ):
            self.epoch.assign(epoch)

            # get current loss weightings for the generator
            G_loss_config = get_loss_config_from_schedule(G_loss_schedule)

            # train
            for X, Y in tqdm(
                training_set,
                desc='{:<21}'.format('Epoch {} (training)'.format(epoch)),
                total=training_data_size,
            ):
                self.train_step(
                    X, Y, G_optimizer, D_optimizer, G_loss_config, D_loss_config, metrics
                )
                if summary.graph and not summarized_graph_already:
                    self.summarize_graph()
                    summarized_graph_already = True

            # validate
            for X, Y in tqdm(
                validation_set,
                desc='{:<21}'.format('Epoch {} (validation)'.format(epoch)),
                total=validation_data_size,
            ):
                self.validation_step(X, Y, G_loss_config, D_loss_config, metrics)

            # collect metrics for display
            display_training_dict = dict(
                gen_loss=metrics['trn_G_total'].result().numpy(),
                dsc_loss=metrics['trn_D_total_loss'].result().numpy(),
                dsc_acc=metrics['trn_D_total_acc'].result().numpy(),
            )
            display_validation_dict = dict(
                gen_loss=metrics['val_G_total'].result().numpy(),
                dsc_loss=metrics['val_D_total_loss'].result().numpy(),
                dsc_acc=metrics['val_D_total_acc'].result().numpy(),
            )

            # compute frechet inception distance and write its summaries/displays
            if summary.fid_frequency and summary.fid_batches:
                if epoch % summary.fid_frequency == 0 and (
                    epoch > 0 or not summary.fid_ignore_0
                ):
                    trn_fid = self.compute_fid(training_set.take(summary.fid_batches))
                    val_fid = self.compute_fid(validation_set.take(summary.fid_batches))
                    trn_fid['G_fid'] = sum(trn_fid.values()) / 2
                    val_fid['G_fid'] = sum(val_fid.values()) / 2
                    self.summarize_scalars(trn_fid, 'training', epoch)
                    self.summarize_scalars(val_fid, 'validation', epoch)

                try:
                    display_training_dict['gen_fid'] = trn_fid['G_fid']
                    display_validation_dict['gen_fid'] = val_fid['G_fid']
                except KeyError:
                    pass

            # display metrics
            print_metrics(display_training_dict, display_validation_dict)

            # write hyperparameters summaries for tensorboard
            if summary.hparams:
                self.summarize_hparams(self.get_summary_hparams(training_config))

            # write scalar summaries for tensorboard
            training_scalars = {
                key[4:]: value.result()
                for key, value in metrics.items()
                if key.startswith('trn')
            }
            validation_scalars = {
                key[4:]: value.result()
                for key, value in metrics.items()
                if key.startswith('val')
            }
            self.summarize_scalars(training_scalars, 'training', epoch)
            self.summarize_scalars(validation_scalars, 'validation', epoch)

            # write image summaries for tensorboard
            if summary.image_samples_frequency:
                if epoch % summary.image_samples_frequency == 0:
                    training_images = self.get_summary_images(
                        training_summary_set, epoch == 0
                    )
                    validation_images = self.get_summary_images(
                        validation_summary_set, epoch == 0
                    )
                    self.summarize_images(training_images, 'training', epoch)
                    self.summarize_images(validation_images, 'validation', epoch)

            # store checkpoints
            self.save_epoch()
            self.save_model()

            if (
                checkpoint_copy.enabled
                and epoch >= checkpoint_copy.starting_epoch
                and epoch % checkpoint_copy.frequency == 0
            ):
                copy_checkpoint_folder(self.model_path, self.checkpoints_path, epoch)

            # reset loss accumulators
            for metric_tensor in metrics.values():
                metric_tensor.reset_states()

    def prepare_datasets(self, data_config, training_config):
        """
        Delegate the construction of the datasets for training

        Args:
            data_config (namespace): contains data configurations and paths
            training_config (namespace): contains some training configurations
                                         for the dataset
        Returns:
            training_set, validation_set (tf.data.Dataset objects)
        """
        return prepare_datasets(data_config, training_config)
