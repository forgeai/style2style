import os
import logging
from collections import OrderedDict
from tensorboard.plugins.hparams import api as hp

from data_pipeline.processing.load import optimize_loading
from data_pipeline.processing.extract import tfrecords2dataset
from data_pipeline.processing.transform import (
    arrange_double_dataset_for_training,
    arrange_double_dataset_for_validation,
)
from data_pipeline.factory.data_creation_pipeline import perform_entire_pipeline
from utils.constants import OBJECTIVES, UPSAMPLING_METHODS, NON_CONDITIONAL_COMPONENTS


def get_summary_attributes():
    """
    Return all the attributes that are part of the summarization for the CycleGan

    Return:
        attributes (tuple of str)
    """
    return (
        'graph',
        'hparams',
        'image_samples',
        'image_samples_frequency',
        'fid_batches',
        'fid_ignore_0',
        'fid_frequency',
    )


def get_hparams():
    """
    Return a dictionary of all hyperparameters' ranges for the dashboard

    Return:
        (OrderedDict): maps hyperparameter name to a hp.HParam object
    """
    return OrderedDict(
        objective=hp.HParam('objective', hp.Discrete(OBJECTIVES)),
        g_type=hp.HParam('g_type', hp.Discrete(NON_CONDITIONAL_COMPONENTS)),
        d_type=hp.HParam('d_type', hp.Discrete(NON_CONDITIONAL_COMPONENTS)),
        g_dropout=hp.HParam('g_dropout', hp.RealInterval(0.0, 1.0)),
        d_dropout=hp.HParam('d_dropout', hp.RealInterval(0.0, 1.0)),
        g_l2_reg=hp.HParam('g_l2_reg', hp.RealInterval(0.0, 10.0)),
        d_l2_reg=hp.HParam('d_l2_reg', hp.RealInterval(0.0, 10.0)),
        g_upsampling=hp.HParam('g_upsampling', hp.Discrete(UPSAMPLING_METHODS)),
        d_upsampling=hp.HParam('d_upsampling', hp.Discrete(UPSAMPLING_METHODS)),
        lambda_adversarial=hp.HParam('lambda_adversarial', hp.RealInterval(0.0, 3.0)),
        lambda_cycle=hp.HParam('lambda_cycle', hp.RealInterval(0.0, 3.0)),
        lambda_identity=hp.HParam('lambda_identity', hp.RealInterval(0.0, 3.0)),
        lambda_gradient_penalty=hp.HParam(
            'lambda_gradient_penalty', hp.RealInterval(0.0, 1e5)
        ),
    )


def prepare_datasets(data_config, training_config):
    """
    Construct the datasets for training

    Args:
        data_config (namespace): contains data configurations and paths
        training_config (namespace): contains training configurations for the dataset
    Return:
        training_set, validation_set (tf.data.Dataset objects)
    """
    logger = logging.getLogger(__name__)

    # create/refine data
    tfrecords_x_path = perform_entire_pipeline(
        data_config.dataset_x_path,
        data_config.splits,
        data_config.min_size,
        data_config.max_size,
        data_config.fixed_size,
        data_config.fixed_mode,
    )
    tfrecords_y_path = perform_entire_pipeline(
        data_config.dataset_y_path,
        data_config.splits,
        data_config.min_size,
        data_config.max_size,
        data_config.fixed_size,
        data_config.fixed_mode,
    )

    # extract data
    logger.info('Loading datasets...')
    dataset_x = tfrecords2dataset(
        tfrecords_x_path, ('training', 'validation'), training_config.overlapping_datasets
    )
    logger.info('\tLoaded {}'.format(os.path.basename(tfrecords_x_path)))
    dataset_y = tfrecords2dataset(
        tfrecords_y_path, ('training', 'validation'), training_config.overlapping_datasets
    )
    logger.info('\tLoaded {}'.format(os.path.basename(tfrecords_y_path)))

    # transform data
    logger.info('Preparing datasets for training...')
    training_set = arrange_double_dataset_for_training(
        dataset_x['training'], dataset_y['training'], training_config
    )
    validation_set = arrange_double_dataset_for_validation(
        dataset_x['validation'], dataset_y['validation'], training_config
    )

    # load data
    training_set = optimize_loading(training_set)
    validation_set = optimize_loading(validation_set)

    return training_set, validation_set
