import tensorflow as tf
import tensorflow.keras as keras
from static_variables import resolve_static

from utils.constants import OBJECTIVES
from models.factory.type_loader import get_model_type


def create_discriminator(config):
    """
    Create an instance of a discriminator

    Args:
        config (namespace): contains layers parameterization
    Return:
        a discriminator (tf.keras.Model)
    """
    assert (
        config.objective in OBJECTIVES
    ), f'Optimization objective must be one of {OBJECTIVES}'

    ComponentClass = get_model_type(config.type, is_component=True)
    config.output_activation = (
        'sigmoid' if config.objective == 'binary_crossentropy' else None
    )
    component_creator = ComponentClass(config, 'cyclegan_discriminator')

    return component_creator.create_model()


def get_discriminator_loss_function(objective):
    """
    Return a functor with the appropariate loss function depending
    on the overall objective of the gan

    Args:
        objective (str): optimization objective (loss function type)
    Return:
        discriminator loss function
    """
    assert objective in OBJECTIVES, f'Optimization objective must be one of {OBJECTIVES}'

    if objective == 'binary_crossentropy':
        return discriminator_binary_crossentropy_loss
    elif objective == 'mean_squared_error':
        return discriminator_mean_squared_error
    return discriminator_wasserstein_loss


def get_discriminator_threshold(objective):
    """
    Return the prediction threshold between real and fake depending
    on the overall objective of the gan

    Args:
        objective (str): optimization objective (loss function type)
    Return:
        threshold (float)
    """
    assert objective in OBJECTIVES, f'Optimization objective must be one of {OBJECTIVES}'

    if objective == 'wasserstein_distance':
        return 0
    return 0.5


@resolve_static(
    static_variables={'bce': keras.losses.BinaryCrossentropy(from_logits=False)}
)
def discriminator_binary_crossentropy_loss(real_pred, fake_pred):
    """
    Compute the binary crossentropy loss value for the discriminator
    given a pair of predictions on real and generated image tensors

    Args:
        real_pred (tf.Tensor): discriminator output for an authentic batch
        fake_pred (tf.Tensor): discriminator output for a generated batch
    Return:
        loss value (tf.float32)
    """
    return bce(tf.ones_like(real_pred), real_pred) + bce(
        tf.zeros_like(fake_pred), fake_pred
    )


@resolve_static(static_variables={'mse': tf.keras.losses.MeanSquaredError()})
def discriminator_mean_squared_error(real_pred, fake_pred):
    """
    Compute the mean squared error value for the discriminator
    given a pair of predictions on real and generated image tensors

    Args:
        real_pred (tf.Tensor): discriminator output for an authentic batch
        fake_pred (tf.Tensor): discriminator output for a generated batch
    Return:
        loss value (tf.float32)
    """
    return mse(tf.ones_like(real_pred), real_pred) + mse(
        tf.zeros_like(fake_pred), fake_pred
    )


def discriminator_wasserstein_loss(real_pred, fake_pred):
    """
    Compute the wasserstein loss value for the discriminator
    given a pair of predictions on real and generated image tensors

    Args:
        real_pred (tf.Tensor): discriminator output for an authentic batch
        fake_pred (tf.Tensor): discriminator output for a generated batch
    Return:
        loss value (tf.float32)
    """
    return tf.reduce_mean(fake_pred) - tf.reduce_mean(real_pred)


@resolve_static(static_variables={'gradients_mse': tf.keras.losses.MeanSquaredError()})
def discriminator_gradient_penalty(gradients):
    """
    Compute the divergence from the unit vectors for the discriminator's gradients

    Args:
        gradients (tf.Tensor): the gradients of the discriminator with respect to
                               some image tensors
    Return:
        loss value (tf.float32)
    """
    flat_gradients = tf.reshape(gradients, (gradients.shape[0], -1))
    gradients_norm = tf.norm(flat_gradients, ord='euclidean', axis=-1)

    return gradients_mse(tf.ones_like(gradients_norm), gradients_norm)


def discriminator_accuracy(real_pred, fake_pred, threshold):
    """
    Compute the accuracy value for the discriminator
    given a pair of predictions on real and generated image tensors

    Args:
        real_pred (tf.Tensor): discriminator output for an authentic batch
        fake_pred (tf.Tensor): discriminator output for a generated batch
        threshold (float): the threshold for separating real and fake predictions
    Return:
        accuracy value (tf.float32)
    """
    real_acc = tf.reduce_mean(
        keras.metrics.binary_accuracy(tf.ones_like(real_pred), real_pred, threshold)
    )
    fake_acc = tf.reduce_mean(
        keras.metrics.binary_accuracy(tf.zeros_like(fake_pred), fake_pred, threshold)
    )
    return (real_acc + fake_acc) / 2
