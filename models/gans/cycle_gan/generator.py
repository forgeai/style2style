import tensorflow as tf
import tensorflow.keras as keras
from static_variables import resolve_static

from utils.constants import OBJECTIVES
from utils.json_processor import dict2namespace
from models.gans.base_gan.toolset import DynamicValue
from models.factory.type_loader import get_model_type


def create_generator(config):
    """
    Create an instance of a generator

    Args:
        config (namespace): contains layers parameterization
    Return:
        a generator (tf.keras.Model)
    """
    ComponentClass = get_model_type(config.type, is_component=True)
    config.output_activation = 'tanh'
    component_creator = ComponentClass(config, 'cyclegan_generator')

    return component_creator.create_model()


def get_loss_schedule(training_config):
    """
    Create a dynamic value configuration for the loss weightings

    Args:
        training_config (namespace): contains loss weighting schedules
    Return:
        loss_schedule (namespace): maps loss component names
                                   to DynamicValue objects
    """
    loss_schedule = {}
    for loss_lambda in ('lambda_adversarial', 'lambda_cycle', 'lambda_identity'):
        loss_lambda_schedule = getattr(training_config, loss_lambda)
        if not isinstance(loss_lambda_schedule, list):
            loss_lambda_schedule = [loss_lambda_schedule, loss_lambda_schedule, 0]
        loss_schedule[loss_lambda] = DynamicValue(*loss_lambda_schedule)
    return dict2namespace(loss_schedule)


def get_loss_config_from_schedule(loss_schedule):
    """
    Get the configuration values for the loss weightings in the current iteration
    As a side effect, the scheduler will update its values

    Args:
        loss_schedule (namespace): maps loss component names
                                   to DynamicValue objects
    Return:
        loss_config (namespace): maps loss component names
                                 to floating point values
    """
    loss_config = {}
    for loss_lambda in ('lambda_adversarial', 'lambda_cycle', 'lambda_identity'):
        loss_config[loss_lambda] = getattr(loss_schedule, loss_lambda)()
    return dict2namespace(loss_config)


def get_generator_loss_function(objective):
    """
    Return a functor with the appropariate loss function depending
    on the overall objective of the gan

    Args:
        objective (str): optimization objective (loss function type)
    Return:
        generator loss function
    """
    assert objective in OBJECTIVES, f'Optimization objective must be one of {OBJECTIVES}'

    if objective == 'binary_crossentropy':
        return generator_binary_crossentropy_loss
    elif objective == 'mean_squared_error':
        return generator_mean_squared_error
    return generator_wasserstein_loss


@resolve_static(
    static_variables={'bce': keras.losses.BinaryCrossentropy(from_logits=False)}
)
def generator_binary_crossentropy_loss(fake_pred):
    """
    Compute the binary crossentropy loss value for the generator
    given a discriminator prediction on a generated image tensor

    Args:
        fake_pred (tf.Tensor): discriminator output for a generated batch
    Return:
        loss value (tf.float32)
    """
    return bce(tf.ones_like(fake_pred), fake_pred)


@resolve_static(static_variables={'mse': tf.keras.losses.MeanSquaredError()})
def generator_mean_squared_error(fake_pred):
    """
    Compute the mean squared error value for the generator
    given a discriminator prediction on a generated image tensor

    Args:
        fake_pred (tf.Tensor): discriminator output for a generated batch
    Return:
        loss value (tf.float32)
    """
    return mse(tf.ones_like(fake_pred), fake_pred)


def generator_wasserstein_loss(fake_pred):
    """
    Compute the wasserstein loss value for the generator
    given a discriminator prediction on a generated image tensor

    Args:
        fake_pred (tf.Tensor): discriminator output for a generated batch
    Return:
        loss value (tf.float32)
    """
    return -tf.reduce_mean(fake_pred)


@resolve_static(static_variables={'mae': keras.losses.MeanAbsoluteError()})
def cycle_loss(X1, X2):
    """
    Compute the cycle consistency loss for the generator

    Args:
        X1 (tf.Tensor): an image Tensor batch
        X2 (tf.Tensor): an image Tensor batch
    Return:
        loss value (tf.float32)
    """
    return mae(X1, X2)


@resolve_static(static_variables={'mae': keras.losses.MeanAbsoluteError()})
def identity_loss(X1, X2):
    """
    Compute the identity loss for the generator

    Args:
        X1 (tf.Tensor): an image Tensor batch
        X2 (tf.Tensor): an image Tensor batch
    Return:
        loss value (tf.float32)
    """
    return mae(X1, X2)
