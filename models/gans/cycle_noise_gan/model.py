import tensorflow as tf
from tqdm import tqdm, trange

from utils.json_processor import dict2namespace as d2n
from models.factory.file_handling import copy_checkpoint_folder
from models.gans.base_gan.toolset import (
    print_metrics,
    create_optimizer,
    get_dataset_samples_for_summaries,
)
from models.gans.cycle_gan.model import CycleGan
from models.gans.cycle_gan.toolset import get_summary_attributes
from models.gans.cycle_gan.generator import (
    get_loss_schedule,
    get_loss_config_from_schedule,
    get_generator_loss_function,
)
from models.gans.cycle_gan.discriminator import (
    get_discriminator_threshold,
    get_discriminator_loss_function,
)
from models.gans.cycle_noise_gan.toolset import prepare_datasets


class CycleNoiseGan(CycleGan):
    def __init__(self, model_config):
        super().__init__(model_config)

    def train(self, training_config, training_set, validation_set):
        """
        Perform the training process

        Args:
            training_config (namespace): contains configurations for training
            training_set (tf.data.Dataset): the training set
            validation_set (tf.data.Dataset): the validation set
        """
        G_config = d2n(training_config.generator)
        D_config = d2n(training_config.discriminator)
        G_optimizer = create_optimizer(G_config, training_config.epochs)
        D_optimizer = create_optimizer(D_config, training_config.epochs)
        G_loss_schedule = get_loss_schedule(G_config)

        summary = d2n(training_config.summary)
        checkpoint_copy = d2n(training_config.checkpoint_copy)

        # set default (null) values for summary configs if not provided
        for attribute in get_summary_attributes():
            setattr(summary, attribute, getattr(summary, attribute, 0))

        if summary.graph:
            tf.summary.trace_on(graph=True, profiler=True)
            summarized_graph_already = False
        if summary.hparams:
            self.initialize_hparams(
                list(self.get_summary_hparams(training_config).keys()),
                metric='G_fid',
                display_name='FID',
            )
        if summary.fid_frequency:
            trn_fid = {}
            val_fid = {}

        # collect data samples for summaries
        training_summary_set = get_dataset_samples_for_summaries(
            training_set, ([None, None, 3], [None, None, 3]), summary.image_samples
        )
        validation_summary_set = get_dataset_samples_for_summaries(
            validation_set, ([None, None, 3], [None, None, 3]), summary.image_samples
        )

        self.generator_loss = get_generator_loss_function(self.model_config.objective)
        self.discriminator_loss = get_discriminator_loss_function(
            self.model_config.objective
        )
        self.discriminator_threshold = get_discriminator_threshold(
            self.model_config.objective
        )

        training_data_size = 0
        validation_data_size = 0

        metrics_to_monitor = [
            'trn_G_cycle',
            'trn_G_identity',
            'trn_G_adversarial',
            'trn_G_X2Y',
            'trn_G_Y2X',
            'trn_G_total',
            'trn_D_X_loss',
            'trn_D_Y_loss',
            'trn_D_total_loss',
            'trn_D_X_acc',
            'trn_D_Y_acc',
            'trn_D_total_acc',
            'val_G_cycle',
            'val_G_identity',
            'val_G_adversarial',
            'val_G_X2Y',
            'val_G_Y2X',
            'val_G_total',
            'val_D_X_loss',
            'val_D_Y_loss',
            'val_D_total_loss',
            'val_D_X_acc',
            'val_D_Y_acc',
            'val_D_total_acc',
        ]
        if D_config.lambda_gradient_penalty > 0:
            metrics_to_monitor.extend(
                [
                    'trn_D_loss',
                    'trn_D_X_gp',
                    'trn_D_Y_gp',
                    'trn_D_gp',
                    'val_D_loss',
                    'val_D_X_gp',
                    'val_D_Y_gp',
                    'val_D_gp',
                ]
            )
        metrics = {
            metric_name: tf.keras.metrics.Mean(metric_name, dtype=tf.float32)
            for metric_name in metrics_to_monitor
        }

        # start the training iterations
        starting_epoch = self.epoch.value()
        for epoch in trange(
            starting_epoch,
            starting_epoch + training_config.epochs,
            desc='{:^21}'.format('Training Progress'),
        ):
            self.epoch.assign(epoch)

            # get current loss weightings for the generator
            G_loss_config = get_loss_config_from_schedule(G_loss_schedule)

            # train
            training_size_counter = 0
            for X, Y in tqdm(
                training_set,
                desc='{:<21}'.format('Epoch {} (training)'.format(epoch)),
                total=training_data_size,
            ):
                self.train_step(
                    X, Y, G_optimizer, D_optimizer, G_loss_config, D_config, metrics
                )
                if summary.graph and not summarized_graph_already:
                    self.summarize_graph()
                    summarized_graph_already = True
                training_size_counter += 1

            training_data_size = training_size_counter

            # validate
            validation_size_counter = 0
            for X, Y in tqdm(
                validation_set,
                desc='{:<21}'.format('Epoch {} (validation)'.format(epoch)),
                total=validation_data_size,
            ):
                self.validation_step(X, Y, G_loss_config, D_config, metrics)
                validation_size_counter += 1

            validation_data_size = validation_size_counter

            # collect metrics for display
            display_training_dict = dict(
                gen_loss=metrics['trn_G_total'].result().numpy(),
                dsc_loss=metrics['trn_D_total_loss'].result().numpy(),
                dsc_acc=metrics['trn_D_total_acc'].result().numpy(),
            )
            display_validation_dict = dict(
                gen_loss=metrics['val_G_total'].result().numpy(),
                dsc_loss=metrics['val_D_total_loss'].result().numpy(),
                dsc_acc=metrics['val_D_total_acc'].result().numpy(),
            )

            # compute frechet inception distance and write its summaries/displays
            if summary.fid_frequency and summary.fid_batches:
                if epoch % summary.fid_frequency == 0 and (
                    epoch > 0 or not summary.fid_ignore_0
                ):
                    trn_fid = self.compute_fid(training_set.take(summary.fid_batches))
                    val_fid = self.compute_fid(validation_set.take(summary.fid_batches))
                    trn_fid['G_fid'] = sum(trn_fid.values()) / 2
                    val_fid['G_fid'] = sum(val_fid.values()) / 2
                    self.summarize_scalars(trn_fid, 'training', epoch)
                    self.summarize_scalars(val_fid, 'validation', epoch)

                try:
                    display_training_dict['gen_fid'] = trn_fid['G_fid']
                    display_validation_dict['gen_fid'] = val_fid['G_fid']
                except KeyError:
                    pass

            # display metrics
            print_metrics(display_training_dict, display_validation_dict)

            # write hyperparameters summaries for tensorboard
            if summary.hparams:
                self.summarize_hparams(self.get_summary_hparams(training_config))

            # write scalar summaries for tensorboard
            training_scalars = {
                key[4:]: value.result()
                for key, value in metrics.items()
                if key.startswith('trn')
            }
            validation_scalars = {
                key[4:]: value.result()
                for key, value in metrics.items()
                if key.startswith('val')
            }
            self.summarize_scalars(training_scalars, 'training', epoch)
            self.summarize_scalars(validation_scalars, 'validation', epoch)

            # write image summaries for tensorboard
            if summary.image_samples_frequency:
                if epoch % summary.image_samples_frequency == 0:
                    training_images = self.get_summary_images(
                        training_summary_set, epoch == 0
                    )
                    validation_images = self.get_summary_images(
                        validation_summary_set, epoch == 0
                    )
                    self.summarize_images(training_images, 'training', epoch)
                    self.summarize_images(validation_images, 'validation', epoch)

            # store checkpoints
            self.save_epoch()
            self.save_model()

            if (
                checkpoint_copy.enabled
                and epoch >= checkpoint_copy.starting_epoch
                and epoch % checkpoint_copy.frequency == 0
            ):
                copy_checkpoint_folder(self.model_path, self.checkpoints_path, epoch)

            # reset loss accumulators
            for metric_tensor in metrics.values():
                metric_tensor.reset_states()

    def prepare_datasets(self, data_config, training_config):
        """
        Delegate the construction of the datasets for training

        Args:
            data_config (namespace): contains data configurations and paths
            training_config (namespace): contains some training configurations
                                         for the dataset
        Returns:
            training_set, validation_set (tf.data.Dataset objects)
        """
        return prepare_datasets(data_config, training_config)
