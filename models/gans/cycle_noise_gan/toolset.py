import logging
from os.path import basename

from data_pipeline.processing.load import optimize_loading
from data_pipeline.processing.extract import tfrecords2dataset
from data_pipeline.processing.transform import (
    arrange_double_dataset_for_training,
    arrange_double_dataset_for_validation,
)
from data_pipeline.factory.toolset import get_dataset_size
from data_pipeline.factory.noise_generation import extract_fresh_noise_dataset
from data_pipeline.factory.data_creation_pipeline import perform_entire_pipeline


def prepare_datasets(data_config, training_config):
    """
    Construct the datasets for training

    Args:
        data_config (namespace): contains data configurations and paths
        training_config (namespace): contains training configurations for the dataset
    Return:
        training_set, validation_set (tf.data.Dataset objects)
    """
    logger = logging.getLogger(__name__)

    # create/refine data
    tfrecords_x_path = perform_entire_pipeline(
        data_config.dataset_x_path,
        data_config.splits,
        data_config.min_size,
        data_config.max_size,
        data_config.fixed_size,
        data_config.fixed_mode,
    )

    # extract data
    logger.info('Loading datasets...')
    dataset_x = tfrecords2dataset(
        tfrecords_x_path, ('training', 'validation'), training_config.overlapping_datasets
    )
    logger.info('\tLoaded {}'.format(basename(tfrecords_x_path)))

    training_size = get_dataset_size(dataset_x['training'])
    validation_size = get_dataset_size(dataset_x['validation'])
    dataset_y = extract_fresh_noise_dataset(data_config, training_size, validation_size)
    logger.info('\tSet up {} noise generator'.format(data_config.noise['type']))

    # transform data
    logger.info('Preparing datasets for training...')
    training_set = arrange_double_dataset_for_training(
        dataset_x['training'], dataset_y['training'], training_config
    )
    validation_set = arrange_double_dataset_for_validation(
        dataset_x['validation'], dataset_y['validation'], training_config
    )

    # load data
    training_set = optimize_loading(training_set)
    validation_set = optimize_loading(validation_set)

    return training_set, validation_set
