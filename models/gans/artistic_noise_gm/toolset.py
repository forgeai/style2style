import logging
import numpy as np
import tensorflow as tf
import tensorflow.keras as keras
from os import listdir
from os.path import join
from collections import OrderedDict
from static_variables import resolve_static
from tensorboard.plugins.hparams import api as hp

from utils.image_processor import image_path_to_tensor
from utils.constants import UPSAMPLING_METHODS, CONDITIONAL_COMPONENTS
from data_pipeline.processing.load import optimize_loading
from data_pipeline.processing.transform import (
    arrange_single_dataset_for_training,
    arrange_single_dataset_for_validation,
)
from data_pipeline.factory.noise_generation import extract_fresh_noise_dataset
from models.components.auxiliaries.layers import DynamicPadding, DynamicCropping


def get_hparams():
    """
    Return a dictionary of all hyperparameters' ranges for the dashboard

    Return:
        (OrderedDict): maps hyperparameter name to a hp.HParam object
    """
    return OrderedDict(
        type=hp.HParam('type', hp.Discrete(CONDITIONAL_COMPONENTS)),
        dropout=hp.HParam('dropout', hp.RealInterval(0.0, 1.0)),
        l2_reg=hp.HParam('l2_reg', hp.RealInterval(0.0, 10.0)),
        upsampling=hp.HParam('upsampling', hp.Discrete(UPSAMPLING_METHODS)),
        smoothing_kernel=hp.HParam('smoothing_kernel', hp.IntInterval(2, 100)),
        lambda_style=hp.HParam('lambda_style', hp.RealInterval(0.0, 1.0)),
        lambda_reconstruction=hp.HParam(
            'lambda_reconstruction', hp.RealInterval(0.0, 1.0)
        ),
        reconstruction_variation=hp.HParam(
            'reconstruction_variation', hp.RealInterval(0.0, 1e5)
        ),
        total_variation=hp.HParam('total_variation', hp.RealInterval(0.0, 1e5)),
    )


def create_smoother(kernel_size):
    """
    Create a module that applies mean filtering on input images by performing
    average pooling and bilinear upsampling while preserving the input shape

    Args:
        kernel_size (int): the size of the kernel used for the average pooling
    Return:
        smoother (tf.keras.Model): the smoothing model
    """
    inputs = keras.Input(shape=(None, None, 3), dtype=tf.float32, name='input_image')
    input_shape = tf.shape(inputs)

    # compute the paddings required for preserving the shape of the image
    paddings = [[0, 0], [0, 0], [0, 0], [0, 0]]
    for d in range(1, 3):
        full_paddings = (kernel_size - input_shape[d]) % kernel_size
        side_paddings = full_paddings // 2
        paddings[d] = [side_paddings, full_paddings - side_paddings]

    # pass the inputs through the model
    x = DynamicPadding()(inputs, paddings)
    x = keras.layers.AveragePooling2D(pool_size=kernel_size)(x)
    x = keras.layers.UpSampling2D(size=kernel_size, interpolation='bilinear')(x)
    outputs = DynamicCropping()(x, (paddings[1][0], paddings[2][0]), input_shape[1:3])

    smoother = keras.Model(inputs=inputs, outputs=outputs, name='smoother')
    smoother.trainable = False

    return smoother


def create_noise_reconstructor(style_images_path):
    """
    Create an image that is meant to simulate the noise data which is able to
    reconstruct the original artistic style image

    Args:
        style_images_path (str): path to the location of the style images on disk
    Return:
        noise_reconstructor (tf.Tensors): a randomly initialized image tensor
    """
    style_image_names = listdir(style_images_path)
    assert len(style_image_names) == 1, 'Expected only 1 style image'

    style_image = image_path_to_tensor(join(style_images_path, style_image_names[0]))
    image_shape = (1,) + style_image.numpy().shape
    noise_reconstructor = tf.Variable(
        initial_value=np.random.normal(size=image_shape, scale=0.25),
        trainable=True,
        dtype=tf.float64,
    )
    return noise_reconstructor


@resolve_static(static_variables={'mae': keras.losses.MeanAbsoluteError()})
def noise_reconstruction_loss(style_image, style_image_reconstructed):
    """
    Compute the loss of reconstructing the style images from a noise sample

    Args:
        style_image (tf.Tensor): the original style image tensor
        style_image_reconstructed (tf.Tensor): the reconstructed style image tensor
    Return:
        loss_value (tf.float32)
    """
    return mae(style_image, style_image_reconstructed)


def prepare_datasets(data_config, training_config):
    """
    Construct the datasets for training

    Args:
        data_config (namespace): contains data configurations and paths
        training_config (namespace): contains training configurations for the dataset
    Return:
        training_set, validation_set (tf.data.Dataset objects)
    """
    logger = logging.getLogger(__name__)

    # extract data
    logger.info('Loading datasets...')
    dataset_x = extract_fresh_noise_dataset(
        data_config,
        data_config.training_samples_per_epoch,
        data_config.validation_samples_per_epoch,
    )
    logger.info('\tSet up {} noise generator'.format(data_config.noise['type']))

    # transform data
    logger.info('Preparing datasets for training...')
    training_set = arrange_single_dataset_for_training(
        dataset_x['training'], training_config
    )
    validation_set = arrange_single_dataset_for_validation(
        dataset_x['validation'], training_config
    )

    # load data
    training_set = optimize_loading(training_set)
    validation_set = optimize_loading(validation_set)

    return training_set, validation_set
