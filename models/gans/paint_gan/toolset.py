import cv2
import logging
import numpy as np
import tensorflow as tf
from PIL import Image
from os import makedirs
from os.path import join
from shutil import copyfile
from itertools import combinations
from collections import OrderedDict
from tensorboard.plugins.hparams import api as hp

from utils.constants import (
    OBJECTIVES,
    UPSAMPLING_METHODS,
    CONDITIONAL_COMPONENTS,
)
from data_pipeline.processing.load import optimize_loading
from data_pipeline.factory.image_processing import resize_to_fixed_dimensions
from data_pipeline.factory.noise_generation import extract_fresh_noise_masks_dataset


def get_summary_attributes():
    """
    Return all the attributes that are part of the summarization for the PaintGan

    Return:
        attributes (tuple of str)
    """
    return (
        'graph',
        'hparams',
        'image_samples',
        'image_samples_frequency',
    )


def get_hparams():
    """
    Return a dictionary of all hyperparameters' ranges for the dashboard

    Return:
        (OrderedDict): maps hyperparameter name to a hp.HParam object
    """
    return OrderedDict(
        objective=hp.HParam('objective', hp.Discrete(OBJECTIVES)),
        type=hp.HParam('type', hp.Discrete(CONDITIONAL_COMPONENTS)),
        dropout=hp.HParam('dropout', hp.RealInterval(0.0, 1.0)),
        l2_reg=hp.HParam('l2_reg', hp.RealInterval(0.0, 10.0)),
        upsampling=hp.HParam('upsampling', hp.Discrete(UPSAMPLING_METHODS)),
        seed_sigma=hp.HParam('seed_sigma', hp.RealInterval(0.0, 10.0)),
        alpha_ms_ssim=hp.HParam('alpha_ms_ssim', hp.RealInterval(0.0, 1.0)),
        lambda_style=hp.HParam('lambda_style', hp.RealInterval(0.0, 1e5)),
        lambda_reconstruction=hp.HParam(
            'lambda_reconstruction', hp.RealInterval(0.0, 1e5)
        ),
    )


def store_painting_set(data_config, storing_path):
    """
    Process the painting toolset and store its items (presumably in the model folder)
    The set contains the original painting image + annotation maps and label names

    Args:
        data_config (namespace): contains data configurations and paths
        storing_path (str): location where the painting set will be saved
    """
    makedirs(storing_path)
    fixed_dim = data_config.fixed_size
    fixed_mode = data_config.fixed_mode

    if isinstance(fixed_dim, int):
        fixed_dim = (fixed_dim, fixed_dim)
    else:
        assert len(fixed_dim) == 2, 'Image size must have a maximum rank of 2'
        fixed_dim = tuple(fixed_dim)

    # painting image
    painting_reading_path = join(data_config.painting_set_path, 'painting.png')
    painting_writing_path = join(storing_path, 'painting.png')
    painting = cv2.imread(painting_reading_path, cv2.IMREAD_COLOR)
    resized_painting = resize_to_fixed_dimensions(painting, fixed_dim, fixed_mode)
    cv2.imwrite(painting_writing_path, resized_painting)

    # labels image
    label_reading_path = join(data_config.painting_set_path, 'label.png')
    label_writing_path = join(storing_path, 'label.png')
    label = np.asarray(Image.open(label_reading_path))
    resized_label = resize_to_fixed_dimensions(
        label, fixed_dim, fixed_mode, cv2.INTER_NEAREST
    )
    Image.fromarray(resized_label).save(label_writing_path)

    # labels visualization and names
    for file_name in ('label_viz.png', 'label_names.txt'):
        file_reading_path = join(data_config.painting_set_path, file_name)
        file_writing_path = join(storing_path, file_name)
        copyfile(file_reading_path, file_writing_path)


def create_painting_reconstructor(painting_set_path, sigma):
    """
    Create an image seed which is meant to reconstruct the original painting

    Args:
        painting_set_path (str): path to the location of the painting set on disk
        sigma (float): the stddev of the reconstructor's initial value
    Return:
        painting_reconstructor (tf.Tensors): a randomly initialized image tensor
    """
    painting_image_path = join(painting_set_path, 'painting.png')
    painting_image = cv2.imread(painting_image_path, cv2.IMREAD_GRAYSCALE)

    reconstructor_shape = (1,) + painting_image.shape + (1,)
    painting_reconstructor = tf.Variable(
        initial_value=np.random.normal(size=reconstructor_shape, scale=sigma),
        trainable=True,
        dtype=tf.float64,
    )
    return painting_reconstructor


def get_label_pairs_generator(labels):
    """
    Create a generator of pairs of all labels to be used in the blending phase
    Run over all possible pairs uniformly but in random order

    Args:
        labels (list of ints): the labels found in the painting image
    Return:
        label_pairs (tf.data.Dataset): the labels pair generator
    """
    label_combinations = list(combinations(labels, 2))
    label_pairs = tf.data.Dataset.from_tensor_slices(label_combinations)
    label_pairs = label_pairs.shuffle(
        len(label_combinations), reshuffle_each_iteration=True
    ).repeat()
    return label_pairs


def create_single_pattern_input_mask(shape, label, num_labels):
    """
    Create an input tensor that serves as a mask for generating a mono-patterned image

    Args:
        shape (tuple): the dimensions of the image
        label (int): the style index
        num_labels (int): total number of labels
    Return:
        seed (tf.Tensor of rank 4): a categorical n-channeled image where
                                    n is the total number of labels
    """
    seed = label * tf.ones(shape, dtype=tf.int32)
    return tf.one_hot(seed, depth=num_labels, dtype=tf.float32)


def create_combined_pattern_input_mask(binary_mask, label_pair, num_labels):
    """
    Create an input tensor that serves as a mask for generating a bi-patterned image

    Args:
        binary_mask (tf.Tensor of rank 4): a boolean 1-channeled image
        label_pair (tuple of 2 ints): the labels to be blended
        num_labels (int): total number of labels
    Return:
        seed (tf.Tensor of rank 4): a categorical n-channeled image where
                                    n is the total number of labels
    """
    seed = tf.where(binary_mask, label_pair[0], label_pair[1])
    return tf.one_hot(seed, depth=num_labels, dtype=tf.float32)


def get_labels_mask(labels_image_path):
    """
    Reconstruct the categorical label mask from the image created by labelme

    Args:
        labels_image_path (str): path to the labels image
    Return:
        labels_mask (np.array): contains the pixelwise class indices
    """
    return np.asarray(Image.open(labels_image_path))


def get_label_names(label_names_path):
    """
    Return a dictionary that maps label indices to the name of the pattern they represent

    Args:
        label_names_path (str): path to the text file that contains the label names
    Return:
        label_names (dict): maps label index to pattern name
    """
    with open(label_names_path, 'r') as labels_file:
        lines = labels_file.readlines()
    return {i: name[:-1] for i, name in enumerate(lines)}


def resize_feature_mask(mask, size):
    """
    Resize the feature mask to comply with the downsampling operations of
    the feature extractor

    Args:
        mask (tf.Tensor of rank 3): [batch, height, width]
                                    encodes which pixels should be used for
                                    feature extraction
        size (tuple of 2 ints): expected height and width of the resized mask
    Return:
        mask_resized (tf.Tensor): the resized version of the mask
    """
    mask_with_channel = tf.expand_dims(mask, axis=-1)
    mask_resized = tf.image.resize(
        mask_with_channel, size, method=tf.image.ResizeMethod.NEAREST_NEIGHBOR
    )
    return mask_resized[..., 0]


def get_feature_correlations(feature_extractor, feature_layers, image, mask=None):
    """
    Compute the gram matrix of the feature activations for different layer levels
    and return them in the shape of a dictionary

    Args:
        feature_extractor (FeatureExtractor): the feature extracting instance
        feature_layers (list of ints): the levels in the network to extract from
        image (tf.Tensor): the input image from which features are extracted
        mask (tf.Tensor): encodes which pixels should be used for feature extraction
    Return:
        feature_correlations (dict): maps layer level to feature correlations matrix
    """
    if mask is None:
        mask = tf.ones(image.shape[:-1], dtype=tf.float32)

    feature_correlations = {}
    for layer_index in feature_layers:
        feature_map = feature_extractor(image, layer_index)
        feature_mask = resize_feature_mask(mask, feature_map.shape[1:3])
        feature_vectors = tf.boolean_mask(feature_map, feature_mask)

        corr_matrix = tf.linalg.einsum('vc,vd->cd', feature_vectors, feature_vectors)
        vector_length = tf.cast(tf.shape(feature_vectors)[0], tf.float32)
        feature_correlations[layer_index] = corr_matrix / vector_length

    return feature_correlations


def get_dataset_samples_for_summaries(dataset, num_samples):
    """
    Create a dataset that contains only a few unchanging samples from a larger one
    and packs them in one batch

    Args:
        dataset (tf.data.Dataset): a presumably large dataset
        num_samples (int): number of samples to have in the new dataset
    Return:
        dataset_samples (tf.data.Dataset)
    """
    subset = dataset.take(num_samples)
    subset = subset.map(lambda pair, seed: seed)
    subset = subset.unbatch().batch(num_samples)
    return tf.data.Dataset.from_tensors([x for x in subset][0])


def prepare_datasets(data_config, training_config, labels):
    """
    Construct the dataset for training

    Args:
        data_config (namespace): contains data configurations and paths
        training_config (namespace): contains training configurations for the dataset
        labels (list): the pattern style indices
    Return:
        training_set, None (tf.data.Dataset and empty placeholder for validation_set)
    """
    logger = logging.getLogger(__name__)

    # extract data
    logger.info('Loading datasets...')
    dataset = extract_fresh_noise_masks_dataset(
        data_config, data_config.training_samples_per_epoch
    )
    logger.info('\tSet up {} noise generator'.format(data_config.noise['type']))

    # transform data
    logger.info('Preparing dataset for training...')
    training_masks = dataset.batch(1)
    label_pairs_generator = get_label_pairs_generator(labels)
    training_zip = tf.data.Dataset.zip((training_masks, label_pairs_generator))
    training_set = training_zip.map(
        lambda mask, pair: (
            pair,
            create_combined_pattern_input_mask(mask, pair, len(labels)),
        ),
        num_parallel_calls=tf.data.experimental.AUTOTUNE,
    )

    # load data
    training_set = optimize_loading(training_set)

    return training_set, None
