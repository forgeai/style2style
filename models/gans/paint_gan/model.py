import cv2
import numpy as np
import tensorflow as tf
import tensorflow.keras as keras
from math import inf
from os import makedirs
from os.path import join
from tqdm import tqdm, trange
from collections import OrderedDict

from models.gans.base_gan.model import BaseGan
from models.gans.base_gan.toolset import (
    print_metrics,
    create_optimizer,
)
from models.gans.paint_gan.paint_network import (
    brush_style_loss,
    create_paint_network,
    painting_reconstruction_l1_loss,
    painting_reconstruction_ms_ssim_loss,
)
from models.gans.paint_gan.toolset import (
    get_hparams,
    get_labels_mask,
    get_label_names,
    prepare_datasets,
    store_painting_set,
    get_summary_attributes,
    get_feature_correlations,
    create_painting_reconstructor,
    create_single_pattern_input_mask,
    get_dataset_samples_for_summaries,
)
from models.gans.artistic_base_gan.feature_extractors import VggFeatureExtractor
from models.factory.file_handling import copy_checkpoint_folder
from models.factory.component_serialization import save_component, load_component
from models.components.auxiliaries.toolset import get_component_hparams_dict
from utils.json_processor import dict2namespace as d2n
from utils.image_processor import tensor2image, image_path_to_tensor, float2uint8


class PaintGan(BaseGan):
    def __init__(self, model_config, data_config=None):
        super().__init__(model_config)
        self.painting_set_path = join(self.model_path, 'painting_set')

        if self.model_config.new:
            assert (
                data_config is not None
            ), 'Creating a new PaintGan requires a data config'
            assert data_config.fixed_size, 'PaintGan requires fixed size images'
            assert data_config.fixed_mode, 'PaintGan requires crop mode for resizing'
            store_painting_set(data_config, self.painting_set_path)

        labels_mask_path = join(self.painting_set_path, 'label.png')
        label_names_path = join(self.painting_set_path, 'label_names.txt')
        labels_mask = get_labels_mask(labels_mask_path)
        self.label_names = get_label_names(label_names_path)
        self.num_labels = len(self.label_names)

        self.painting = image_path_to_tensor(join(self.painting_set_path, 'painting.png'))
        self.painting = tf.expand_dims(self.painting, axis=0)

        self.painting_mask = tf.one_hot(
            labels_mask, depth=self.num_labels, dtype=tf.float32
        )
        self.painting_mask = tf.expand_dims(self.painting_mask, axis=0)

        # on existing models this would override the value loaded in super().__init__()
        if model_config.new:
            self.painting_reconstructor = create_painting_reconstructor(
                self.painting_set_path, self.model_config.seed_sigma
            )

    def build_model(self):
        """
        Build a new model using the architectural configurations
        """
        self.paint_net = create_paint_network(d2n(self.model_config.paint_net))
        self.feature_extractor = VggFeatureExtractor(
            self.model_config.style_feature_layers
        )

    def create_reconstructor_checkpoint_manager(self):
        """
        Create the checkpoint manager for saving and loading the painting reconstructor
        """
        self.reconstructor_checkpoint = tf.train.Checkpoint(
            painting_reconstructor=self.painting_reconstructor
        )
        self.reconstructor_checkpoint_manager = tf.train.CheckpointManager(
            checkpoint=self.reconstructor_checkpoint,
            directory=join(self.checkpoints_path, 'reconstructors'),
            max_to_keep=1,
        )

    def load_reconstructor(self):
        """
        Load the painting reconstructor using the checkpoint manager
        """
        painting_set_path = join(self.model_path, 'painting_set')
        self.painting_reconstructor = create_painting_reconstructor(
            painting_set_path, self.model_config.seed_sigma
        )

        self.create_reconstructor_checkpoint_manager()
        self.reconstructor_checkpoint.restore(
            self.reconstructor_checkpoint_manager.latest_checkpoint
        ).assert_consumed()

    def save_reconstructor(self):
        """
        Save the painting reconstructor both as a checkpoint and as an image
        """
        # save checkpoint
        self.reconstructor_checkpoint_manager.save()

        # save image
        reconstructors_path = join(self.model_path, 'reconstructors')
        makedirs(reconstructors_path, exist_ok=True)

        image = np.squeeze(self.painting_reconstructor.numpy())
        image_path = join(reconstructors_path, 'painting_reconstructor.npy')
        np.save(image_path, image)

        image_viz = float2uint8(tensor2image(image))
        image_viz = cv2.cvtColor(image_viz, cv2.COLOR_BGR2RGB)
        image_viz_path = join(reconstructors_path, 'painting_reconstructor.png')
        cv2.imwrite(image_viz_path, image_viz)

    def load_model(self):
        """
        Load the paint net and the painting reconstructor
        """
        paint_net_path = join(self.checkpoints_path, 'paint_net')
        self.paint_net = load_component(paint_net_path)
        self.load_reconstructor()

    def save_model(self):
        """
        Save the paint net and the painting reconstructor
        """
        paint_net_path = join(self.checkpoints_path, 'paint_net')
        paint_net_config = d2n(self.model_config['paint_net'])
        save_component(self.paint_net, paint_net_config, paint_net_path)
        self.save_reconstructor()

    def save_model_based_on_performance(self, current_losses, best_losses):
        """
        Save the paint net based on the training loss by serializing its
        architecture and weights. In the PaintGan the model is saved
        only if the training loss is the best so far

        Args:
            current_losses (dict): contains specific loss value tensors
                                   from the current iteration
            best_losses (dict): contains the best specific loss values
                                encountered so far
        Return:
            best_losses (dict): the updated best loss values
        """
        loss_of_interest = current_losses['trn_total_loss'].result().numpy()

        if loss_of_interest < best_losses['trn_total_loss']:
            best_losses['trn_total_loss'] = loss_of_interest
            self.save_model()

        return best_losses

    def get_summary_hparams(self, training_config):
        """
        Create a dictionary of hparams to summarize

        Args:
            training_config (namespace): config file for training
        Return:
            hparams_dict (OrderedDict): maps hparams objects to actual values
        """
        gan_hparams = get_hparams()
        hparams_dict = OrderedDict()

        # paint_gan general hparams
        hparams_dict[gan_hparams['seed_sigma']] = self.model_config.seed_sigma

        # paint_net hparams
        paint_net_config = d2n(self.model_config.paint_net)
        paint_net_hparams = get_component_hparams_dict(paint_net_config)
        for hparam, value in paint_net_hparams.items():
            hparams_dict[gan_hparams[hparam]] = value

        # training hparams
        for param in ('alpha_ms_ssim', 'lambda_style', 'lambda_reconstruction'):
            hparams_dict[gan_hparams[param]] = training_config.paint_net[param]

        return hparams_dict

    def get_summary_images(self, samples_dataset, include_inputs=None):
        """
        Create a dictionary of images to summarize

        Args:
            samples_dataset (tf.data.Dataset): a dataset of paitining input masks
            include_inputs (bool): doesn't have any special role in PaintGan
                                   because there are no static inputs
        Return:
            summaries_dict (dict): maps summary names to image tensors
        """
        summaries_dict = {}
        input_shape = tf.shape(self.painting_reconstructor)

        # reconstruction
        reconstruction_inputs = dict(
            mask=self.painting_mask, seed=tf.cast(self.painting_reconstructor, tf.float32)
        )
        reconstructed_painting = self.paint_net(reconstruction_inputs)
        summaries_dict['Reconstruction'] = tensor2image(reconstructed_painting)
        summaries_dict['Reconstructor'] = tensor2image(self.painting_reconstructor)

        # mono-patterned images
        inputs = dict(
            seed=tf.random.normal(input_shape, stddev=self.model_config.seed_sigma)
        )
        for label in self.label_names:
            inputs['mask'] = self.single_style_masks[label]
            paint_prediction = self.paint_net(inputs)
            summaries_dict[f'Style [{label}] {self.label_names[label]}'] = tensor2image(
                paint_prediction
            )

        # bi-patterned images
        for sample_mask in samples_dataset:
            inputs = dict(
                mask=sample_mask,
                seed=tf.random.normal(input_shape, stddev=self.model_config.seed_sigma),
            )
            paint_prediction = self.paint_net(inputs)
            summaries_dict['Combined Styles'] = tensor2image(paint_prediction)

        return summaries_dict

    @tf.function
    def train_step(
        self,
        paint_mask,
        label_masks,
        feature_correlations,
        optimizer,
        loss_config,
        feature_weights,
        metrics,
    ):
        """
        Perform an iteration of optimization for the style, blending and reconstruction
        capabilities of the paint net

        Args:
            paint_mask (tf.Tensor): a categorially encoded label mask for the paint net
            label_masks (list of tf.Tensor): individual masks for the labels present in
                                             the paint mask
            feature_correlations (list of dicts of tf.Tensors): the feature correlations
                                                                for the labels on duty
            optimizer (keras.optimizers.Optimizer): the optimizer for the paint net
            loss_config (namespace): configurations for paint net's loss weighting
            feature_weights (namespace): configurations for feature levels weighting
            metrics (dict): contains the accumulators for the metrics' evaluation
        """
        input_shape = tf.shape(self.painting_reconstructor)

        with tf.GradientTape() as tape:
            inputs = dict(
                mask=paint_mask,
                seed=tf.random.normal(input_shape, stddev=self.model_config.seed_sigma),
            )
            paint_prediction = self.paint_net(inputs, training=True)

            # style loss
            style_loss = 0
            for label_mask, label_feature_correlations in zip(
                label_masks, feature_correlations
            ):
                prediction_feature_correlations = get_feature_correlations(
                    self.feature_extractor,
                    feature_weights.keys(),
                    paint_prediction,
                    label_mask,
                )
                style_loss += brush_style_loss(
                    label_feature_correlations,
                    prediction_feature_correlations,
                    feature_weights,
                )

            # reconstruction loss
            reconstruction_inputs = dict(
                mask=self.painting_mask,
                seed=tf.cast(self.painting_reconstructor, tf.float32),
            )
            reconstructed_painting = self.paint_net(reconstruction_inputs, training=True)
            rec_l1_loss = painting_reconstruction_l1_loss(
                self.painting, reconstructed_painting
            )
            rec_ms_ssim_loss = painting_reconstruction_ms_ssim_loss(
                self.painting, reconstructed_painting
            )
            rec_loss = (
                loss_config.alpha_ms_ssim * rec_ms_ssim_loss
                + (1.0 - loss_config.alpha_ms_ssim) * rec_l1_loss
            )

            # final loss
            total_loss = (
                loss_config.lambda_style * style_loss
                + loss_config.lambda_reconstruction * rec_loss
            )

        # apply gradients to paint_net
        trainable_variables = self.paint_net.trainable_variables + [
            self.painting_reconstructor
        ]
        gradients = tape.gradient(total_loss, trainable_variables)
        optimizer.apply_gradients(zip(gradients, trainable_variables))

        # do another round for the reconstruction while keeping the paint net frozen
        with tf.GradientTape(watch_accessed_variables=False) as rec_tape:
            rec_tape.watch(self.painting_reconstructor)

            rec_inputs = dict(
                mask=self.painting_mask,
                seed=tf.cast(self.painting_reconstructor, tf.float32),
            )
            rec_painting = self.paint_net(rec_inputs, training=False)
            rec_l1_loss = painting_reconstruction_l1_loss(self.painting, rec_painting)
            rec_ms_ssim_loss = painting_reconstruction_ms_ssim_loss(
                self.painting, rec_painting
            )
            rec_loss = (
                loss_config.alpha_ms_ssim * rec_ms_ssim_loss
                + (1.0 - loss_config.alpha_ms_ssim) * rec_l1_loss
            )

        # apply gradients to painting reconstructor only
        rec_gradients = rec_tape.gradient(rec_loss, [self.painting_reconstructor])
        optimizer.apply_gradients(zip(rec_gradients, [self.painting_reconstructor]))

        metrics['trn_total_loss'](total_loss)
        metrics['trn_style_loss'](style_loss)
        metrics['trn_rec_loss'](rec_loss)
        metrics['trn_rec_l1_loss'](rec_l1_loss)
        metrics['trn_rec_ms_ssim_loss'](rec_ms_ssim_loss)

    def train(self, training_config, training_set, validation_set):
        """
        Perform the training process

        Args:
            training_config (namespace): contains configurations for training
            training_set (tf.data.Dataset): the training set
            validation_set (None): empty placeholder for the validation set
        """
        self.create_reconstructor_checkpoint_manager()

        self.single_style_masks = {}
        style_feature_correlations = {}
        for label in self.label_names:
            self.single_style_masks[label] = create_single_pattern_input_mask(
                tf.shape(self.painting)[:3], label, self.num_labels
            )
            style_feature_correlations[label] = get_feature_correlations(
                self.feature_extractor,
                self.model_config.style_feature_layers,
                self.painting,
                self.painting_mask[..., label],
            )

        paint_net_config = d2n(training_config.paint_net)
        loss_config = d2n(
            dict(
                alpha_ms_ssim=paint_net_config.alpha_ms_ssim,
                lambda_style=paint_net_config.lambda_style,
                lambda_reconstruction=paint_net_config.lambda_reconstruction,
            )
        )
        feature_weights_dict = dict(
            zip(
                self.model_config.style_feature_layers,
                self.model_config.style_feature_weights,
            )
        )

        summary = d2n(training_config.summary)
        checkpoint_copy = d2n(training_config.checkpoint_copy)
        optimizer = create_optimizer(paint_net_config, training_config.epochs)

        # set default (null) values for summary configs if not provided
        for attribute in get_summary_attributes():
            setattr(summary, attribute, getattr(summary, attribute, 0))

        if summary.graph:
            tf.summary.trace_on(graph=True, profiler=True)
            summarized_graph_already = False
        if summary.hparams:
            self.initialize_hparams(
                list(self.get_summary_hparams(training_config).keys()),
                metric='total_loss',
                display_name='Loss',
            )

        # collect data samples for summaries
        training_summary_set = get_dataset_samples_for_summaries(
            training_set, summary.image_samples
        )
        training_data_size = 0

        metrics_to_monitor = (
            'trn_total_loss',
            'trn_style_loss',
            'trn_rec_loss',
            'trn_rec_l1_loss',
            'trn_rec_ms_ssim_loss',
        )
        metrics = {
            metric_name: keras.metrics.Mean(metric_name, dtype=tf.float32)
            for metric_name in metrics_to_monitor
        }
        best_losses = {'trn_total_loss': inf}

        # summarize the original painting
        if summary.image_samples_frequency:
            painting_summary = dict(Painting=tensor2image(self.painting))
            self.summarize_images(painting_summary, 'training', 0)

        # start the training iterations
        starting_epoch = self.epoch.value()
        for epoch in trange(
            starting_epoch,
            starting_epoch + training_config.epochs,
            desc='{:^21}'.format('Training Progress'),
        ):
            self.epoch.assign(epoch)

            # train
            training_size_counter = 0
            for label_pair, paint_mask in tqdm(
                training_set,
                desc='{:<21}'.format('Epoch {} (training)'.format(epoch)),
                total=training_data_size,
            ):
                labels = label_pair.numpy()
                label_masks = [paint_mask[..., k] for k in labels]
                feature_correlations = [style_feature_correlations[k] for k in labels]

                self.train_step(
                    paint_mask,
                    label_masks,
                    feature_correlations,
                    optimizer,
                    loss_config,
                    feature_weights_dict,
                    metrics,
                )

                if summary.graph and not summarized_graph_already:
                    self.summarize_graph()
                    summarized_graph_already = True
                training_size_counter += 1

            training_data_size = training_size_counter

            # collect metrics for display
            display_training_dict = dict(
                rec_l1_loss=metrics['trn_rec_l1_loss'].result().numpy(),
                rec_ms_ssim_loss=metrics['trn_rec_ms_ssim_loss'].result().numpy(),
                rec_loss=metrics['trn_rec_loss'].result().numpy(),
                style_loss=metrics['trn_style_loss'].result().numpy(),
                total_loss=metrics['trn_total_loss'].result().numpy(),
            )
            display_validation_dict = dict(
                rec_l1_loss=np.nan,
                rec_ms_ssim_loss=np.nan,
                rec_loss=np.nan,
                style_loss=np.nan,
                total_loss=np.nan,
            )

            # display metrics
            print_metrics(display_training_dict, display_validation_dict, 24, 6)

            # write hyperparameters summaries for tensorboard
            if summary.hparams:
                self.summarize_hparams(self.get_summary_hparams(training_config))

            # write scalar summaries for tensorboard
            training_scalars = {
                key[4:]: value.result()
                for key, value in metrics.items()
                if key.startswith('trn')
            }
            evaluation_loss = metrics['trn_total_loss'].result()
            self.summarize_scalars(training_scalars, 'training', epoch)
            self.summarize_scalars(dict(total_loss=evaluation_loss), 'validation', epoch)

            # write image summaries for tensorboard
            if summary.image_samples_frequency:
                if epoch % summary.image_samples_frequency == 0:
                    training_images = self.get_summary_images(training_summary_set)
                    self.summarize_images(training_images, 'training', epoch)

            # store checkpoints
            self.save_epoch()
            best_losses = self.save_model_based_on_performance(metrics, best_losses)

            if (
                checkpoint_copy.enabled
                and epoch >= checkpoint_copy.starting_epoch
                and epoch % checkpoint_copy.frequency == 0
            ):
                copy_checkpoint_folder(self.model_path, self.checkpoints_path, epoch)

            # reset loss accumulators
            for metric_tensor in metrics.values():
                metric_tensor.reset_states()

    def prepare_datasets(self, data_config, training_config):
        """
        Delegate the construction of the dataset for training

        Args:
            data_config (namespace): contains data configurations and paths
            training_config (namespace): contains some training configurations
                                         for the dataset
        Returns:
            training_set, validation_set (tf.data.Dataset and empty placeholder)
        """
        return prepare_datasets(
            data_config, training_config, list(self.label_names.keys())
        )
