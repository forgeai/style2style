import tensorflow as tf
import tensorflow.keras as keras
from static_variables import resolve_static

from models.factory.type_loader import get_model_type


def create_paint_network(config):
    """
    Create an instance of a paint network

    Args:
        config (namespace): contains layers parameterization
    Return:
        a style network (tf.keras.Model)
    """
    ComponentClass = get_model_type(config.type, is_component=True)
    config.output_activation = 'tanh'
    component_creator = ComponentClass(config, 'paint_network')

    return component_creator.create_model()


@resolve_static(static_variables={'mse': keras.losses.MeanSquaredError()})
def brush_style_loss(style_correlations, synthesized_correlations, layer_weights):
    """
    Compute the style loss between two images by calculating a weighted mean squared
    error between their feature correlations from different levels

    Args:
        style_correlations (dict of tf.Tensors): maps layer index to a squared
                                                (gram) matrix for the style image
        synthesized_correlations (dict of tf.Tensor): maps layer index to a squared (gram)
                                                      matrix for the synthesized image
        layer_weights (dict): maps feature level to its loss weight
    Return:
        loss_value (tf.float32)
    """
    loss_value = 0.0

    for layer_index, layer_weight in layer_weights.items():
        style_matrix = style_correlations[layer_index]
        synthesized_matrix = synthesized_correlations[layer_index]
        loss_value += layer_weight * mse(style_matrix, synthesized_matrix)

    return loss_value


@resolve_static(static_variables={'mae': keras.losses.MeanAbsoluteError()})
def painting_reconstruction_l1_loss(painting, reconstruction):
    """
    Compute the L1 loss between the original painting and the
    attempted reconstruction

    Args:
        painting (tf.Tensor): the original painting Tensor
        reconstruction (tf.Tensor): the synthesized reconstruction of the painting
    Return:
        loss_value (tf.float32)
    """
    return mae(painting, reconstruction)


def painting_reconstruction_ms_ssim_loss(painting, reconstruction):
    """
    Compute the MS-SSIM loss between the original painting and the
    attempted reconstruction

    Args:
        painting (tf.Tensor): the original painting Tensor
        reconstruction (tf.Tensor): the synthesized reconstruction of the painting
    Return:
        loss_value (tf.float32)
    """
    return 1.0 - tf.image.ssim_multiscale(painting, reconstruction, 2.0)
