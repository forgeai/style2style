from collections import OrderedDict
from tensorboard.plugins.hparams import api as hp

from utils.constants import (
    OBJECTIVES,
    UPSAMPLING_METHODS,
    CONDITIONAL_COMPONENTS,
    NON_CONDITIONAL_COMPONENTS,
)


def get_hparams():
    """
    Return a dictionary of all hyperparameters' ranges for the dashboard

    Return:
        (OrderedDict): maps hyperparameter name to a hp.HParam object
    """
    return OrderedDict(
        objective=hp.HParam('objective', hp.Discrete(OBJECTIVES)),
        s_type=hp.HParam('s_type', hp.Discrete(CONDITIONAL_COMPONENTS)),
        d_type=hp.HParam('d_type', hp.Discrete(NON_CONDITIONAL_COMPONENTS)),
        s_dropout=hp.HParam('s_dropout', hp.RealInterval(0.0, 1.0)),
        d_dropout=hp.HParam('d_dropout', hp.RealInterval(0.0, 1.0)),
        s_l2_reg=hp.HParam('s_l2_reg', hp.RealInterval(0.0, 10.0)),
        d_l2_reg=hp.HParam('d_l2_reg', hp.RealInterval(0.0, 10.0)),
        s_upsampling=hp.HParam('s_upsampling', hp.Discrete(UPSAMPLING_METHODS)),
        d_upsampling=hp.HParam('d_upsampling', hp.Discrete(UPSAMPLING_METHODS)),
        lambda_style=hp.HParam('lambda_style', hp.RealInterval(0.0, 1.0)),
        lambda_reconstruction=hp.HParam(
            'lambda_reconstruction', hp.RealInterval(0.0, 1.0)
        ),
        lambda_adversarial=hp.HParam('lambda_adversarial', hp.RealInterval(0.0, 1.0)),
        lambda_gradient_penalty=hp.HParam(
            'lambda_gradient_penalty', hp.RealInterval(0.0, 1e5)
        ),
        total_variation=hp.HParam('total_variation', hp.RealInterval(0.0, 1e5)),
    )
