import shutil
import numpy as np
import tensorflow as tf
from math import inf
from os import makedirs
from os.path import join
from tqdm import tqdm, trange
from collections import OrderedDict

from utils.image_processor import tensor2image
from utils.json_processor import dict2namespace as d2n
from models.factory.file_handling import copy_checkpoint_folder
from models.factory.component_serialization import save_component, load_component
from models.gans.base_gan.toolset import (
    print_metrics,
    create_optimizer,
    get_dataset_samples_for_summaries,
)
from models.gans.cycle_gan.discriminator import (
    create_discriminator,
    discriminator_accuracy,
    discriminator_gradient_penalty,
    get_discriminator_threshold,
    get_discriminator_loss_function,
)
from models.gans.cycle_gan.generator import get_generator_loss_function
from models.gans.artistic_noise_gan.toolset import get_hparams
from models.gans.artistic_noise_gm.model import ArtisticNoiseGm
from models.gans.artistic_noise_gm.toolset import noise_reconstruction_loss
from models.gans.artistic_gm.style_network import (
    style_loss,
    variation_loss,
    create_style_network,
)
from models.gans.artistic_base_gan.toolset import (
    get_channel_eigenvectors,
    get_summary_attributes,
    create_features_dataset,
    project_image_to_featurespace,
)
from models.gans.artistic_base_gan.feature_extractors import VggFeatureExtractor
from models.components.auxiliaries.toolset import get_component_hparams_dict


class ArtisticNoiseGan(ArtisticNoiseGm):
    def __init__(self, model_config, data_config=None):
        super().__init__(model_config, data_config)

    def build_model(self):
        """
        Build a new model using the architectural configurations
        """
        self.discriminator = create_discriminator(d2n(self.model_config.discriminator))
        self.style_net = create_style_network(d2n(self.model_config.style_net))
        self.feature_extractor = VggFeatureExtractor(
            self.model_config.style_feature_layers
        )

    def load_model(self):
        """
        Load the architecture and the weights of the style net and discriminator +
        the noise reconstructor
        """
        super().load_model()
        discriminator_path = join(self.checkpoints_path, 'discriminator')
        self.discriminator = load_component(discriminator_path)

    def save_model(self):
        """
        Save the weights and dependencies of the style net and discriminator and
        create model diagrams if saving for the first time
        """
        super().save_model()
        discriminator_path = join(self.checkpoints_path, 'discriminator')
        discriminator_config = d2n(self.model_config['discriminator'])
        save_component(self.discriminator, discriminator_config, discriminator_path)

    def get_summary_hparams(self, training_config):
        """
        Create a dictionary of hparams to summarize

        Args:
            training_config (namespace): config file for training
        Return:
            hparams_dict (OrderedDict): maps hparams objects to actual values
        """
        gan_hparams = get_hparams()
        hparams_dict = OrderedDict()

        # artistic_noise_gan general hparams
        hparams_dict[gan_hparams['objective']] = self.model_config.discriminator[
            'objective'
        ]

        # style_net hparams
        style_net_config = d2n(self.model_config.style_net)
        style_net_hparams = get_component_hparams_dict(style_net_config)
        for hparam, value in style_net_hparams.items():
            hparams_dict[gan_hparams[f's_{hparam}']] = value

        # discriminator hparams
        discriminator_config = d2n(self.model_config.discriminator)
        discriminator_hparams = get_component_hparams_dict(discriminator_config)
        for hparam, value in discriminator_hparams.items():
            hparams_dict[gan_hparams[f'd_{hparam}']] = value

        # training hparams
        for hparam in (
            'lambda_style',
            'lambda_reconstruction',
            'lambda_adversarial',
            'total_variation',
        ):
            hparams_dict[gan_hparams[hparam]] = training_config.style_net[hparam]
        hparams_dict[
            gan_hparams['lambda_gradient_penalty']
        ] = training_config.discriminator['lambda_gradient_penalty']

        return hparams_dict

    def get_summary_images(self, samples_dataset, include_inputs, include_reconstructor):
        """
        Create a dictionary of images to summarize

        Args:
            samples_dataset (tf.data.Dataset): a dataset of content images
            include_inputs (bool): whether to include images that do not
                                   depend on the model evolution and thus
                                   do not change over the course of epochs
            include_reconstructor (bool): whether to include the style image
                                          reconstructor in the summaries
        Return:
            summaries_dict (dict): maps summary names to image tensors
        """
        summaries_dict = {}

        for sample_noise in samples_dataset:
            if include_inputs:
                summaries_dict['Input Image'] = tensor2image(sample_noise)

            # synthesized style
            synthesized_image = self.style_net(sample_noise)
            summaries_dict['Synthesized'] = tensor2image(synthesized_image)

            # style image reconstruction
            if include_reconstructor:
                casted_noise_reconstructor = tf.cast(self.noise_reconstructor, tf.float32)
                reconstructed_image = self.style_net(casted_noise_reconstructor)
                heat_map, _ = tf.linalg.normalize(
                    self.discriminator(casted_noise_reconstructor), ord=np.inf
                )

                noise_image = tensor2image(casted_noise_reconstructor)
                summaries_dict['Noise Reconstructor'] = noise_image
                summaries_dict['Reconstructed'] = tensor2image(reconstructed_image)
                summaries_dict['Heat Map Reconstructor'] = heat_map
                summaries_dict['Heat Masked Reconstructor'] = tf.where(
                    heat_map > self.discriminator_threshold, noise_image, 0.0
                )

        return summaries_dict

    def train_style_net(
        self,
        noise_input,
        style_batch,
        noise_reconstructor,
        optimizer,
        loss_config,
        features_config,
        metrics,
        metrics_prefix='trn',
    ):
        """
        Perform an iteration of style net and noise reconstructor optimization

        Args:
            noise_input (tf.Tensor): a batch of noise input images
            style_batch (tf.Tensor): a batch of image + features for the style image
            noise_reconstructor (tf.Tensor): noise input to reconstruct the style image
            optimizer (keras.optimizers.Optimizer): the optimizer for the style net
            loss_config (namespace): configurations for the loss weighting
            features_config (namespace): configurations for features levels and weights
            metrics (dict): contains the accumulators for the metrics' evaluation
            metrics_prefix (str): prefix to add for the metrics keys
        """
        style_input = style_batch[0]
        style_features = style_batch[1:]

        with tf.GradientTape() as tape:
            synthesized_batch = self.style_net(noise_input, training=True)

            # style loss
            synthesized_style_features = tuple(
                [
                    self.feature_extractor(synthesized_batch, layer_number)
                    for layer_number in features_config.style_feature_layers
                ]
            )
            style_loss_value = style_loss(
                synthesized_style_features,
                style_features,
                features_config.style_feature_weights,
            )

            # total variation loss
            variation_loss_value = variation_loss(synthesized_batch)

            # noise reconstructor loss
            casted_noise_reconstructor = tf.cast(noise_reconstructor, tf.float32)
            style_image_reconstructed = self.style_net(
                casted_noise_reconstructor, training=True
            )
            rec_error = noise_reconstruction_loss(style_input, style_image_reconstructed)

            # noise adversarial loss
            fake_noise_pred = self.discriminator(
                casted_noise_reconstructor, training=True
            )
            adv_loss = self.adversarial_loss(fake_noise_pred)

            # final loss
            total_loss = (
                loss_config.lambda_style * style_loss_value
                + loss_config.lambda_reconstruction * rec_error
                + loss_config.lambda_adversarial * adv_loss
                + loss_config.total_variation * variation_loss_value
            )

        metrics[metrics_prefix + '_total_loss'](total_loss)
        metrics[metrics_prefix + '_style_loss'](style_loss_value)
        metrics[metrics_prefix + '_variation_loss'](variation_loss_value)

        # apply gradients to style_net + noise_reconstructor
        trainable_variables = self.style_net.trainable_variables + [noise_reconstructor]
        gradients = tape.gradient(total_loss, trainable_variables)
        optimizer.apply_gradients(zip(gradients, trainable_variables))

        # do another round for the reconstruction while keeping the style net frozen
        with tf.GradientTape(watch_accessed_variables=False) as rec_tape:
            rec_tape.watch(noise_reconstructor)

            casted_noise_reconstructor = tf.cast(noise_reconstructor, tf.float32)
            style_image_reconstructed = self.style_net(
                casted_noise_reconstructor, training=False
            )
            rec_error = noise_reconstruction_loss(style_input, style_image_reconstructed)
            fake_noise_pred = self.discriminator(
                casted_noise_reconstructor, training=True
            )
            adv_loss = self.adversarial_loss(fake_noise_pred)

            total_loss = (
                loss_config.lambda_reconstruction * rec_error
                + loss_config.lambda_adversarial * adv_loss
            )

        # apply gradients to noise_reconstructor only
        rec_gradients = rec_tape.gradient(total_loss, [noise_reconstructor])
        optimizer.apply_gradients(zip(rec_gradients, [noise_reconstructor]))

        metrics[metrics_prefix + '_rec_error'](rec_error)
        metrics[metrics_prefix + '_adv_loss'](adv_loss)

    def validate_style_net(
        self,
        noise_input,
        style_batch,
        noise_reconstructor,
        loss_config,
        features_config,
        metrics,
        metrics_prefix='val',
    ):
        """
        Perform an iteration of style net and noise reconstructor validation

        Args:
            noise_input (tf.Tensor): a batch of noise input images
            style_batch (tf.Tensor): a batch of image + features for the style image
            noise_reconstructor (tf.Tensor): noise input to reconstruct the style image
            loss_config (namespace): configurations for the loss weighting
            features_config (namespace): configurations for features levels and weights
            metrics (dict): contains the accumulators for the metrics' evaluation
            metrics_prefix (str): prefix to add for the metrics keys
        """
        style_input = style_batch[0]
        style_features = style_batch[1:]
        synthesized_batch = self.style_net(noise_input, training=False)

        # style loss
        synthesized_style_features = tuple(
            [
                self.feature_extractor(synthesized_batch, layer_number)
                for layer_number in features_config.style_feature_layers
            ]
        )
        style_loss_value = style_loss(
            synthesized_style_features,
            style_features,
            features_config.style_feature_weights,
        )

        # total variation loss
        variation_loss_value = variation_loss(synthesized_batch)

        # noise reconstructor loss
        casted_noise_reconstructor = tf.cast(noise_reconstructor, tf.float32)
        style_image_reconstructed = self.style_net(
            casted_noise_reconstructor, training=False
        )
        rec_error = noise_reconstruction_loss(style_input, style_image_reconstructed)

        # noise adversarial loss
        fake_noise_pred = self.discriminator(casted_noise_reconstructor, training=True)
        adv_loss = self.adversarial_loss(fake_noise_pred)

        # final loss
        total_loss = (
            loss_config.lambda_style * style_loss_value
            + loss_config.lambda_reconstruction * rec_error
            + loss_config.lambda_adversarial * adv_loss
            + loss_config.total_variation * variation_loss_value
        )

        metrics[metrics_prefix + '_total_loss'](total_loss)
        metrics[metrics_prefix + '_style_loss'](style_loss_value)
        metrics[metrics_prefix + '_rec_error'](rec_error)
        metrics[metrics_prefix + '_adv_loss'](adv_loss)
        metrics[metrics_prefix + '_variation_loss'](variation_loss_value)

    def train_discriminator(
        self,
        noise_input,
        noise_reconstructor,
        optimizer,
        loss_config,
        metrics,
        metrics_prefix='trn',
    ):
        """
        Perform an iteration of discriminator optimization

        Args:
            noise_input (tf.Tensor): a batch of noise input images
            noise_reconstructor (tf.Tensor): noise input to reconstruct the style image
            optimizer (keras.optimizers.Optimizer): the optimizer for the discriminator
            loss_config (namespace): configurations for the discriminators' loss weighting
            metrics (dict): contains the accumulators for the metrics' evaluation
            metrics_prefix (str): prefix to add for the metrics keys
        """
        do_gp = loss_config.lambda_gradient_penalty > 0

        with tf.GradientTape() as tape:
            casted_noise_reconstructor = tf.cast(noise_reconstructor, tf.float32)
            real_noise_pred = self.discriminator(noise_input, training=True)
            fake_noise_pred = self.discriminator(
                casted_noise_reconstructor, training=True
            )

            D_loss = self.discriminator_loss(real_noise_pred, fake_noise_pred)
            D_total_loss = D_loss

            if do_gp:
                alpha = tf.random.uniform((1,), dtype=tf.float32)
                interpolated = alpha * noise_input + (1.0 - alpha) * tf.cast(
                    casted_noise_reconstructor, tf.float32
                )
                inter_pred = self.discriminator(interpolated, training=True)
                inter_gradients = tf.gradients(inter_pred, interpolated)

                D_gp = discriminator_gradient_penalty(inter_gradients[0])
                D_total_loss += loss_config.lambda_gradient_penalty * D_gp

        D_acc = discriminator_accuracy(
            real_noise_pred, fake_noise_pred, self.discriminator_threshold
        )

        metrics[metrics_prefix + '_D_total_loss'](D_total_loss)
        metrics[metrics_prefix + '_D_acc'](D_acc)
        if do_gp:
            metrics[metrics_prefix + '_D_loss'](D_loss)
            metrics[metrics_prefix + '_D_gp'](D_gp)

        # apply gradients
        gradients = tape.gradient(D_total_loss, self.discriminator.trainable_variables)
        optimizer.apply_gradients(zip(gradients, self.discriminator.trainable_variables))

    def validate_discriminator(
        self,
        noise_input,
        noise_reconstructor,
        loss_config,
        metrics,
        metrics_prefix='val',
    ):
        """
        Perform an iteration of discriminator validation

        Args:
            noise_input (tf.Tensor): a batch of noise input images
            noise_reconstructor (tf.Tensor): noise input to reconstruct the style image
            loss_config (namespace): configurations for the discriminators' loss weighting
            metrics (dict): contains the accumulators for the metrics' evaluation
            metrics_prefix (str): prefix to add for the metrics keys
        """
        do_gp = loss_config.lambda_gradient_penalty > 0

        casted_noise_reconstructor = tf.cast(noise_reconstructor, tf.float32)
        real_noise_pred = self.discriminator(noise_input, training=False)
        fake_noise_pred = self.discriminator(casted_noise_reconstructor, training=False)

        D_loss = self.discriminator_loss(real_noise_pred, fake_noise_pred)
        D_total_loss = D_loss

        if do_gp:
            alpha = tf.random.uniform((1,), dtype=tf.float32)
            interpolated = alpha * noise_input + (1.0 - alpha) * tf.cast(
                casted_noise_reconstructor, tf.float32
            )
            inter_pred = self.discriminator(interpolated, training=False)
            inter_gradients = tf.gradients(inter_pred, interpolated)

            D_gp = discriminator_gradient_penalty(inter_gradients[0])
            D_total_loss += loss_config.lambda_gradient_penalty * D_gp

        D_acc = discriminator_accuracy(
            real_noise_pred, fake_noise_pred, self.discriminator_threshold
        )

        metrics[metrics_prefix + '_D_total_loss'](D_total_loss)
        metrics[metrics_prefix + '_D_acc'](D_acc)
        if do_gp:
            metrics[metrics_prefix + '_D_loss'](D_loss)
            metrics[metrics_prefix + '_D_gp'](D_gp)

    @tf.function
    def train_step(
        self,
        noise_input,
        style_batch,
        noise_reconstructor,
        style_net_optimizer,
        discriminator_optimizer,
        S_loss_config,
        D_loss_config,
        features_config,
        metrics,
        metrics_prefix='trn',
    ):
        """
        Perform an iteration of training

        Args:
            noise_input (tf.Tensor): a batch of noise input images
            style_batch (tf.Tensor): a batch of image + features for the style image
            noise_reconstructor (tf.Tensor): noise input to reconstruct the style image
            style_net_optimizer (keras.optimizers.Optimizer): the optimizer for
                                                              the style net
            discriminator_optimizer (keras.optimizers.Optimizer): the optimizer for
                                                                  the discriminator
            S_loss_config (namespace): configurations for style net's loss weighting
            D_loss_config (namespace): configurations for discriminator's loss weighting
            features_config (namespace): configurations for features levels and weights
            metrics (dict): contains the accumulators for the metrics' evaluation
            metrics_prefix (str): prefix to add for the metrics keys
        """
        self.train_style_net(
            noise_input,
            style_batch,
            noise_reconstructor,
            style_net_optimizer,
            S_loss_config,
            features_config,
            metrics,
            metrics_prefix,
        )
        self.train_discriminator(
            noise_input,
            noise_reconstructor,
            discriminator_optimizer,
            D_loss_config,
            metrics,
            metrics_prefix,
        )

    @tf.function
    def validation_step(
        self,
        noise_input,
        style_batch,
        noise_reconstructor,
        S_loss_config,
        D_loss_config,
        features_config,
        metrics,
        metrics_prefix='val',
    ):
        """
        Perform an iteration of validation

        Args:
            noise_input (tf.Tensor): a batch of noise input images
            style_batch (tf.Tensor): a batch of image + features for the style image
            noise_reconstructor (tf.Tensor): noise input to reconstruct the style image
            S_loss_config (namespace): configurations for style net's loss weighting
            D_loss_config (namespace): configurations for discriminator's loss weighting
            features_config (namespace): configurations for features levels and weights
            metrics (dict): contains the accumulators for the metrics' evaluation
            metrics_prefix (str): prefix to add for the metrics keys
        """
        self.validate_style_net(
            noise_input,
            style_batch,
            noise_reconstructor,
            S_loss_config,
            features_config,
            metrics,
            metrics_prefix,
        )
        self.validate_discriminator(
            noise_input, noise_reconstructor, D_loss_config, metrics, metrics_prefix
        )

    def iteration_step(
        self,
        noise_input,
        style_features_set,
        style_net_optimizer,
        discriminator_optimizer,
        S_loss_config,
        D_loss_config,
        features_config,
        metrics,
    ):
        """
        Perform an iteration of training or validation by updating the weights of
        the style net / discriminator if needed

        Args:
            noise_input (tf.Tensor): a batch of noise input images
            style_features_set (tf.data.Dataset): contains the style image + its features
                                                  (image, feature_1, feature_2, ...)
            style_net_optimizer (keras.optimizers.Optimizer): the optimizer for
                                                              the style net
            discriminator_optimizer (keras.optimizers.Optimizer): the optimizer for
                                                                  the discriminator
            S_loss_config (namespace): configurations for style net's loss weighting
            D_loss_config (namespace): configurations for discriminator's loss weighting
            features_config (namespace): configurations for features levels and weights
            metrics (dict): contains the accumulators for the metrics' evaluation
        """
        for style_features in style_features_set:
            if style_net_optimizer is None:
                self.validation_step(
                    noise_input,
                    style_features,
                    self.noise_reconstructor,
                    S_loss_config,
                    D_loss_config,
                    features_config,
                    metrics,
                )
            else:
                self.train_step(
                    noise_input,
                    style_features,
                    self.noise_reconstructor,
                    style_net_optimizer,
                    discriminator_optimizer,
                    S_loss_config,
                    D_loss_config,
                    features_config,
                    metrics,
                )

    def train(self, training_config, training_set, validation_set):
        """
        Perform the training process

        Args:
            training_config (namespace): contains configurations for training
            training_set (tf.data.Dataset): the training set
            validation_set (tf.data.Dataset): the validation set
        """
        self.create_noise_reconstructor_checkpoint_manager()

        # style features
        _, style_dataset = self.get_style_images_datasets()
        style_features_set = create_features_dataset(
            style_dataset,
            self.feature_extractor,
            self.model_config.style_feature_layers,
            include_inputs=True,
        )

        # cache extracted features on disk if enabled
        if training_config.cache_data:
            cached_data_path = join(self.model_path, 'cached_data')
            shutil.rmtree(cached_data_path, ignore_errors=True)
            makedirs(cached_data_path)

            style_features_set = style_features_set.cache(
                join(cached_data_path, 'style_features')
            )

        style_net_config = d2n(training_config.style_net)
        discriminator_config = d2n(training_config.discriminator)
        S_loss_config = d2n(
            dict(
                lambda_style=style_net_config.lambda_style,
                lambda_reconstruction=style_net_config.lambda_reconstruction,
                lambda_adversarial=style_net_config.lambda_adversarial,
                total_variation=style_net_config.total_variation,
            )
        )
        D_loss_config = d2n(
            dict(lambda_gradient_penalty=discriminator_config.lambda_gradient_penalty)
        )
        features_config = d2n(
            dict(
                style_feature_layers=self.model_config.style_feature_layers,
                style_feature_weights=self.model_config.style_feature_weights,
            )
        )

        summary = d2n(training_config.summary)
        checkpoint_copy = d2n(training_config.checkpoint_copy)
        style_net_optimizer = create_optimizer(style_net_config, training_config.epochs)
        discriminator_optimizer = create_optimizer(
            discriminator_config, training_config.epochs
        )

        # set default (null) values for summary configs if not provided
        for attribute in get_summary_attributes():
            setattr(summary, attribute, getattr(summary, attribute, 0))

        if summary.graph:
            tf.summary.trace_on(graph=True, profiler=True)
            summarized_graph_already = False
        if summary.hparams:
            self.initialize_hparams(
                list(self.get_summary_hparams(training_config).keys()),
                metric='total_loss',
                display_name='Loss',
            )

        # collect data samples for summaries
        training_summary_set = get_dataset_samples_for_summaries(
            training_set, [None, None, 3], summary.image_samples
        )
        validation_summary_set = get_dataset_samples_for_summaries(
            validation_set, [None, None, 3], summary.image_samples
        )

        self.adversarial_loss = get_generator_loss_function(
            self.model_config.discriminator['objective']
        )
        self.discriminator_loss = get_discriminator_loss_function(
            self.model_config.discriminator['objective']
        )
        self.discriminator_threshold = get_discriminator_threshold(
            self.model_config.discriminator['objective']
        )

        training_data_size = 0
        validation_data_size = 0

        metrics_to_monitor = [
            'trn_total_loss',
            'trn_style_loss',
            'trn_variation_loss',
            'trn_rec_error',
            'trn_adv_loss',
            'trn_D_total_loss',
            'trn_D_acc',
            'val_total_loss',
            'val_style_loss',
            'val_variation_loss',
            'val_rec_error',
            'val_adv_loss',
            'val_D_total_loss',
            'val_D_acc',
        ]
        if discriminator_config.lambda_gradient_penalty > 0:
            metrics_to_monitor.extend(
                ['trn_D_loss', 'trn_D_gp', 'val_D_loss', 'val_D_gp']
            )
        metrics = {
            metric_name: tf.keras.metrics.Mean(metric_name, dtype=tf.float32)
            for metric_name in metrics_to_monitor
        }
        best_losses = {'val_total_loss': inf}

        # compute RGB eigenvectors for the style image
        style_tensor = next(iter(style_dataset))
        style_eigenvectors = get_channel_eigenvectors(style_tensor)

        # summarize the style image
        if summary.image_samples_frequency:
            style_image_summaries = dict(Style=tensor2image(style_tensor))
            self.summarize_images(style_image_summaries, 'training', 0)

        # summarize the style images RGB distributions
        if summary.colour_distributions_frequency:
            style_colour_summaries = {}

            # iterate over all 3 eigenvectors but reverse their summary order
            for i in range(3):
                tensor_name = f'Style Eigenvector {i + 1}'
                tensor_value = project_image_to_featurespace(
                    style_tensor, style_eigenvectors[:, (3 - i - 1) : (3 - i)]
                )
                style_colour_summaries[tensor_name] = tensor_value

            self.summarize_distributions(style_colour_summaries, 'training', 0)

        # start the training iterations
        starting_epoch = self.epoch.value()
        for epoch in trange(
            starting_epoch,
            starting_epoch + training_config.epochs,
            desc='{:^21}'.format('Training Progress'),
        ):
            self.epoch.assign(epoch)

            # train
            training_size_counter = 0
            for noise_batch in tqdm(
                training_set,
                desc='{:<21}'.format('Epoch {} (training)'.format(epoch)),
                total=training_data_size,
            ):
                self.iteration_step(
                    noise_batch,
                    style_features_set,
                    style_net_optimizer,
                    discriminator_optimizer,
                    S_loss_config,
                    D_loss_config,
                    features_config,
                    metrics,
                )
                if summary.graph and not summarized_graph_already:
                    self.summarize_graph()
                    summarized_graph_already = True
                training_size_counter += 1

            training_data_size = training_size_counter

            # validate
            validation_size_counter = 0
            for noise_batch in tqdm(
                validation_set,
                desc='{:<21}'.format('Epoch {} (validation)'.format(epoch)),
                total=validation_data_size,
            ):
                self.iteration_step(
                    noise_batch,
                    style_features_set,
                    None,
                    None,
                    S_loss_config,
                    D_loss_config,
                    features_config,
                    metrics,
                )
                validation_size_counter += 1

            validation_data_size = validation_size_counter

            # collect metrics for display
            display_training_dict = dict(
                D_loss=metrics['trn_D_total_loss'].result().numpy(),
                D_acc=metrics['trn_D_acc'].result().numpy(),
                style_loss=metrics['trn_style_loss'].result().numpy(),
                rec_error=metrics['trn_rec_error'].result().numpy(),
                adv_loss=metrics['trn_adv_loss'].result().numpy(),
                variation_loss=metrics['trn_variation_loss'].result().numpy(),
                total_loss=metrics['trn_total_loss'].result().numpy(),
            )
            display_validation_dict = dict(
                D_loss=metrics['val_D_total_loss'].result().numpy(),
                D_acc=metrics['val_D_acc'].result().numpy(),
                style_loss=metrics['val_style_loss'].result().numpy(),
                rec_error=metrics['val_rec_error'].result().numpy(),
                adv_loss=metrics['val_adv_loss'].result().numpy(),
                variation_loss=metrics['val_variation_loss'].result().numpy(),
                total_loss=metrics['val_total_loss'].result().numpy(),
            )

            # display metrics
            print_metrics(display_training_dict, display_validation_dict)

            # write hyperparameters summaries for tensorboard
            if summary.hparams:
                self.summarize_hparams(self.get_summary_hparams(training_config))

            # write scalar summaries for tensorboard
            training_scalars = {
                key[4:]: value.result()
                for key, value in metrics.items()
                if key.startswith('trn')
            }
            validation_scalars = {
                key[4:]: value.result()
                for key, value in metrics.items()
                if key.startswith('val')
            }
            self.summarize_scalars(training_scalars, 'training', epoch)
            self.summarize_scalars(validation_scalars, 'validation', epoch)

            # write image summaries for tensorboard
            if summary.image_samples_frequency:
                if epoch % summary.image_samples_frequency == 0:
                    training_images = self.get_summary_images(
                        training_summary_set, epoch == 0, True
                    )
                    validation_images = self.get_summary_images(
                        validation_summary_set, epoch == 0, False
                    )
                    self.summarize_images(training_images, 'training', epoch)
                    self.summarize_images(validation_images, 'validation', epoch)

            # write colour distributions summaries for tensorboard
            if summary.colour_distributions_frequency:
                if epoch % summary.colour_distributions_frequency == 0:
                    training_distributions = self.get_summary_colour_distributions(
                        training_summary_set, style_eigenvectors
                    )
                    validation_distributions = self.get_summary_colour_distributions(
                        validation_summary_set, style_eigenvectors
                    )
                    self.summarize_distributions(
                        training_distributions, 'training', epoch
                    )
                    self.summarize_distributions(
                        validation_distributions, 'validation', epoch
                    )

            # store checkpoints
            self.save_epoch()
            best_losses = self.save_model_based_on_performance(metrics, best_losses)

            if (
                checkpoint_copy.enabled
                and epoch >= checkpoint_copy.starting_epoch
                and epoch % checkpoint_copy.frequency == 0
            ):
                copy_checkpoint_folder(self.model_path, self.checkpoints_path, epoch)

            # reset loss accumulators
            for metric_tensor in metrics.values():
                metric_tensor.reset_states()
