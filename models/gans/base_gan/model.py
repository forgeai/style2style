import os
import tensorflow as tf
from abc import ABC, abstractmethod
from tensorboard.plugins.hparams import api as hp

from models.factory.file_handling import create_model_paths, copy_model_config


class BaseGan(ABC):
    def __init__(self, model_config):
        # set config files
        self.model_config = model_config
        self.model_path, self.summaries_path, self.checkpoints_path = create_model_paths(
            model_config.name, model_config.project
        )

        # build the architecture
        self.build_model()
        self.initialize_epoch()

        if model_config.new:
            copy_model_config(model_config, self.model_path)
        else:
            self.create_epoch_checkpoint_manager()
            self.load_epoch()
            self.load_model()

    def initiate_new_version(self):
        """
        Create the folders required for a new version of the model

        Return:
            the version of the model (int)
        """
        _, self.summaries_path, self.checkpoints_path = create_model_paths(
            model_name=self.model_config.name,
            project=self.model_config.project,
            new_version=True,
        )
        return int(os.path.basename(os.path.dirname(self.checkpoints_path)))

    def create_epoch_checkpoint_manager(self):
        """
        Create the epoch checkpoint manager
        """
        self.epoch_checkpoint = tf.train.Checkpoint(epoch=self.epoch)
        self.epoch_checkpoint_manager = tf.train.CheckpointManager(
            checkpoint=self.epoch_checkpoint,
            directory=os.path.join(self.checkpoints_path, 'epoch'),
            max_to_keep=1,
        )

    def initialize_epoch(self):
        """
        Create the epoch tensor and set it to 0
        """
        self.epoch = tf.Variable(initial_value=0, trainable=False, dtype=tf.int64)

    def save_epoch(self):
        """
        Checkpoint the epoch value
        """
        self.epoch_checkpoint_manager.save()

    def load_epoch(self):
        """
        Load the epoch value from the checkpoint
        """
        self.epoch_checkpoint.restore(
            self.epoch_checkpoint_manager.latest_checkpoint
        ).assert_consumed()

    def open_summary_writers(self):
        """
        Open the writers that store summaries for tensorboard
        """
        self.summary_writer = {
            data_type: tf.summary.create_file_writer(
                os.path.join(self.summaries_path, data_type)
            )
            for data_type in ('training', 'validation')
        }

    def close_summary_writers(self):
        """
        Create the writers that store summaries for tensorboard
        """
        for writer in self.summary_writer.values():
            writer.close()

    def summarize_graph(self):
        """
        Build the network graph in the summaries and add profiling information
        """
        os.makedirs(
            os.path.join(self.summaries_path, 'training', 'plugins', 'profile'),
            exist_ok=True,
        )
        with self.summary_writer['training'].as_default():
            tf.summary.trace_export(
                name="network_trace",
                step=0,
                profiler_outdir=os.path.join(self.summaries_path, 'training'),
            )

    def summarize_scalars(self, summaries_dict, summaries_scope, epoch):
        """
        Write scalar tensors to summaries

        Args:
            summaries_dict (dict): maps tensor names to tensor values
            summaries_scope (str): 'training' or 'validation'
            epoch (int): the current epoch
        """
        assert summaries_scope in ('training', 'validation'), 'Invalid summary scope'

        with self.summary_writer[summaries_scope].as_default():
            for tensor_name, tensor_value in summaries_dict.items():
                tf.summary.scalar(tensor_name, tensor_value, step=epoch)

    def summarize_images(self, summaries_dict, summaries_scope, epoch):
        """
        Save image tensors to summaries

        Args:
            summaries_dict (dict): maps tensor names to tensor values
            summaries_scope (str): 'training' or 'validation'
            epoch (int): the current epoch
        """
        assert summaries_scope in ('training', 'validation'), 'Invalid summary scope'

        with self.summary_writer[summaries_scope].as_default():
            for tensor_name, tensor_value in summaries_dict.items():
                tf.summary.image(tensor_name, tensor_value, max_outputs=4, step=epoch)

    def summarize_distributions(self, summaries_dict, summaries_scope, epoch):
        """
        Plot tensor distributions and histograms for the provided data

        Args:
            summaries_dict (dict): maps tensor names to tensor values
            summaries_scope (str): 'training' or 'validation'
            epoch (int): the current epoch
        """
        assert summaries_scope in ('training', 'validation'), 'Invalid summary scope'

        with self.summary_writer[summaries_scope].as_default():
            for tensor_name, tensor_value in summaries_dict.items():
                tf.summary.histogram(tensor_name, tensor_value, step=epoch)

    def initialize_hparams(self, hparams_list, metric, display_name):
        """
        Initialize summary writer with the hyperparameters for the dashboard

        Args:
            hparams_list (list): contains hp.HParam objects
            metric (str): the identifier of the metric
            display_name (str): the display name for the metric
        """
        with self.summary_writer['validation'].as_default():
            hp.hparams_config(
                hparams=hparams_list,
                metrics=[hp.Metric(metric, display_name=display_name)],
            )

    def summarize_hparams(self, hparams):
        """
        Summarize results for the hyperparameter configuration

        Args:
            hparams (OrderedDict): maps hp.HParam object to specific values
        """
        with self.summary_writer['validation'].as_default():
            hp.hparams(hparams)

    @abstractmethod
    def build_model(self):
        pass

    @abstractmethod
    def load_model(self):
        pass

    @abstractmethod
    def save_model(self):
        pass

    @abstractmethod
    def train(self, training_config, training_set, validation_set):
        pass

    @abstractmethod
    def get_summary_hparams(self, training_config):
        pass

    @abstractmethod
    def get_summary_images(self, samples_dataset, include_inputs):
        pass

    @abstractmethod
    def prepare_datasets(self, data_config, training_config):
        pass
