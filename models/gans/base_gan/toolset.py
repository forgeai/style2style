import tensorflow as tf
import tensorflow.keras as keras
from tqdm import tqdm
from tqdm.utils import _term_move_up

from utils.constants import OPTIMIZERS


def create_optimizer(config, total_iterations):
    """
    Create an adam optimizer with polynomial decay

    Args:
        config (namespace): configurations for the behaviour
        total_iterations (int): total number of optimization steps
    Return:
        the optimizer (keras.optimizers.Adam)
    """
    assert config.optimizer in OPTIMIZERS, 'Optimizer must be "sgd", "adam" or "rmsprop"'

    lr_scheduler = keras.optimizers.schedules.PolynomialDecay(
        initial_learning_rate=config.initial_learning_rate,
        end_learning_rate=config.end_learning_rate,
        power=config.decay_polynomial_power,
        decay_steps=total_iterations,
    )
    if config.optimizer == 'sgd':
        return keras.optimizers.SGD(learning_rate=lr_scheduler)
    elif config.optimizer == 'adam':
        return keras.optimizers.Adam(learning_rate=lr_scheduler)
    return keras.optimizers.RMSprop(learning_rate=lr_scheduler)


def print_metrics(training_dict, validation_dict, cell_size=20, lines_to_flush=7):
    """
    Display and flush the value of the tracked metrics

    Args:
        training_dict (dict): the evaluation on the training set
        validation_dict (dict): the evaluation on the validation set
        cell_size (int): the size (in number of characters) of a table cell
        lines_to_flush (int): the number of lines to remove upwards before printing
    """
    assert set(training_dict.keys()) == set(
        validation_dict.keys()
    ), 'Training and Validation metrics do not match'

    tqdm.write((len(training_dict) + lines_to_flush) * (_term_move_up() + '\r'))
    tqdm.write((3 * cell_size + 4) * '-')
    tqdm.write(
        '|{1:^{0}}|{2:^{0}}|{3:^{0}}|'.format(
            cell_size, 'Metric', 'Training', 'Validation'
        )
    )
    tqdm.write('|{0}|{0}|{0}|'.format(cell_size * '-'))

    for metric in training_dict.keys():
        tqdm.write(
            '|{1:^{0}}|{2:^{0}.5f}|{3:^{0}.5f}|'.format(
                cell_size, metric, training_dict[metric], validation_dict[metric]
            )
        )
    tqdm.write((3 * cell_size + 4) * '-')


def get_dataset_samples_for_summaries(dataset, padded_shape, num_samples):
    """
    Create a dataset that contains only a few unchanging samples from a larger one
    and packs them in one batch

    Args:
        dataset (tf.data.Dataset): a presumably large dataset
        padded_shape (tuple): the shape of an element from the database
        num_samples (int): number of samples to have in the new dataset
    Return:
        dataset_samples (tf.data.Dataset)
    """
    subset = dataset.unbatch().take(num_samples).padded_batch(num_samples, padded_shape)
    return tf.data.Dataset.from_tensors([x for x in subset][0])


class DynamicValue:
    """
    Maintains a value that increases/decreases linearly over time

    Args:
        start_value (float): initial value
        end_value (float): final value where it will saturate
        steps (int): amount of updates required to reach the final value
    """

    def __init__(self, start_value, end_value, steps):
        self.end_value = end_value
        self.value = start_value
        self.delta = (end_value - start_value) / max(steps, 1)
        self.is_saturated = False

    def __call__(self):
        """
        Get the current value and update it afterwards
        """
        value = self.get_value()
        self.update_value()
        return value

    def get_value(self):
        """
        Get the current value without changing the state
        """
        return self.value

    def update_value(self):
        """
        Add or subtract the delta for the current state
        """
        if not self.is_saturated:
            self.value += self.delta

            if (self.delta > 0 and self.value >= self.end_value) or (
                self.delta < 0 and self.value <= self.end_value
            ):
                self.value = self.end_value
                self.is_saturated = True
