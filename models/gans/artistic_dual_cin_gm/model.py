import shutil
import tensorflow as tf
from math import inf
from os.path import join
from os import makedirs, listdir
from tqdm import tqdm, trange
from itertools import product

from models.gans.artistic_gm.style_network import create_style_network
from models.gans.artistic_cin_gm.model import ArtisticCinGm
from models.gans.artistic_base_gan.model import ArtisticBaseGan
from models.gans.artistic_base_gan.toolset import (
    get_channel_eigenvectors,
    get_summary_attributes,
    create_features_dataset,
    project_image_to_featurespace,
)
from models.gans.artistic_base_gan.feature_extractors import VggFeatureExtractor
from models.gans.base_gan.toolset import (
    print_metrics,
    create_optimizer,
    get_dataset_samples_for_summaries,
)
from models.factory.file_handling import copy_checkpoint_folder
from utils.json_processor import dict2namespace as d2n
from data_pipeline.factory.toolset import get_dataset_size


class ArtisticDualCinGm(ArtisticCinGm):
    def __init__(self, model_config, data_config=None):
        model_config.low_style_content_feature_layers = sorted(
            model_config.low_style_content_feature_layers
        )
        model_config.high_style_content_feature_layers = sorted(
            model_config.high_style_content_feature_layers
        )

        if data_config is not None:
            model_config.style_net['num_styles'] = 2 * len(
                listdir(data_config.style_images_path)
            )

        ArtisticBaseGan.__init__(self, model_config, data_config)

    def build_model(self):
        """
        Build a new model using the architectural configurations
        """
        self.style_net = create_style_network(d2n(self.model_config.style_net))
        layer_indices = set(
            self.model_config.low_style_content_feature_layers
            + self.model_config.high_style_content_feature_layers
            + self.model_config.low_style_feature_layers
            + self.model_config.high_style_feature_layers
        )
        self.feature_extractor = VggFeatureExtractor(layer_indices)

    def save_model_based_on_performance(self, current_losses, best_losses):
        """
        Save the style net based on the validation losses by serializing its
        architecture and weights. In the ArtisticDualCinGm the model is saved
        only if both validation losses are the best so far

        Args:
            current_losses (dict): contains specific loss value tensors
                                   from the current iteration
            best_losses (dict): contains the best specific loss values
                                encountered so far
        Return:
            best_losses (dict): the updated best loss values
        """
        ls_loss = current_losses['val_ls_total_loss'].result().numpy()
        hs_loss = current_losses['val_hs_total_loss'].result().numpy()

        if (
            ls_loss < best_losses['val_ls_total_loss']
            and hs_loss < best_losses['val_hs_total_loss']
        ):

            best_losses['val_ls_total_loss'] = ls_loss
            best_losses['val_hs_total_loss'] = hs_loss
            self.save_model()

        return best_losses

    def get_summary_hparams(self, training_config):
        """
        Not made for this
        The lightweight ArtisticCinGm should be used instead

        Args:
            training_config (namespace): config file for training
        Return:
            an empty dictionary
        """
        return {}

    def iteration_step(
        self,
        content_batch,
        style_features_set,
        optimizer,
        loss_config,
        features_config,
        is_high_style,
        metrics,
    ):
        """
        Perform an iteration of training or validation by updating the weights of
        the style net if needed. Do a separate run for each style and emphasis
        (low or high)

        Args:
            content_batch (tf.Tensor): a batch of content images that includes extracted
                                       features (image, feature_1, feature_2, ...)
            style_features_set (tf.data.Dataset): contains features from the style images
                                                  (index, (feature_1, feature_2, ...))
            optimizer (keras.optimizers.Optimizer): the optimizer for the style net
            loss_config (namespace): configurations for the loss weighting
            features_config (namespace): configurations for features levels and weights
            is_high_style (bool): flag for optimizing the low / high styling
            metrics (dict): contains the accumulators for the metrics' evaluation
        """
        content_input = content_batch[0]
        content_features = content_batch[1:]
        num_styles = self.model_config.style_net['num_styles']
        metrics_prefix = 'hs' if is_high_style else 'ls'

        for style_index, style_features in style_features_set:
            condition_weights = tf.one_hot(
                is_high_style * num_styles // 2 + style_index, num_styles
            )

            if optimizer is None:
                self.validation_step(
                    content_input,
                    content_features,
                    style_features,
                    condition_weights,
                    loss_config,
                    features_config,
                    metrics,
                    'val_' + metrics_prefix,
                )
            else:
                self.train_step(
                    content_input,
                    content_features,
                    style_features,
                    condition_weights,
                    optimizer,
                    loss_config,
                    features_config,
                    metrics,
                    'trn_' + metrics_prefix,
                )

    def train(self, training_config, training_set, validation_set):
        """
        Perform the training process

        Args:
            training_config (namespace): contains configurations for training
            training_set (tf.data.Dataset): the training set
            validation_set (tf.data.Dataset): the validation set
        """
        # content features for low style
        content_feature_layers = sorted(
            list(
                set(
                    self.model_config.low_style_content_feature_layers
                    + self.model_config.high_style_content_feature_layers
                )
            )
        )
        ls_content_feature_indices = [0] + [
            i + 1
            for i, l in enumerate(content_feature_layers)
            if l in self.model_config.low_style_content_feature_layers
        ]
        hs_content_feature_indices = [0] + [
            i + 1
            for i, l in enumerate(content_feature_layers)
            if l in self.model_config.high_style_content_feature_layers
        ]
        content_features_training_set = create_features_dataset(
            training_set,
            self.feature_extractor,
            content_feature_layers,
            include_inputs=True,
        )
        content_features_validation_set = create_features_dataset(
            validation_set,
            self.feature_extractor,
            content_feature_layers,
            include_inputs=True,
        )

        # style features
        style_indices, style_dataset = self.get_style_images_datasets()
        low_style_features_set = tf.data.Dataset.zip(
            (
                style_indices,
                create_features_dataset(
                    style_dataset,
                    self.feature_extractor,
                    self.model_config.low_style_feature_layers,
                    include_inputs=False,
                ),
            )
        )
        high_style_features_set = tf.data.Dataset.zip(
            (
                style_indices,
                create_features_dataset(
                    style_dataset,
                    self.feature_extractor,
                    self.model_config.high_style_feature_layers,
                    include_inputs=False,
                ),
            )
        )

        # cache extracted features on disk if enabled
        if training_config.cache_data:
            cached_data_path = join(self.model_path, 'cached_data')
            shutil.rmtree(cached_data_path, ignore_errors=True)
            makedirs(cached_data_path)

            content_features_training_set = content_features_training_set.cache(
                join(cached_data_path, 'content_features_training')
            )
            content_features_validation_set = content_features_validation_set.cache(
                join(cached_data_path, 'content_features_validation')
            )
            low_style_features_set = low_style_features_set.cache(
                join(cached_data_path, 'low_style_features')
            )
            high_style_features_set = high_style_features_set.cache(
                join(cached_data_path, 'high_style_features')
            )

        style_net_config = d2n(training_config.style_net)
        ls_loss_config = d2n(
            dict(
                lambda_content=style_net_config.low_style_lambda_content,
                lambda_style=style_net_config.low_style_lambda_style,
                total_variation=style_net_config.total_variation,
            )
        )
        hs_loss_config = d2n(
            dict(
                lambda_content=style_net_config.high_style_lambda_content,
                lambda_style=style_net_config.high_style_lambda_style,
                total_variation=style_net_config.total_variation,
            )
        )
        ls_features_config = d2n(
            dict(
                content_feature_layers=self.model_config.low_style_content_feature_layers,
                content_feature_weights=self.model_config.low_style_content_feature_weights,  # noqa: E501
                style_feature_layers=self.model_config.low_style_feature_layers,
                style_feature_weights=self.model_config.low_style_feature_weights,
            )
        )
        hs_features_config = d2n(
            dict(
                content_feature_layers=self.model_config.high_style_content_feature_layers,  # noqa: E501
                content_feature_weights=self.model_config.high_style_content_feature_weights,  # noqa: E501
                style_feature_layers=self.model_config.high_style_feature_layers,
                style_feature_weights=self.model_config.high_style_feature_weights,
            )
        )

        summary = d2n(training_config.summary)
        checkpoint_copy = d2n(training_config.checkpoint_copy)
        optimizer = create_optimizer(style_net_config, training_config.epochs)

        # set default (null) values for summary configs if not provided
        for attribute in get_summary_attributes():
            setattr(summary, attribute, getattr(summary, attribute, 0))

        if summary.graph:
            tf.summary.trace_on(graph=True, profiler=True)
            summarized_graph_already = False

        # collect data samples for summaries
        training_summary_set = get_dataset_samples_for_summaries(
            training_set, [None, None, 3], summary.image_samples
        )
        validation_summary_set = get_dataset_samples_for_summaries(
            validation_set, [None, None, 3], summary.image_samples
        )

        training_data_size = get_dataset_size(training_set)
        validation_data_size = get_dataset_size(validation_set)

        metrics_to_monitor = [
            '{}_{}_{}_loss'.format(x, y, z)
            for x, y, z in product(
                ('trn', 'val'), ('ls', 'hs'), ('total', 'style', 'content', 'variation')
            )
        ]
        metrics = {
            metric_name: tf.keras.metrics.Mean(metric_name, dtype=tf.float32)
            for metric_name in metrics_to_monitor
        }
        best_losses = {'val_ls_total_loss': inf, 'val_hs_total_loss': inf}

        # compute RGB eigenvectors for style images
        style_eigenvectors = {}
        num_style_images = self.model_config.style_net['num_styles'] // 2

        for style_index, style_image in zip(style_indices, style_dataset):
            eigenvectors = get_channel_eigenvectors(style_image)
            style_eigenvectors[style_index.numpy()[0]] = eigenvectors
            style_eigenvectors[style_index.numpy()[0] + num_style_images] = eigenvectors

        # summarize the style images
        if summary.image_samples_frequency:
            style_image_summaries = self.get_summary_style_images(
                style_indices, style_dataset
            )
            self.summarize_images(style_image_summaries, 'training', 0)

        # summarize the style images RGB distributions
        if summary.colour_distributions_frequency:
            style_colour_summaries = {}

            # iterate over style images
            for style_image, (style_index, eigenvectors) in zip(
                style_dataset, style_eigenvectors.items()
            ):

                # iterate over all 3 eigenvectors but reverse their summary order
                for i in range(3):
                    tensor_name = f'Style {style_index} Eigenvector {i + 1}'
                    tensor_value = project_image_to_featurespace(
                        style_image, eigenvectors[:, (3 - i - 1) : (3 - i)]
                    )
                    style_colour_summaries[tensor_name] = tensor_value

            self.summarize_distributions(style_colour_summaries, 'training', 0)

        # start the training iterations
        starting_epoch = self.epoch.value()
        for epoch in trange(
            starting_epoch,
            starting_epoch + training_config.epochs,
            desc='{:^21}'.format('Training Progress'),
        ):
            self.epoch.assign(epoch)

            # train
            for content_batch in tqdm(
                content_features_training_set,
                desc='{:<21}'.format('Epoch {} (training)'.format(epoch)),
                total=training_data_size,
            ):
                ls_content_batch = tf.tuple(
                    [content_batch[i] for i in ls_content_feature_indices]
                )
                hs_content_batch = tf.tuple(
                    [content_batch[i] for i in hs_content_feature_indices]
                )

                self.iteration_step(
                    ls_content_batch,
                    low_style_features_set,
                    optimizer,
                    ls_loss_config,
                    ls_features_config,
                    False,
                    metrics,
                )
                self.iteration_step(
                    hs_content_batch,
                    high_style_features_set,
                    optimizer,
                    hs_loss_config,
                    hs_features_config,
                    True,
                    metrics,
                )
                if summary.graph and not summarized_graph_already:
                    self.summarize_graph()
                    summarized_graph_already = True

            # validate
            for content_batch in tqdm(
                content_features_validation_set,
                desc='{:<21}'.format('Epoch {} (validation)'.format(epoch)),
                total=validation_data_size,
            ):
                ls_content_batch = tf.tuple(
                    [content_batch[i] for i in ls_content_feature_indices]
                )
                hs_content_batch = tf.tuple(
                    [content_batch[i] for i in hs_content_feature_indices]
                )

                self.iteration_step(
                    ls_content_batch,
                    low_style_features_set,
                    None,
                    ls_loss_config,
                    ls_features_config,
                    False,
                    metrics,
                )
                self.iteration_step(
                    hs_content_batch,
                    high_style_features_set,
                    None,
                    hs_loss_config,
                    hs_features_config,
                    True,
                    metrics,
                )

            # collect metrics for display
            display_training_dict = dict(
                ls_content_loss=metrics['trn_ls_content_loss'].result().numpy(),
                ls_style_loss=metrics['trn_ls_style_loss'].result().numpy(),
                ls_variation_loss=metrics['trn_ls_variation_loss'].result().numpy(),
                ls_total_loss=metrics['trn_ls_total_loss'].result().numpy(),
                hs_content_loss=metrics['trn_hs_content_loss'].result().numpy(),
                hs_style_loss=metrics['trn_hs_style_loss'].result().numpy(),
                hs_variation_loss=metrics['trn_hs_variation_loss'].result().numpy(),
                hs_total_loss=metrics['trn_hs_total_loss'].result().numpy(),
            )
            display_validation_dict = dict(
                ls_content_loss=metrics['val_ls_content_loss'].result().numpy(),
                ls_style_loss=metrics['val_ls_style_loss'].result().numpy(),
                ls_variation_loss=metrics['val_ls_variation_loss'].result().numpy(),
                ls_total_loss=metrics['val_ls_total_loss'].result().numpy(),
                hs_content_loss=metrics['val_hs_content_loss'].result().numpy(),
                hs_style_loss=metrics['val_hs_style_loss'].result().numpy(),
                hs_variation_loss=metrics['val_hs_variation_loss'].result().numpy(),
                hs_total_loss=metrics['val_hs_total_loss'].result().numpy(),
            )

            # display metrics
            print_metrics(display_training_dict, display_validation_dict)

            # write scalar summaries for tensorboard
            training_scalars = {
                key[4:]: value.result()
                for key, value in metrics.items()
                if key.startswith('trn')
            }
            validation_scalars = {
                key[4:]: value.result()
                for key, value in metrics.items()
                if key.startswith('val')
            }
            self.summarize_scalars(training_scalars, 'training', epoch)
            self.summarize_scalars(validation_scalars, 'validation', epoch)

            # write image summaries for tensorboard
            if summary.image_samples_frequency:
                if epoch % summary.image_samples_frequency == 0:
                    training_images = self.get_summary_images(
                        training_summary_set, epoch == 0
                    )
                    validation_images = self.get_summary_images(
                        validation_summary_set, epoch == 0
                    )
                    self.summarize_images(training_images, 'training', epoch)
                    self.summarize_images(validation_images, 'validation', epoch)

            # write colour distributions summaries for tensorboard
            if summary.colour_distributions_frequency:
                if epoch % summary.colour_distributions_frequency == 0:
                    training_distributions = self.get_summary_colour_distributions(
                        training_summary_set, style_eigenvectors
                    )
                    validation_distributions = self.get_summary_colour_distributions(
                        validation_summary_set, style_eigenvectors
                    )
                    self.summarize_distributions(
                        training_distributions, 'training', epoch
                    )
                    self.summarize_distributions(
                        validation_distributions, 'validation', epoch
                    )

            # store checkpoints
            self.save_epoch()
            best_losses = self.save_model_based_on_performance(metrics, best_losses)

            if (
                checkpoint_copy.enabled
                and epoch >= checkpoint_copy.starting_epoch
                and epoch % checkpoint_copy.frequency == 0
            ):
                copy_checkpoint_folder(self.model_path, self.checkpoints_path, epoch)

            # reset loss accumulators
            for metric_tensor in metrics.values():
                metric_tensor.reset_states()
