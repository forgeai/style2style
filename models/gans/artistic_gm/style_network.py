import tensorflow as tf
import tensorflow.keras as keras
from static_variables import resolve_static

from models.factory.type_loader import get_model_type
from models.gans.artistic_base_gan.toolset import gram_matrix


def create_style_network(config):
    """
    Create an instance of a style network

    Args:
        config (namespace): contains layers parameterization
    Return:
        a style network (tf.keras.Model)
    """
    ComponentClass = get_model_type(config.type, is_component=True)
    config.output_activation = 'tanh'
    component_creator = ComponentClass(config, 'style_network')

    return component_creator.create_model()


@resolve_static(static_variables={'mse': keras.losses.MeanSquaredError()})
def content_loss(synthesized_features_batch, content_features_batch, layer_weights):
    """
    Compute the content loss between the model predictions and the original
    inputs. It consists of the difference in high-level features as extracted
    by a pre-trained classifier (Vgg16). More specifically it is the Euclidean
    distance between the activations from various levels in the network.

    Args:
        synthesized_features_batch (tf.Tensor): features extracted from the
                                                synthesized images
        content_features_batch (tf.Tensor): features extracted from the
                                            original inputs to the model
        layer_weights (list of floats): loss weighting for each layer
    Return:
        loss_value (tf.float32)
    """
    loss_value = 0.0

    for synth_features, content_features, loss_weight in zip(
        synthesized_features_batch, content_features_batch, layer_weights
    ):
        loss_value += loss_weight * mse(content_features, synth_features)

    return loss_value


@resolve_static(static_variables={'mse': keras.losses.MeanSquaredError()})
def style_loss(synthesized_features_batch, style_features_batch, layer_weights):
    """
    Compute the style loss between the model predictions and the style images.
    It consists of the difference in the statistics of low-level features extracted
    using a pre-trained classifier (Vgg16). More specifically, it is the Frobenius
    norm between the Gram matrices of the activations.

    Args:
        synthesized_features_batch (tf.Tensor): features extracted from the
                                                synthesized images
        style_features_batch (tf.Tensor): features extracted from the
                                          style image
        layer_weights (list of floats): loss weighting for each layer
    Return:
        loss_value (tf.float32)
    """
    loss_value = 0.0

    for synth_features, style_features, loss_weight in zip(
        synthesized_features_batch, style_features_batch, layer_weights
    ):
        synth_gram_matrix = gram_matrix(synth_features)
        style_gram_matrix = gram_matrix(style_features)

        image_shape = tf.shape(synth_features)
        norm_const = 4.0 * tf.cast(image_shape[1] * image_shape[2], tf.float32)
        loss_value += loss_weight / norm_const * mse(style_gram_matrix, synth_gram_matrix)

    return loss_value


def variation_loss(image):
    """
    Loss value for the pixel variation computed by averaging the absolute
    differences between all neighbouring pixels

    Args:
        image (tf.Tensor): a synthesized model output
    Return:
        loss value (tf.Tensor of tf.float32)
    """
    image_area = tf.shape(image)[-2] * tf.shape(image)[-3]
    total_variation_loss = tf.reduce_mean(tf.image.total_variation(image))

    return tf.cast(total_variation_loss, tf.float32) / tf.cast(image_area, tf.float32)
