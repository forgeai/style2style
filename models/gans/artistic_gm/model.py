import shutil
import tensorflow as tf
from math import inf
from tqdm import tqdm, trange
from os.path import join
from os import makedirs, listdir

from utils.image_processor import tensor2image
from utils.json_processor import dict2namespace as d2n
from data_pipeline.factory.toolset import get_dataset_size
from models.gans.artistic_gm.toolset import get_hparams
from models.gans.artistic_gm.style_network import (
    style_loss,
    content_loss,
    variation_loss,
    create_style_network,
)
from models.gans.artistic_base_gan.model import ArtisticBaseGan
from models.gans.artistic_base_gan.feature_extractors import VggFeatureExtractor
from models.gans.artistic_base_gan.toolset import (
    get_channel_eigenvectors,
    get_summary_attributes,
    create_features_dataset,
    project_image_to_featurespace,
)
from models.gans.base_gan.toolset import (
    print_metrics,
    create_optimizer,
    get_dataset_samples_for_summaries,
)
from models.factory.file_handling import copy_checkpoint_folder
from models.factory.component_serialization import save_component, load_component
from models.components.auxiliaries.toolset import get_component_hparams_dict


class ArtisticGm(ArtisticBaseGan):
    def __init__(self, model_config, data_config=None):
        assert (
            len(listdir(data_config.style_images_path)) == 1
        ), 'ArtisticGm accepts only 1 style image'
        super().__init__(model_config, data_config)

    def build_model(self):
        """
        Build a new model using the architectural configurations
        """
        self.style_net = create_style_network(d2n(self.model_config.style_net))
        layer_indices = set(
            self.model_config.content_feature_layers
            + self.model_config.style_feature_layers
        )
        self.feature_extractor = VggFeatureExtractor(layer_indices)

    def load_model(self):
        """
        Load the architecture and the weights of the style net
        """
        component_path = join(self.checkpoints_path, 'style_net')
        self.style_net = load_component(component_path)

    def save_model(self):
        """
        Save the style net weights and dependencies, and create a model diagram
        if saving for the first time
        """
        component_path = join(self.checkpoints_path, 'style_net')
        component_config = d2n(self.model_config['style_net'])
        save_component(self.style_net, component_config, component_path)

    def save_model_based_on_performance(self, current_losses, best_losses):
        """
        Save the style net based on the validation loss by serializing its
        architecture and weights. In the ArtisticGm the model is saved
        only if the validation loss is the best so far

        Args:
            current_losses (dict): contains specific loss value tensors
                                   from the current iteration
            best_losses (dict): contains the best specific loss values
                                encountered so far
        Return:
            best_losses (dict): the updated best loss values
        """
        loss_of_interest = current_losses['val_total_loss'].result().numpy()

        if loss_of_interest < best_losses['val_total_loss']:
            best_losses['val_total_loss'] = loss_of_interest
            self.save_model()

        return best_losses

    def get_summary_hparams(self, training_config):
        """
        Create a dictionary of hparams to summarize

        Args:
            training_config (namespace): config file for training
        Return:
            hparams_dict (OrderedDict): maps hparams objects to actual values
        """
        gan_hparams = get_hparams()
        hparams_dict = OrderedDict()

        # style_net hparams
        style_net_config = d2n(self.model_config.style_net)
        style_net_hparams = get_component_hparams_dict(style_net_config)
        for hparam, value in style_net_hparams.items():
            hparams_dict[gan_hparams[hparam]] = value

        # training hparams
        for hparam in ('lambda_style', 'lambda_content', 'total_variation'):
            hparams_dict[gan_hparams[hparam]] = training_config.style_net[hparam]

        return hparams_dict

    def get_summary_images(self, samples_dataset, include_inputs):
        """
        Create a dictionary of images to summarize

        Args:
            samples_dataset (tf.data.Dataset): a dataset of content images
            include_inputs (bool): whether to include images that do not
                                   depend on the model evolution and thus
                                   do not change over the course of epochs
        Return:
            summaries_dict (dict): maps summary names to image tensors
        """
        summaries_dict = {}

        for sample_images in samples_dataset:
            if include_inputs:
                summaries_dict['Input Image'] = tensor2image(sample_images)
            synthesized_image = self.style_net(sample_images)
            summaries_dict['Synthesized'] = tensor2image(synthesized_image)

        return summaries_dict

    def get_summary_colour_distributions(self, dataset, style_eigenvectors):
        """
        Create a dictionary of colour distributions to summarize

        Args:
            dataset (tf.data.Dataset): a dataset of content images
            style_eigenvectors (dict): the style eigenvectors matrix
        Return:
            summaries_dict (dict): maps summary names to image tensors
        """
        summaries_dict = {}

        synthesized_batch_list = []
        for image_batch in dataset:
            synthesized_batch_list.append(self.style_net(image_batch))

        synthesized_stack = tf.concat(synthesized_batch_list, axis=0)
        for i in range(3):
            tensor_name = f'Style Eigenvector {i + 1} synthesized data'
            tensor_value = project_image_to_featurespace(
                synthesized_stack, style_eigenvectors[:, (3 - i - 1) : (3 - i)]
            )
            summaries_dict[tensor_name] = tensor_value

        return summaries_dict

    @tf.function
    def train_step(
        self,
        content_input,
        content_features,
        style_features,
        optimizer,
        loss_config,
        features_config,
        metrics,
        metrics_prefix='trn',
    ):
        """
        Perform an iteration of training

        Args:
            content_input (tf.Tensor): a batch of content input images
            content_features (tf.Tensor): a batch of features for the content loss
            style_features (tf.Tensor): a batch of features for the style loss
            optimizer (keras.optimizers.Optimizer): the optimizer for the style net
            loss_config (namespace): configurations for the loss weighting
            features_config (namespace): configurations for features levels and weights
            metrics (dict): contains the accumulators for the metrics' evaluation
            metrics_prefix (str): prefix to add for the metrics keys
        """
        with tf.GradientTape() as tape:
            synthesized_batch = self.style_net(content_input, training=True)

            # content loss
            synthesized_content_features = tuple(
                [
                    self.feature_extractor(synthesized_batch, layer_number)
                    for layer_number in features_config.content_feature_layers
                ]
            )
            content_loss_value = content_loss(
                synthesized_content_features,
                content_features,
                features_config.content_feature_weights,
            )

            # style loss
            synthesized_style_features = tuple(
                [
                    self.feature_extractor(synthesized_batch, layer_number)
                    for layer_number in features_config.style_feature_layers
                ]
            )
            style_loss_value = style_loss(
                synthesized_style_features,
                style_features,
                features_config.style_feature_weights,
            )

            # total variation loss
            variation_loss_value = variation_loss(synthesized_batch)

            # final loss
            total_loss = (
                loss_config.lambda_content * content_loss_value
                + loss_config.lambda_style * style_loss_value
                + loss_config.total_variation * variation_loss_value
            )

        metrics[metrics_prefix + '_total_loss'](total_loss)
        metrics[metrics_prefix + '_style_loss'](style_loss_value)
        metrics[metrics_prefix + '_content_loss'](content_loss_value)
        metrics[metrics_prefix + '_variation_loss'](variation_loss_value)

        # apply gradients
        gradients = tape.gradient(total_loss, self.style_net.trainable_variables)
        optimizer.apply_gradients(zip(gradients, self.style_net.trainable_variables))

    @tf.function
    def validation_step(
        self,
        content_input,
        content_features,
        style_features,
        loss_config,
        features_config,
        metrics,
        metrics_prefix='val',
    ):
        """
        Perform an iteration of validation for a single style

        Args:
            content_input (tf.Tensor): a batch of content input images
            content_features (tf.Tensor): a batch of features for the content loss
            style_features (tf.Tensor): a batch of features for the style loss
            loss_config (namespace): configurations for the loss weighting
            features_config (namespace): configurations for features levels and weights
            metrics (dict): contains the accumulators for the metrics' evaluation
            metrics_prefix (str): prefix to add for the metrics keys
        """
        synthesized_batch = self.style_net(content_input, training=False)

        # content loss
        synthesized_content_features = tuple(
            [
                self.feature_extractor(synthesized_batch, layer_number)
                for layer_number in features_config.content_feature_layers
            ]
        )
        content_loss_value = content_loss(
            synthesized_content_features,
            content_features,
            features_config.content_feature_weights,
        )

        # style loss
        synthesized_style_features = tuple(
            [
                self.feature_extractor(synthesized_batch, layer_number)
                for layer_number in features_config.style_feature_layers
            ]
        )
        style_loss_value = style_loss(
            synthesized_style_features,
            style_features,
            features_config.style_feature_weights,
        )

        # total variation loss
        variation_loss_value = variation_loss(synthesized_batch)

        # final loss
        total_loss = (
            loss_config.lambda_content * content_loss_value
            + loss_config.lambda_style * style_loss_value
            + loss_config.total_variation * variation_loss_value
        )

        metrics[metrics_prefix + '_total_loss'](total_loss)
        metrics[metrics_prefix + '_style_loss'](style_loss_value)
        metrics[metrics_prefix + '_content_loss'](content_loss_value)
        metrics[metrics_prefix + '_variation_loss'](variation_loss_value)

    def iteration_step(
        self,
        content_batch,
        style_features_set,
        optimizer,
        loss_config,
        features_config,
        metrics,
    ):
        """
        Perform an iteration of training or validation by updating the weights of
        the style net if needed

        Args:
            content_batch (tf.Tensor): a batch of content images that includes extracted
                                       features (image, feature_1, feature_2, ...)
            style_features_set (tf.data.Dataset): contains features from the style image
                                                  (feature_1, feature_2, ...)
            optimizer (keras.optimizers.Optimizer): the optimizer for the style net
            loss_config (namespace): configurations for the loss weighting
            features_config (namespace): configurations for features levels and weights
            metrics (dict): contains the accumulators for the metrics' evaluation
        """
        content_input = content_batch[0]
        content_features = content_batch[1:]

        for style_features in style_features_set:
            if optimizer is None:
                self.validation_step(
                    content_input,
                    content_features,
                    style_features,
                    loss_config,
                    features_config,
                    metrics,
                )
            else:
                self.train_step(
                    content_input,
                    content_features,
                    style_features,
                    optimizer,
                    loss_config,
                    features_config,
                    metrics,
                )

    def train(self, training_config, training_set, validation_set):
        """
        Perform the training process

        Args:
            training_config (namespace): contains configurations for training
            training_set (tf.data.Dataset): the training set
            validation_set (tf.data.Dataset): the validation set
        """
        # content features
        content_features_training_set = create_features_dataset(
            training_set,
            self.feature_extractor,
            self.model_config.content_feature_layers,
            include_inputs=True,
        )
        content_features_validation_set = create_features_dataset(
            validation_set,
            self.feature_extractor,
            self.model_config.content_feature_layers,
            include_inputs=True,
        )

        # style features
        _, style_dataset = self.get_style_images_datasets()
        style_features_set = create_features_dataset(
            style_dataset,
            self.feature_extractor,
            self.model_config.style_feature_layers,
            include_inputs=False,
        )

        # cache extracted features on disk if enabled
        if training_config.cache_data:
            cached_data_path = join(self.model_path, 'cached_data')
            shutil.rmtree(cached_data_path, ignore_errors=True)
            makedirs(cached_data_path)

            content_features_training_set = content_features_training_set.cache(
                join(cached_data_path, 'content_features_training')
            )
            content_features_validation_set = content_features_validation_set.cache(
                join(cached_data_path, 'content_features_validation')
            )
            style_features_set = style_features_set.cache(
                join(cached_data_path, 'style_features')
            )

        style_net_config = d2n(training_config.style_net)
        loss_config = d2n(
            dict(
                lambda_content=style_net_config.lambda_content,
                lambda_style=style_net_config.lambda_style,
                total_variation=style_net_config.total_variation,
            )
        )
        features_config = d2n(
            dict(
                content_feature_layers=self.model_config.content_feature_layers,
                content_feature_weights=self.model_config.content_feature_weights,
                style_feature_layers=self.model_config.style_feature_layers,
                style_feature_weights=self.model_config.style_feature_weights,
            )
        )

        summary = d2n(training_config.summary)
        checkpoint_copy = d2n(training_config.checkpoint_copy)
        optimizer = create_optimizer(style_net_config, training_config.epochs)

        # set default (null) values for summary configs if not provided
        for attribute in get_summary_attributes():
            setattr(summary, attribute, getattr(summary, attribute, 0))

        if summary.graph:
            tf.summary.trace_on(graph=True, profiler=True)
            summarized_graph_already = False
        if summary.hparams:
            self.initialize_hparams(
                list(self.get_summary_hparams(training_config).keys()),
                metric='total_loss',
                display_name='Loss',
            )

        # collect data samples for summaries
        training_summary_set = get_dataset_samples_for_summaries(
            training_set, [None, None, 3], summary.image_samples
        )
        validation_summary_set = get_dataset_samples_for_summaries(
            validation_set, [None, None, 3], summary.image_samples
        )

        training_data_size = get_dataset_size(training_set)
        validation_data_size = get_dataset_size(validation_set)

        metrics_to_monitor = (
            'trn_total_loss',
            'trn_style_loss',
            'trn_content_loss',
            'trn_variation_loss',
            'val_total_loss',
            'val_style_loss',
            'val_content_loss',
            'val_variation_loss',
        )
        metrics = {
            metric_name: tf.keras.metrics.Mean(metric_name, dtype=tf.float32)
            for metric_name in metrics_to_monitor
        }
        best_losses = {'val_total_loss': inf}

        # compute RGB eigenvectors for the style image
        style_tensor = next(iter(style_dataset))
        style_eigenvectors = get_channel_eigenvectors(style_tensor)

        # summarize the style image
        if summary.image_samples_frequency:
            style_image_summaries = dict(Style=tensor2image(style_tensor))
            self.summarize_images(style_image_summaries, 'training', 0)

        # summarize the style images RGB distributions
        if summary.colour_distributions_frequency:
            style_colour_summaries = {}

            # iterate over all 3 eigenvectors but reverse their summary order
            for i in range(3):
                tensor_name = f'Style Eigenvector {i + 1}'
                tensor_value = project_image_to_featurespace(
                    style_tensor, style_eigenvectors[:, (3 - i - 1) : (3 - i)]
                )
                style_colour_summaries[tensor_name] = tensor_value

            self.summarize_distributions(style_colour_summaries, 'training', 0)

        # start the training iterations
        starting_epoch = self.epoch.value()
        for epoch in trange(
            starting_epoch,
            starting_epoch + training_config.epochs,
            desc='{:^21}'.format('Training Progress'),
        ):
            self.epoch.assign(epoch)

            # train
            for content_batch in tqdm(
                content_features_training_set,
                desc='{:<21}'.format('Epoch {} (training)'.format(epoch)),
                total=training_data_size,
            ):
                self.iteration_step(
                    content_batch,
                    style_features_set,
                    optimizer,
                    loss_config,
                    features_config,
                    metrics,
                )
                if summary.graph and not summarized_graph_already:
                    self.summarize_graph()
                    summarized_graph_already = True

            # validate
            for content_batch in tqdm(
                content_features_validation_set,
                desc='{:<21}'.format('Epoch {} (validation)'.format(epoch)),
                total=validation_data_size,
            ):
                self.iteration_step(
                    content_batch,
                    style_features_set,
                    None,
                    loss_config,
                    features_config,
                    metrics,
                )

            # collect metrics for display
            display_training_dict = dict(
                content_loss=metrics['trn_content_loss'].result().numpy(),
                style_loss=metrics['trn_style_loss'].result().numpy(),
                variation_loss=metrics['trn_variation_loss'].result().numpy(),
                total_loss=metrics['trn_total_loss'].result().numpy(),
            )
            display_validation_dict = dict(
                content_loss=metrics['val_content_loss'].result().numpy(),
                style_loss=metrics['val_style_loss'].result().numpy(),
                variation_loss=metrics['val_variation_loss'].result().numpy(),
                total_loss=metrics['val_total_loss'].result().numpy(),
            )

            # display metrics
            print_metrics(display_training_dict, display_validation_dict)

            # write hyperparameters summaries for tensorboard
            if summary.hparams:
                self.summarize_hparams(self.get_summary_hparams(training_config))

            # write scalar summaries for tensorboard
            training_scalars = {
                key[4:]: value.result()
                for key, value in metrics.items()
                if key.startswith('trn')
            }
            validation_scalars = {
                key[4:]: value.result()
                for key, value in metrics.items()
                if key.startswith('val')
            }
            self.summarize_scalars(training_scalars, 'training', epoch)
            self.summarize_scalars(validation_scalars, 'validation', epoch)

            # write image summaries for tensorboard
            if summary.image_samples_frequency:
                if epoch % summary.image_samples_frequency == 0:
                    training_images = self.get_summary_images(
                        training_summary_set, epoch == 0
                    )
                    validation_images = self.get_summary_images(
                        validation_summary_set, epoch == 0
                    )
                    self.summarize_images(training_images, 'training', epoch)
                    self.summarize_images(validation_images, 'validation', epoch)

            # write colour distributions summaries for tensorboard
            if summary.colour_distributions_frequency:
                if epoch % summary.colour_distributions_frequency == 0:
                    training_distributions = self.get_summary_colour_distributions(
                        training_summary_set, style_eigenvectors
                    )
                    validation_distributions = self.get_summary_colour_distributions(
                        validation_summary_set, style_eigenvectors
                    )
                    self.summarize_distributions(
                        training_distributions, 'training', epoch
                    )
                    self.summarize_distributions(
                        validation_distributions, 'validation', epoch
                    )

            # store checkpoints
            self.save_epoch()
            best_losses = self.save_model_based_on_performance(metrics, best_losses)

            if (
                checkpoint_copy.enabled
                and epoch >= checkpoint_copy.starting_epoch
                and epoch % checkpoint_copy.frequency == 0
            ):
                copy_checkpoint_folder(self.model_path, self.checkpoints_path, epoch)

            # reset loss accumulators
            for metric_tensor in metrics.values():
                metric_tensor.reset_states()
