from collections import OrderedDict
from tensorboard.plugins.hparams import api as hp

from utils.constants import UPSAMPLING_METHODS, CONDITIONAL_COMPONENTS


def get_hparams():
    """
    Return a dictionary of all hyperparameters' ranges for the dashboard

    Return:
        (OrderedDict): maps hyperparameter name to a hp.HParam object
    """
    return OrderedDict(
        type=hp.HParam('type', hp.Discrete(CONDITIONAL_COMPONENTS)),
        dropout=hp.HParam('dropout', hp.RealInterval(0.0, 1.0)),
        l2_reg=hp.HParam('l2_reg', hp.RealInterval(0.0, 10.0)),
        upsampling=hp.HParam('upsampling', hp.Discrete(UPSAMPLING_METHODS)),
        lambda_style=hp.HParam('lambda_style', hp.RealInterval(0.0, 1.0)),
        lambda_content=hp.HParam('lambda_content', hp.RealInterval(0.0, 1.0)),
        total_variation=hp.HParam('total_variation', hp.RealInterval(0.0, 1e5)),
    )
