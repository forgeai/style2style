import tqdm
import logging
import tensorflow as tf
from os.path import join

from utils.json_processor import json2dict
from data_pipeline.tfrecords.handler import image2example


def serialize_tfrecords(images_path, tfrecords_path, samples_per_record):
    """
    Serialize the entire content of a directory containing images into tfrecords

    Args:
        images_path (str): path to the directory with images (read)
        tfrecords_path (str): path to the directory that will contain
                              the tfrecords (write)
        samples_per_record (int): number of samples to encapsulate in one record
    """
    logger = logging.getLogger(__name__)
    allocation = json2dict(join(images_path, '#data_allocation.json'))

    for data_slice in ('training', 'validation', 'testing'):
        image_paths = [join(images_path, img) for img in allocation[data_slice]]

        for low in tqdm.tqdm(
            range(0, len(image_paths), samples_per_record),
            desc=f'\t{data_slice} set',
            disable=logger.getEffectiveLevel() > logging.INFO,
        ):
            high = min(low + samples_per_record, len(image_paths))

            record_path = join(
                tfrecords_path,
                '{}_{:04d}.tfrecord'.format(data_slice, low // samples_per_record),
            )
            with tf.io.TFRecordWriter(record_path) as writer:
                for image_path in image_paths[low:high]:
                    tf_example = image2example(image_path)
                    writer.write(tf_example.SerializeToString())
