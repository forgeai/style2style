import tensorflow as tf


def _bytes_feature(value):
    """
    Returns a bytes_list from a string / byte
    """
    if isinstance(value, type(tf.constant(0))):
        value = value.numpy()  # BytesList won't unpack a string from an EagerTensor.
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def _float_feature(value):
    """
    Returns a float_list from a float / double
    """
    return tf.train.Feature(float_list=tf.train.FloatList(value=[value]))


def _int64_feature(value):
    """
    Returns an int64_list from a bool / enum / int / uint
    """
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def image2example(image_path):
    """
    Create a tf.train.Example dictionary with data from an image

    Args:
        image_path (str): path to the image
    Return:
        (tf.train.Example): contains the image data
    """
    with open(image_path, 'rb') as image_file:
        image_string = image_file.read()
    image_shape = tf.image.decode_image(image_string).shape

    feature = {
        'height': _int64_feature(image_shape[0]),
        'width': _int64_feature(image_shape[1]),
        'depth': _int64_feature(image_shape[2]),
        'image_raw': _bytes_feature(image_string),
    }
    return tf.train.Example(features=tf.train.Features(feature=feature))


def example2image(example_proto):
    """
    Parse a tf.train.Example proto to recover the original image

    Args:
        example_proto (tf.train.Example): the example protobuf
    Return:
        (tf.Tensor): the unpacked image
    """
    image_feature_description = {
        'height': tf.io.FixedLenFeature([], tf.int64),
        'width': tf.io.FixedLenFeature([], tf.int64),
        'depth': tf.io.FixedLenFeature([], tf.int64),
        'image_raw': tf.io.FixedLenFeature([], tf.string),
    }
    image_features = tf.io.parse_single_example(example_proto, image_feature_description)

    height = image_features['height']
    width = image_features['width']
    depth = image_features['depth']
    flat_image = tf.io.decode_image(image_features['image_raw'], dtype=tf.uint8)

    return tf.reshape(flat_image, (height, width, depth))
