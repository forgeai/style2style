import os
import tensorflow as tf

from utils.image_processor import image2tensor
from data_pipeline.tfrecords.handler import example2image


def tfrecords2dataset(
    data_path, data_slices=('training', 'validation', 'testing'), overlapping_datasets=1
):
    """
    Load tfrecords from disk and return a dictionary of dataset objects
    mapping all records allocated to the slices specified by the data_slices

    Args:
        data_path (str): path to the directory containing the tfrecords
        data_slices (tuple / list): the required slices of data
                                    (training, testing , ...)
        overlapping_datasets (int): the tf cycle length for reading data
    Return:
        datasets_dict (dict: str -> tf.data.Dataset)
    """
    assert isinstance(
        data_slices, (list, tuple)
    ), 'Parameter data_slices must be a list or a tuple'
    assert all(
        [x in ('training', 'validation', 'testing') for x in data_slices]
    ), 'Invalid data slice requested by the get_dataset'

    datasets_dict = {}
    for data_slice in data_slices:

        files = tf.data.Dataset.list_files(
            os.path.join(data_path, '{}_*.tfrecord'.format(data_slice))
        )
        datasets_dict[data_slice] = (
            files.interleave(
                tf.data.TFRecordDataset,
                cycle_length=overlapping_datasets,
                num_parallel_calls=tf.data.experimental.AUTOTUNE,
            )
            .map(map_func=example2image, num_parallel_calls=tf.data.experimental.AUTOTUNE)
            .map(
                map_func=lambda x: image2tensor(
                    tf.image.convert_image_dtype(x, dtype=tf.float32)
                ),
                num_parallel_calls=tf.data.experimental.AUTOTUNE,
            )
        )

    return datasets_dict
