import tensorflow as tf
from math import ceil, inf

from utils.json_processor import dict2namespace
from data_pipeline.factory.toolset import get_dataset_size


def equalize_smaller_dataset(dataset_x, dataset_y):
    """
    Take two datasets of possible unequal lenghts and repeat
    the data from the small one until it at least reaches the
    size of the bigger one

    Args:
        dataset_x (tf.data.Dataset): contains samples from X distribution
        dataset_y (tf.data.Dataset): contains samples from Y distribution
    Return:
        dataset_x (tf.data.Dataset): contains samples from X distribution
        dataset_y (tf.data.Dataset): contains samples from Y distribution
    """
    x_size = get_dataset_size(dataset_x)
    y_size = get_dataset_size(dataset_y)

    if x_size == y_size:
        return dataset_x, dataset_y
    elif x_size < y_size:
        repetiton_factor = ceil(y_size / x_size)
        return dataset_x.repeat(repetiton_factor), dataset_y
    repetiton_factor = ceil(x_size / y_size)
    return dataset_x, dataset_y.repeat(repetiton_factor)


def arrange_single_dataset_for_training(dataset, config, num_channels=3):
    """
    Prepare the dataset for the training session

    Args:
        dataset (tf.data.Dataset): contains samples from a single distribution
        config (namespace): contains some configurations for the dataset
        num_channels (int): number of channels for the images
    Return:
        the final training dataset (tf.data.Dataset)
    """
    if config.sample_patches:
        if config.sample_patches is True:
            # get the biggest patch that can be sampled from all images
            target_shape, _ = get_image_shape_bounds(dataset)
        else:
            # sample the requested shape, but running the risk of exception
            # throwing if one of the samples' shape is smaller
            target_shape = config.sample_patches
        dataset = sample_image_patches(dataset, target_shape)

    if config.preshuffle_individual_datasets:
        dataset = dataset.shuffle(config.shuffling_buffer)

    if hasattr(config, 'augmentation'):
        augmentation_config = dict2namespace(config.augmentation)
        dataset = augment_dataset(dataset, augmentation_config)

    training_dataset = dataset.padded_batch(
        batch_size=config.batch_size, padded_shapes=[None, None, num_channels]
    )

    return training_dataset


def arrange_single_dataset_for_validation(dataset, config, num_channels=3):
    """
    Prepare the dataset for validation

    Args:
        dataset (tf.data.Dataset): contains samples from a single distribution
        config (namespace): contains some configurations for the dataset
        num_channels (int): number of channels for the images
    Return:
        the final validation dataset (tf.data.Dataset)
    """
    if config.sample_patches:
        if config.sample_patches is True:
            # get the biggest patch size that can be sampled from all images
            target_shape, _ = get_image_shape_bounds(dataset)
        else:
            # sample the requested shape, but run the risk of exception
            # throwing if one of the samples' shape is smaller
            target_shape = config.sample_patches
        dataset = sample_image_patches(dataset, target_shape)

    validation_dataset = dataset.padded_batch(
        batch_size=config.batch_size, padded_shapes=[None, None, num_channels]
    )

    return validation_dataset


def arrange_double_dataset_for_training(dataset_x, dataset_y, config, num_channels=3):
    """
    Merge the dataset objects into one which is ready to be fed to
    the training operation

    Args:
        dataset_x (tf.data.Dataset): contains samples from X distribution
        dataset_y (tf.data.Dataset): contains samples from Y distribution
        config (namespace): contains some configurations for the dataset
        num_channels (int): number of channels for the images
    Return:
        the final training dataset (tf.data.Dataset)
    """
    if config.sample_patches:
        if config.sample_patches is True:
            # get the biggest patch that can be sampled from all images
            target_shape_X, _ = get_image_shape_bounds(dataset_x)
            target_shape_Y, _ = get_image_shape_bounds(dataset_y)
        else:
            # sample the requested shape, but running the risk of exception
            # throwing if one of the samples' shape is smaller
            target_shape_X = config.sample_patches
            target_shape_Y = config.sample_patches
        dataset_x = sample_image_patches(dataset_x, target_shape_X)
        dataset_y = sample_image_patches(dataset_y, target_shape_Y)

    if config.preshuffle_individual_datasets:
        dataset_x = dataset_x.shuffle(config.shuffling_buffer)
        dataset_y = dataset_y.shuffle(config.shuffling_buffer)
    if config.equalize_datasets_sizes:
        dataset_x, dataset_y = equalize_smaller_dataset(dataset_x, dataset_y)

    if hasattr(config, 'augmentation'):
        augmentation_config = dict2namespace(config.augmentation)
        dataset_x = augment_dataset(dataset_x, augmentation_config)
        dataset_y = augment_dataset(dataset_y, augmentation_config)

    training_dataset = tf.data.Dataset.zip((dataset_x, dataset_y))
    if config.shuffle_zipped_dataset:
        training_dataset = training_dataset.shuffle(config.shuffling_buffer)

    training_dataset = training_dataset.padded_batch(
        batch_size=config.batch_size,
        padded_shapes=([None, None, num_channels], [None, None, num_channels]),
    )

    return training_dataset


def arrange_double_dataset_for_validation(dataset_x, dataset_y, config, num_channels=3):
    """
    Merge the dataset objects into one which is ready to be used as
    a validation set

    Args:
        dataset_x (tf.data.Dataset): contains samples from X distribution
        dataset_y (tf.data.Dataset): contains samples from Y distribution
        config (namespace): contains some configurations for the dataset
        num_channels (int): number of channels for the images
    Return:
        the final validation dataset (tf.data.Dataset)
    """
    if config.sample_patches:
        if config.sample_patches is True:
            # get the biggest patch size that can be sampled from all images
            target_shape_X, _ = get_image_shape_bounds(dataset_x)
            target_shape_Y, _ = get_image_shape_bounds(dataset_y)
        else:
            # sample the requested shape, but run the risk of exception
            # throwing if one of the samples' shape is smaller
            target_shape_X = config.sample_patches
            target_shape_Y = config.sample_patches
        dataset_x = sample_image_patches(dataset_x, target_shape_X)
        dataset_y = sample_image_patches(dataset_y, target_shape_Y)
    if config.equalize_datasets_sizes:
        dataset_x, dataset_y = equalize_smaller_dataset(dataset_x, dataset_y)
    validation_dataset = tf.data.Dataset.zip((dataset_x, dataset_y))

    validation_dataset = validation_dataset.padded_batch(
        batch_size=config.batch_size,
        padded_shapes=([None, None, num_channels], [None, None, num_channels]),
    )

    return validation_dataset


def get_image_shape_bounds(dataset):
    """
    Compute the shapes of the biggest and smallest images
    in the dataset by traversing it (each value represents the minimum
    for that particular dimension)
    This operation might be time consuming for large datasets!

    Args:
        dataset (tf.data.Dataset): contains the image samples
    Return:
        min_shape, max_shape (a tuple of lists)
    """
    max_shape = [0, 0]
    min_shape = [inf, inf]

    for sample in dataset:
        for i in range(2):
            if sample.shape[i] < min_shape[i]:
                min_shape[i] = sample.shape[i]
            if sample.shape[i] > max_shape[i]:
                max_shape[i] = sample.shape[i]

    return min_shape, max_shape


def sample_image_patches(dataset, target_shape):
    """
    Sample image patches of the target shape from the given dataset.

    Args:
        dataset (tf.data.Dataset): contains the image samples
        target_shape (list): contains the desired shape for the images
    Return:
        the processed dataset (tf.data.Dataset)
    """
    return dataset.map(
        map_func=lambda x: tf.image.random_crop(x, target_shape + [3]),
        num_parallel_calls=tf.data.experimental.AUTOTUNE,
    )


def augment_dataset(dataset, config):
    """
    Map image augmentation functions to the dataset

    Args:
        dataset (tf.data.Dataset): contains the image samples
        config (namespace): configurations for the augmentation techniques
    Return:
        the augmented dataset (tf.data.Dataset)
    """
    aug_functions = []

    # flips
    if hasattr(config, 'random_flip_left_right') and config.random_flip_left_right:
        aug_functions.append(tf.image.random_flip_left_right)
    if hasattr(config, 'random_flip_up_down') and config.random_flip_up_down:
        aug_functions.append(tf.image.random_flip_up_down)

    # rotations
    if hasattr(config, 'random_rotations') and config.random_rotations:
        aug_functions.append(
            lambda image: tf.image.rot90(
                image=image,
                k=tf.random.uniform(shape=(), minval=0, maxval=4, dtype=tf.int32),
            )
        )

    # jpeg compression quality
    if hasattr(config, 'random_jpeg_quality') and config.random_jpeg_quality:
        aug_functions.append(
            lambda image: tf.where(
                tf.random.uniform(shape=(), minval=0, maxval=1) > 0.5,
                tf.image.random_jpeg_quality(
                    image=image,
                    min_jpeg_quality=config.random_jpeg_quality,
                    max_jpeg_quality=100,
                ),
                image,
            )
        )

    for aug_function in aug_functions:
        dataset = dataset.map(
            map_func=aug_function, num_parallel_calls=tf.data.experimental.AUTOTUNE
        )
    return dataset
