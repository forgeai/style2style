import tensorflow as tf


def optimize_loading(dataset):
    """
    Optimize the input pipeline by prefatching data in the CPU in
    order to avoid stalls when the GPU waits to consume batches

    Args:
        dataset (tf.data.Dataset): the original dataset
    Return:
        dataset (tf.data.Dataset): the optimized dataset
    """
    return dataset.prefetch(buffer_size=tf.data.experimental.AUTOTUNE)
