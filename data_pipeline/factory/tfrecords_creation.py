import os
import sys
import glob
import logging

sys.path.append('.')
from data_pipeline.tfrecords.serializer import serialize_tfrecords


def traverse_data_directories(data_path, samples_per_record):
    """
    Traverse all directories starting from the top level specified in
    data_path and process each leaf folder that contains a set of images

    Args:
        data_path (str): path to a high-level directory from which the
                            process will start to execute by following all
                            folder branches
        samples_per_record (int): number of samples to encapsulate in one record
    """
    for content_tree in os.walk(data_path):
        # check if the folder contains an allocation file
        if glob.glob(os.path.join(content_tree[0], '#data_allocation.json')):
            images_path = content_tree[0]
            tfrecords_path = images_path.replace('processed', 'tfrecords')
            perform_tfrecords_serialization(
                images_path, tfrecords_path, samples_per_record
            )


def perform_tfrecords_serialization(images_path, tfrecords_path, samples_per_record=64):
    """
    Carry out the serialization for a specific dataset of images
    Previously serialized records will be overwritten

    Args:
        images_path (str): path to the images directory
        tfrecords_path (str): path to the tfrecords directory
        samples_per_record (int): number of samples to encapsulate in one record
    """
    logger = logging.getLogger(__name__)

    if os.path.isdir(tfrecords_path):
        logger.info('Found cached tfrecords at {}'.format(tfrecords_path))
        return

    logger.info('Creating tfrecords -> %s' % tfrecords_path)
    os.makedirs(tfrecords_path)
    serialize_tfrecords(images_path, tfrecords_path, samples_per_record)
