import os
import cv2
import glob
import string
import random
import logging
from tqdm import tqdm

from utils.filesystem import get_basename


def generate_image_id(id_length=8):
    """
    Generate an image id using random letters and digits

    Args:
        id_length (int): length of the id string
    Return:
        image_id (str)
    """
    possible_characters = string.ascii_lowercase + string.digits
    return ''.join([random.choice(possible_characters) for _ in range(id_length)])


def is_big_enough(image, min_dim):
    """
    Check if the shape of an image is big enough to be considered for training

    Args:
        image (np.array): the image array
        min_dim (int): minimum size for height and width
    Return:
        bool (True if the image passed the test)
    """
    return all([image.shape[d] >= min_dim for d in range(2)])


def resize_to_maximum_bounds(image, max_dim):
    """
    Resize an image to have its dimensions no greater than a limit
    If the image is smaller already return it unaltered

    Args:
        image (np.array): the image array
        max_dim (int): maximum size for height and width
    Return:
        image (np.array) with a possibly smaller shape
    """
    shrinkage = min([max_dim / image.shape[d] for d in range(2)])
    if shrinkage >= 1:
        return image
    return cv2.resize(
        image, None, fx=shrinkage, fy=shrinkage, interpolation=cv2.INTER_AREA
    )


def resize_to_fixed_dimensions(
    image, fixed_dim, mode='pad', interpolation=cv2.INTER_LINEAR
):
    """
    Resize an image to have its dimensions equal to the specified value
    If in pad mode: resize until the bigger side fits in the frame
                    then pad the rest with null values
    If in crop mode: resize until the smaller side fits in the frame
                     then crop the center of the image

    Args:
        image (np.array): the image array
        fixed_dim (tuple): the height and width of the processed image
        mode (str): the resizing mode ("pad" or "crop")
        interpolation (int): the cv2 code for interpolation on resize
    Return:
        image (np.array) having the demanded shape
    """
    assert mode in ('pad', 'crop'), 'Invalid mode for image resizing'
    height_ratio = image.shape[0] / fixed_dim[0]
    width_ratio = image.shape[1] / fixed_dim[1]

    if mode == 'pad':
        max_ratio = max(height_ratio, width_ratio)
        new_height = round(image.shape[0] / max_ratio)
        new_width = round(image.shape[1] / max_ratio)

        content_image = cv2.resize(
            image, (new_width, new_height), interpolation=interpolation
        )
        top_pad = (fixed_dim[0] - new_height) // 2
        left_pad = (fixed_dim[1] - new_width) // 2
        bottom_pad = fixed_dim[0] - new_height - top_pad
        right_pad = fixed_dim[1] - new_width - left_pad

        return cv2.copyMakeBorder(
            content_image,
            top_pad,
            bottom_pad,
            left_pad,
            right_pad,
            cv2.BORDER_CONSTANT,
            value=[0, 0, 0],
        )
    else:
        min_ratio = min(height_ratio, width_ratio)
        new_height = round(image.shape[0] / min_ratio)
        new_width = round(image.shape[1] / min_ratio)

        content_image = cv2.resize(
            image, (new_width, new_height), interpolation=interpolation
        )
        top_crop = (new_height - fixed_dim[0]) // 2
        left_crop = (new_width - fixed_dim[1]) // 2
        bottom_crop = top_crop + fixed_dim[0]
        right_crop = left_crop + fixed_dim[1]

        return content_image[top_crop:bottom_crop, left_crop:right_crop]


def refine_data_by_range(data_path, min_dim, max_dim):
    """
    Copy and process raw image data
    Resize all images to have their largest side equal to max_dim
    Discard all images that after applying the previous step have
    their smallest side less than min_dim

    Args:
        data_path (str): path to the raw data directory
        min_dim (int): minimum dimension that an image can have either
                        for height or for width
        max_dim (int): maximum dimension that an image can have either
                        for height or for width
    Return:
        refined_data_path (str): path to the refined data directory
    """
    logger = logging.getLogger(__name__)
    data_name = get_basename(data_path)
    refined_data_path = os.path.join(
        'data', 'processed', '{}_{{range_{}-{}}}'.format(data_name, min_dim, max_dim)
    )

    # check for cached data
    if os.path.isdir(refined_data_path):
        logger.info('Found cached refined images at {}'.format(refined_data_path))
        return refined_data_path

    # create the data directory
    os.makedirs(refined_data_path)

    # traverse and process each image
    for content_tree in os.walk(data_path):
        image_paths = glob.glob(os.path.join(content_tree[0], '*'))

        for image_path in tqdm(
            image_paths, desc='{:^21}'.format('Refining images -> %s' % refined_data_path)
        ):
            try:
                raw_image = cv2.imread(image_path, cv2.IMREAD_COLOR)
                refined_image = resize_to_maximum_bounds(raw_image, max_dim)
                if not is_big_enough(refined_image, min_dim):
                    continue
                refined_image_name = generate_image_id() + '.jpg'

                cv2.imwrite(
                    os.path.join(refined_data_path, refined_image_name), refined_image
                )
            except Exception:
                pass

    return refined_data_path


def refine_data_by_fixed_size(data_path, fixed_dim, mode='pad'):
    """
    Copy and process raw image data
    Resize all images to have the fixed specified dimensions
    Proceed by scaling the images until they fit the window,
    then pad the rest with null values

    Args:
        data_path (str): path to the raw data directory
        fixed_dim (tuple): the height and width of the processed images
        mode (str): the resizing mode ("pad" or "crop")
    Return:
        refined_data_path (str): path to the refined data directory
    """
    logger = logging.getLogger(__name__)
    data_name = get_basename(data_path)
    refined_data_path = os.path.join(
        'data', 'processed', '{}_{{fixed_{}_{}-{}}}'.format(data_name, mode, *fixed_dim)
    )

    # check for cached data
    if os.path.isdir(refined_data_path):
        logger.info('Found cached refined images at {}'.format(refined_data_path))
        return refined_data_path

    # create the data directory
    os.makedirs(refined_data_path)

    # traverse and process each image
    for content_tree in os.walk(data_path):
        image_paths = glob.glob(os.path.join(content_tree[0], '*'))

        for image_path in tqdm(
            image_paths, desc='{:^21}'.format('Refining images -> %s' % refined_data_path)
        ):
            try:
                raw_image = cv2.imread(image_path, cv2.IMREAD_COLOR)
                refined_image = resize_to_fixed_dimensions(raw_image, fixed_dim, mode)
                refined_image_name = generate_image_id() + '.jpg'
                cv2.imwrite(
                    os.path.join(refined_data_path, refined_image_name), refined_image
                )
            except Exception:
                pass

    return refined_data_path
