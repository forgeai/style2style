import noise
import random
import numpy as np
import tensorflow as tf
from math import ceil
from itertools import product
from functools import partial
from multiprocessing import Pool

from utils.json_processor import dict2namespace


def get_image_shape(data_config):
    """
    Get the most appropriate image shape for the noise generator
    given the constraints in the data configurations

    Args:
        data_config (namespace): contains data configurations
    Return:
        image_shape (tuple): the shape for the images (rank 2)
    """
    if data_config.fixed_size:
        return tuple(data_config.fixed_size)
    return tuple(2 * [data_config.max_size])


def generate_noise_channel(image_shape, noise_config, noise_function):
    """
    Generate a 2-dimensional structured noise image

    Args:
        image_shape (tuple): the shape for the image (rank 2)
        noise_config (namespace): configurations for the noise
        noise_function (function object): the function for generating noise
    Return:
        noise_channel (np.array): noise image of rank 2
    """
    noise_channel = np.zeros(image_shape, dtype=np.float32)
    repeatx = random.randint(0, noise_config.repeat_range)
    repeaty = random.randint(0, noise_config.repeat_range)
    base = random.randint(0, noise_config.base_range)

    for x, y in product(range(image_shape[0]), range(image_shape[1])):
        noise_channel[x, y] = noise_function(
            x=x / noise_config.scale,
            y=y / noise_config.scale,
            octaves=noise_config.octaves,
            lacunarity=noise_config.lacunarity,
            persistence=noise_config.persistence,
            repeatx=repeatx,
            repeaty=repeaty,
            base=base,
        )
    return noise_channel


def generate_noise_dataset(dataset_size, image_shape, noise_config):
    """
    Create a dataset of noise images by generating a noise space with
    independent channels and then taking all permutations of these to
    create random RGB noise images

    Args:
        dataset_size (int): the number of samples to generate
        image_shape (tuple): the shape of the images (rank 2)
        noise_config (namespace): configurations for the noise
    Return:
        dataset (tf.data.Dataset)
    """
    assert noise_config.type in ('perlin', 'simplex'), 'Invalid noise type'
    noise_function = noise.pnoise2 if noise_config.type == 'perlin' else noise.snoise2
    channel_perms = list(product(range(noise_config.sampling_channels), repeat=3))
    noise_space_shape = (
        (len(channel_perms),) + image_shape + (noise_config.sampling_channels,)
    )
    num_noise_spaces = ceil(dataset_size / len(channel_perms))
    process_pool = Pool(noise_config.num_parallel_processes)

    def noise_sample_generator():
        nonlocal noise_config, noise_function, noise_space_shape, process_pool

        for _ in range(num_noise_spaces):
            noise_channels = process_pool.imap_unordered(
                partial(generate_noise_channel, noise_space_shape[1:3], noise_config),
                (noise_function for _ in range(noise_space_shape[-1])),
            )
            noise_space = np.expand_dims(np.stack(list(noise_channels), axis=-1), axis=0)
            noise_space_repeated = np.repeat(noise_space, noise_space_shape[0], axis=0)
            yield noise_space_repeated

    channel_permutation_function = lambda noise_space, perm: tf.concat(
        values=[
            tf.slice(noise_space, [0, 0, perm[0]], [*image_shape, 1]),
            tf.slice(noise_space, [0, 0, perm[1]], [*image_shape, 1]),
            tf.slice(noise_space, [0, 0, perm[2]], [*image_shape, 1]),
        ],
        axis=-1,
    )

    noise_space_dataset = tf.data.Dataset.from_generator(
        generator=noise_sample_generator,
        output_types=tf.float32,
        output_shapes=tf.TensorShape(noise_space_shape),
    ).unbatch()
    permutations_dataset = tf.data.Dataset.from_tensor_slices(
        tensors=channel_perms
    ).repeat(num_noise_spaces)

    zipped_dataset = tf.data.Dataset.zip((noise_space_dataset, permutations_dataset))
    trimmed_dataset = zipped_dataset.take(dataset_size)
    channel_arranged_dataset = trimmed_dataset.map(
        map_func=channel_permutation_function,
        num_parallel_calls=tf.data.experimental.AUTOTUNE,
    )

    return channel_arranged_dataset


def generate_noise_masks_dataset(dataset_size, image_shape, noise_config):
    """
    Create a dataset of patch masks by thresholding generated noise channels

    Args:
        dataset_size (int): the number of samples to generate
        image_shape (tuple): the shape of the images (rank 2)
        noise_config (namespace): configurations for the noise
    Return:
        dataset (tf.data.Dataset)
    """
    assert noise_config.type in ('perlin', 'simplex'), 'Invalid noise type'
    noise_function = noise.pnoise2 if noise_config.type == 'perlin' else noise.snoise2
    process_pool = Pool(noise_config.num_parallel_processes)

    def noise_sample_generator():
        nonlocal noise_config, noise_function, process_pool, dataset_size

        noise_images = process_pool.imap_unordered(
            partial(generate_noise_channel, image_shape, noise_config),
            (noise_function for _ in range(dataset_size)),
        )
        for noise_image in noise_images:
            yield tf.greater(noise_image, 0)

    dataset = tf.data.Dataset.from_generator(
        noise_sample_generator, output_types=tf.bool, output_shapes=image_shape
    )
    return dataset


def extract_fresh_noise_dataset(data_config, training_size, validation_size):
    """
    Create a new dataset containing generated noise

    Args:
        data_config (namespace): contains data configurations and paths
        training_size (int): size for the training data
        validation_size (int): size for the validation data
    Return:
        datasets_dict (dict: str -> tf.data.Dataset)
    """
    image_shape = get_image_shape(data_config)
    noise_config = dict2namespace(data_config.noise)
    return dict(
        training=generate_noise_dataset(training_size, image_shape, noise_config),
        validation=generate_noise_dataset(validation_size, image_shape, noise_config),
    )


def extract_fresh_noise_masks_dataset(data_config, dataset_size):
    """
    Create a new dataset containing binary masks from generated noise

    Args:
        data_config (namespace): contains data configurations and paths
        dataset_size (int): number of samples to generate in one round
    Return:
        dataset (tf.data.Dataset)
    """
    image_shape = get_image_shape(data_config)
    noise_config = dict2namespace(data_config.noise)
    return generate_noise_masks_dataset(dataset_size, image_shape, noise_config)
