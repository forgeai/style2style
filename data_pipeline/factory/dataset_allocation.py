import os
import sys
import glob
import random
import logging
import numpy as np

sys.path.append('.')
from utils.constants import IMAGE_EXTENSIONS
from utils.json_processor import dict2json, json2dict


def traverse_data_directories(data_path, splits):
    """
    Traverse all directories starting from the top level specified in
    data_path and process each leaf folder that contains a set of images

    Args:
        data_path (str): path to a high-level directory from which the process
                         will start to execute by following all folder branches
        splits (list): percentages of data allocation (train, validate, test)
    """
    for content_tree in os.walk(data_path):
        # check if the folder contains at least one image
        for image_extension in IMAGE_EXTENSIONS:
            if glob.glob(os.path.join(content_tree[0], '*' + image_extension)):
                perform_samples_allocation(content_tree[0], splits)
                break


def perform_samples_allocation(data_path, splits):
    """
    Read all image names from the the data_path, shuffle them, and split
    them into training, validation and testing; create a json that describes
    the samples allocation and save it in data_path
    If an allocation json already exists and it has the same splits, do nothing

    Args:
        data_path (str): path to the original images directory
        splits (list): percentages of data allocation (train, validate, test)
    """
    logger = logging.getLogger(__name__)
    file_path = os.path.join(data_path, '#data_allocation.json')

    try:
        # check if an allocation with the same splits already exists
        allocation_dict = json2dict(file_path)
        if all(np.isclose(allocation_dict['splits'], splits)):
            logger.info('Found cached allocation at {}'.format(file_path))
        else:
            raise Exception

    except Exception:
        logger.info('Creating new allocation file -> {}'.format(file_path))
        image_names = [x for x in os.listdir(data_path) if x.endswith(IMAGE_EXTENSIONS)]
        random.shuffle(image_names)

        training_split, validation_split, _ = splits
        training_size = round(training_split * len(image_names))
        validation_size = round(validation_split * len(image_names))
        trainval_size = training_size + validation_size

        allocation = dict(
            splits=splits,
            training=image_names[:training_size],
            validation=image_names[training_size:trainval_size],
            testing=image_names[trainval_size:],
        )
        dict2json(allocation, file_path)
