import sys

sys.path.append('.')
from data_pipeline.factory.dataset_allocation import perform_samples_allocation
from data_pipeline.factory.tfrecords_creation import perform_tfrecords_serialization
from data_pipeline.factory.image_processing import (
    refine_data_by_range,
    refine_data_by_fixed_size,
)


def perform_entire_pipeline(
    data_path, splits, min_dim, max_dim, fixed_dim, fixed_mode='pad'
):
    """
    Carry out the whole process of obtaining tfrecords from raw images

    Args:
        data_path (str): path to the raw data directory
        splits (list): percentages of data allocation (train, validate, test)
        min_dim (int): minimum dimension that an image can have either
                       for height or for width
        max_dim (int): maximum dimension that an image can have either
                       for height or for width
        fixed_dim (int or list): the exact size that each image must have
                                 if int it broadcasts to both width and height
                                 if None/False the workflow resumes to computing
                                 min and max dims; otherwise min_dim and max_dim
                                 have no effect at all
        fixed_mode (str): the resizing mode for fixed dimensions ("pad" or "crop")
    Return:
        tfrecords_path (str): path to the tfrecords directory
    """
    if fixed_dim:
        if isinstance(fixed_dim, int):
            fixed_dim = (fixed_dim, fixed_dim)
        else:
            assert len(fixed_dim) == 2, 'Image size must have a maximum rank of 2'
            fixed_dim = tuple(fixed_dim)
        refined_data_path = refine_data_by_fixed_size(data_path, fixed_dim, fixed_mode)
    else:
        refined_data_path = refine_data_by_range(data_path, min_dim, max_dim)

    perform_samples_allocation(refined_data_path, splits)
    tfrecords_path = refined_data_path.replace('processed', 'tfrecords')
    perform_tfrecords_serialization(refined_data_path, tfrecords_path)

    return tfrecords_path
