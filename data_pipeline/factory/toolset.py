def get_dataset_size(dataset):
    """
    Get the size of the dataset (number of samples)

    Args:
        dataset (tf.data.Dataset)
    Return:
        size (int)
    """
    return sum(1 for _ in dataset)
