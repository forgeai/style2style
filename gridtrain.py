import sys
import json
import hashlib
import argparse
from copy import copy
from tqdm import tqdm
from os import environ
from os.path import join, isdir
from itertools import product

environ['TF_CPP_MIN_LOG_LEVEL'] = '3' if __name__ == '__main__' else '0'

import logging
import logging.config

logging.config.fileConfig('logging.conf')

sys.path.append('.')
from train import run_training
from utils.json_processor import json2namespace


def parse_arguments():
    parser = argparse.ArgumentParser(
        description='Process configurations for the grid-training'
    )
    parser.add_argument('-m', '--model', help='model configuration file')
    parser.add_argument('-t', '--training', help='training configuration file')
    parser.add_argument('-d', '--data', help='data configuration file')
    parser.add_argument('-p', '--hparams', help='hparams configuration file')

    args = parser.parse_args()
    return args


def overwrite_hparam(config, config_prefix, hparam_path, hparam_value, preconditions):
    """
    Overwrite the hparam value in the config files for a grid training iteration
    only if it finds them defined at the global level or in a specified submodule
    At the same time ensure the preconditions are set (if specified)

    Args:
        config (Namespace): configurations file
        config_prefix (str): identifies the hparams that are allowed to be updated
        hparam_path (str): full name of the hyper-parameter (includes config type)
        hparam_value (object): value of the hyper-parameter (can have multiple types)
        preconditions (dict): maps hparam names to value dictionaries which express
                              certain preconditions that must be met in order to run
                              the session with those values (for example setting the
                              filters and kernels which vary a lot between component
                              types)
    Return:
        updated_config (Namespace): a new config file with updated hparam values
    """

    def update_config_with_hparam_value(config, hparam_name, hparam_value):
        updated_config = copy(config)

        if '.' in hparam_name:
            submodule_name, hparam_name = hparam_name.split('.')
            submodule = getattr(updated_config, submodule_name)
            submodule[hparam_name] = hparam_value
            setattr(updated_config, submodule_name, submodule)

        else:
            setattr(updated_config, hparam_name, hparam_value)

        return updated_config

    hparam_prefix, hparam_name = hparam_path.split('/')

    # the hparam to update does not belong to this config
    if hparam_prefix != config_prefix:
        return config

    # update the actual hparam value
    updated_config = update_config_with_hparam_value(config, hparam_name, hparam_value)

    # set preconditions for the hparam assignment
    if hparam_path in preconditions:
        hparam_preconditions = preconditions[hparam_path]

        if hparam_value in hparam_preconditions:
            for key, value in hparam_preconditions[hparam_value].items():

                key_prefix, key_name = key.split('/')
                assert (
                    key_prefix == config_prefix
                ), 'Cannot apply preconditions to fields from a different config file'

                updated_config = update_config_with_hparam_value(
                    updated_config, key_name, value
                )

    return updated_config


def main(args):

    logger = logging.getLogger(__name__)
    model_config, training_config, data_config, hparams_config = map(
        json2namespace, [args.model, args.training, args.data, args.hparams]
    )

    # make sure the model config is fresh
    if not model_config.new:
        logger.error('Cannot use a trained model in grid-training mode')
        return

    # make sure the hparams summarization is turned on
    training_config.summary['hparams'] = True

    # parse the config to read the hparams values and their corresponding preconditions
    hparams_dict = hparams_config.hparams
    preconditions = hparams_config.preconditions

    # create training grid by taking all combinations of hyperparameters
    hparams_grid = product(*hparams_dict.values())
    num_runs = sum(1 for _ in copy(hparams_grid))

    # run the training grid
    for hparams_instance in tqdm(
        hparams_grid, desc='{:^21}'.format('Grid Progress'), total=num_runs
    ):

        # set the hparams in their corresponding configs
        model_config_copy = copy(model_config)
        training_config_copy = copy(training_config)

        for hparam_path, hparam_value in zip(hparams_dict.keys(), hparams_instance):
            model_config_copy = overwrite_hparam(
                model_config_copy, 'model', hparam_path, hparam_value, preconditions
            )
            training_config_copy = overwrite_hparam(
                training_config_copy, 'training', hparam_path, hparam_value, preconditions
            )

        # compute the hash value of the session configuration
        md5 = hashlib.md5()
        md5.update(json.dumps(model_config_copy).encode('utf-8'))
        md5.update(json.dumps(training_config_copy).encode('utf-8'))
        session_hash = md5.hexdigest()

        # set the name of the model which reflects the hparams configuration
        model_config_copy.name += f'_{session_hash}'

        # skip hparams grid instance if it was already performed
        if isdir(join('projects', model_config_copy.project, model_config_copy.name)):
            continue

        # run the training session
        run_training(model_config_copy, training_config_copy, data_config)

    logger.info('Finished grid-training successfully')


if __name__ == '__main__':
    args = parse_arguments()
    main(args)
