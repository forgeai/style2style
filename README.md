# style2style

General style transfer framework that contains a set of popular GANs for image style transfer using unpaired data samples. The purpose of this project is to achieve the following:
1.  Develop a polymorphic design for creating multiple types of GANs for style transfer in a flexible and unconstrained way
2.  Create a general system that is able to map any GAN architecture to any data containing image samples from:
    *  2 different distributions for content transfer
    *  1 distribution + a set of style images for style transfer
3.  Enhance the efficiency of generator inference to achieve (at least close to) real time performance

## GAN models:

<p align="center">
  <img src="readme_stuff/diagrams/gans_diagram.png"><br>
</p>

* [ ] Cycle GAN

### Guidelines

*  generator: **UnetIn** (the sufix *In* stands for instance normalization in all components) is almost always the most sensible choice; however, if combined with a high *lambda_cycle* it might ignore the adversarial loss completely since passing the input intact can easily be done by just using the highest level convolution as an identity function and ignoring the rest of the convolution levels
*  generator: **ResidualEncoderDecoderIn** works well too, but it's not as good as the Unet at inferring the fine details of the images
*  generator: the **transposed_convolution** upsampling works best in terms of both results quality and running time; the *nearest* and *bilinear* methods take a bit more time to run, create unwanted artifacts and might slow down or inhibit learning altogether
*  generator: low values for the **kernel** sizes are good enough (like 3x3), even though in the literature many models use large kernels like 9x9; in practice using large kernels can sometimes make the first layer stop learning and allow the discriminator to take control of the battle in early game
*  discriminator: for simpler problems like pattern transfer, a **Convolutional** component can do the job and it can also use larger kernel sizes like 7x7 or 9x9 without affecting the training stability; for more complex datasets where not only patterns but higher level content structures have to be switched, a **Unet** component is the appropriate choice
*  to be noticed that for generator models it is recommended to use components that include instance normalization, while the discriminator does not have this requirement
*  loss weighting: there are 3 parameters that for conventional reasons have to sum up to 3.0: **lambda_adversarial**, **lambda_cycle**, **lambda_identity**; a safe choice is to set {*lambda_adversarial*: 0.25, *lambda_cycle*: 2.50, *lambda_identity*: 0.25} as it generally ensures training stability; for more aggressive transfer **lambda_adversarial** can be increased up to ~0.75
*  the learning rate for the discriminator should typically be one order of magnitude smaller than the generator's for stability reasons ([as recommended here](https://arxiv.org/abs/1706.08500))

### Samples

<img src="readme_stuff/samples/cycle_gan/fruit/strawberry2pineapple_1.png" width="384"><img src="readme_stuff/samples/cycle_gan/fruit/strawberry2pineapple_2.png" width="384"><br>
<img src="readme_stuff/samples/cycle_gan/fruit/pineapple2strawberry_1.png" width="384"><img src="readme_stuff/samples/cycle_gan/fruit/pineapple2strawberry_2.png" width="384"><br>
<img src="readme_stuff/samples/cycle_gan/sky/sky2aurora_1.png" width="384"><img src="readme_stuff/samples/cycle_gan/sky/sky2aurora_2.png" width="384"><br>
<img src="readme_stuff/samples/cycle_gan/temples/pyramid2temple_1.png" width="384"><img src="readme_stuff/samples/cycle_gan/temples/pyramid2temple_2.png" width="384"><br>
<img src="readme_stuff/samples/cycle_gan/temples/temple2pyramid_1.png" width="384"><img src="readme_stuff/samples/cycle_gan/temples/temple2pyramid_2.png" width="384"><br>

* [ ] Artistic CIN GM

### Guidelines

*  **UnetCin** works very well for the style network; as for the upsampling method the preferred one is **nearest** as it synthesizes nice images without having the need for a total pixel variation like the other upsampling methods
*  when controlling the trade-off between enforcing style and maintaining the content there are a few considerations to have in mind:
    * as a convention, **lambda_content** and **lambda_style** sum up to 1.0
    *  the content loss from lower level Vgg layers (like 13) has a lower order of magnitude because the activations have a smaller deviation from 0; that implies that **lambda_content** can take higher values (~0.1); their extracted features also occupy less memory on the disk
    *  the content loss from higher level Vgg layers (like 5 or 9) has a higher order of magnitude because the activations have a higher deviation from 0; that implies that **lambda_content** should take lower values (~0.01); their extracted features occupy more memory on the disk
    * scenarios vary a lot depending on the texture of the style images (high vs low contrast, patterns consistency, low vs high frequencies, etc.), but as a general recommendation for mild styling use {*content_feature_layers*: [9], *lambda_style*: 0.95, *lambda_content*: 0.05}, whereas for aggressive styling use {*content_feature_layers*: [13], *lambda_style*: 0.9, *lambda_content*: 0.1}

### Samples

<img src="readme_stuff/samples/artistic_cin_gm/artistic_bird.png" width="768"><br>
<img src="readme_stuff/samples/artistic_cin_gm/artistic_microscopy.png" width="768"><br>

* [ ] Paint GAN

<p align="center">
  <img src="readme_stuff/diagrams/paint_gan_diagram.png"><br>
</p>

## Configurations
This framework offers great flexibility in playing with the models, allowing the user to customize various aspects while keeping him relatively safe (haha... yeah sure) from erroneous use cases.
Training a new model is done by running a command of the form `python .\train.py -d .\configs\data_config.json -m .\configs\gan_config.json -t .\configs\training_config.json`

*  ### Data Config

| **Field** | **Type** | **Description** |
| ------ | ------ | ------ |
| *dataset_x_path* | string | path to the serialized tfrecords for the X dataset |
| *dataset_y_path* | string | path to the serialized tfrecords for the Y dataset |
| *fixed_size* | int or list of 2 ints (or NULL) | the exact height and width that each image should have |
| *min_size* | int | minimum size for any side of the images (if *fixed_size* is not NULL this has no effect) |
| *max_size* | int | maximum size for any side of the images (if *fixed_size* is not NULL this has no effect) |
| *splits* | list of 3 floats [0..1] | splitting portions for the data (training, validation, testing) |


*  ### Model Config
Contains options for customizing the gan:<br/>

| **Field** | **Type** | **Description** |
| ------ | ------ | ------ |
| *new* | boolean | should always be set to `true` for a fresh gan |
| *type* | string | the class of the gan |
| *name* | string | the name of the gan instance |
| *objective* | string | `mean_squared_error` and `wasserstein_distance` imply that the output is unbounded and the adversarial loss is computed using the MSE and the Wasserstein distance respectively, while `binary_crossentropy` implies applying a sigmoid activation to the output and evaluating the loss using binary crossentropy |
| *generator* | dictionary | configs for the generator |
| *discriminator* | dictionary | configs for the discriminator |

The *generator* and the *discriminator* represent configurations for `components`. These configs vary depending on the type of the component (templates for each specific component can be found in `components\templates`), but here are a few examples:<br/>

| **Field** | **Type** | **Description** |
| ------ | ------ | ------ |
| *model* | string | the class of the component (`convolutional`, `unet`, `residual_encoder_decoder`, ...) |
| *upsampling* | string | upsampling method if needed (`transposed_convolution`, `nearest`, or `bilinear`) |
| *dropout* | float [0..1] | dropout rate for the layers (the field name may very depending on the layer type) |
| *filters* | list of ints | number of filters in the order of layers |
| *kernels* | list of ints | number of kernel sizes in the order of layers |

*  ### Training Config
Contains the settings for the training session:

| **Field** | **Type** | **Description** |
| ------ | ------ | ------ |
| *epochs* | int | number of training iterations |
| *batch_size* | int | number of batches to use in one update |
| *summary* | int | all summary fields relate to tensorboard visualization stuff |
| *augmentation* | dictionary | augmentation operations to include in the data pipeline at the transform stage |
| *sample_patches* | boolean or tuple of 2 ints | whether to sample patches instead of using the whole images; if set to `true`, the patch shape is automatically inferred |
| *equalize_datasets_sizes* | boolean | whether to repeat the data from the smaller dataset in order to match the size of the larger one |
| *suffling_buffer* | int | buffer size to use when shuffling the dataset; for a perfect shuffle this value has to be >= size of the dataset |
| *generator* | dictionary | contains personalized configs for the generator, like the `learning_rate`, `optimizer` and `loss_weights` |
| *discriminator* | dictionary | contains personalized configs for the discriminator, like the `learning_rate`, `optimizer` and `loss_weights` |
