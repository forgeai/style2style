import time
import argparse
from shutil import rmtree
from os import listdir, environ
from os.path import join, isdir
from datetime import timedelta

environ['TF_CPP_MIN_LOG_LEVEL'] = '3' if __name__ == '__main__' else '0'
import tensorflow as tf

tf.config.optimizer.set_jit(True)

import logging
import logging.config

logging.config.fileConfig('logging.conf')

from utils.json_processor import json2namespace
from utils.exceptions import ModelAlreadyExistsException
from models.gans.paint_gan.model import PaintGan
from models.gans.artistic_base_gan.model import ArtisticBaseGan
from models.factory.type_loader import get_model_type
from models.factory.file_handling import copy_config, get_latest_version


def parse_arguments():
    parser = argparse.ArgumentParser(
        description='Process configurations and execute operations'
    )
    parser.add_argument(
        '-m',
        '--model',
        help='model configuration file',
        default=join('configs', 'model_cycle_gan.json'),
    )
    parser.add_argument(
        '-t',
        '--training',
        help='training configuration file',
        default=join('configs', 'training_cycle_gan.json'),
    )
    parser.add_argument(
        '-d',
        '--data',
        help='data configuration file',
        default=join('configs', 'data_cycle_gan.json'),
    )

    args = parser.parse_args()
    return args


def failure_cleanup(model_config, clear_last_version):
    """
    Remove uselessly created files in case the training failed before starting

    Args:
        model_config (namespace): configurations file for the model
        clear_last_version (bool): flags if the last created version should be
                                   removed due to a failure that occurred in an
                                   attempt to re-train an existing model
    """
    model_path = join('projects', model_config.project, model_config.name)

    if model_config.new:
        rmtree(model_path, ignore_errors=True)

    elif clear_last_version:
        version_path = join(model_path, str(get_latest_version(model_path)))
        rmtree(version_path, ignore_errors=True)


def run_training(model_config, training_config, data_config):
    """
    Run the training session described by the config files

    Args:
        model_config (namespace): configs for the model
        training_config (namespace): configs for the training session
        data_config (namespace): configs for the input data
    """
    flag_to_clean_up_last_version = False
    logger = logging.getLogger(__name__)

    try:
        # create the model
        logger.info('Building {} ({})...'.format(model_config.name, model_config.type))
        Gan = get_model_type(model_config.type)
        if issubclass(Gan, (ArtisticBaseGan, PaintGan)):
            gan = Gan(model_config, data_config)
        else:
            gan = Gan(model_config)

        # handle filesystem operations
        version = gan.initiate_new_version()
        flag_to_clean_up_last_version = True

        copy_config(data_config, 'data_config.json', gan.model_path, version)
        copy_config(training_config, 'training_config.json', gan.model_path, version)
        gan.create_epoch_checkpoint_manager()
        gan.open_summary_writers()

        # prepare the datasets
        training_set, validation_set = gan.prepare_datasets(data_config, training_config)

        # start the training session
        logger.info('Starting the training session...')
        training_start = time.time()

        gan.train(training_config, training_set, validation_set)
        flag_to_clean_up_last_version = False

        total_training_time = time.time() - training_start
        logging.info(
            '\nTotal training time: {}'.format(
                str(timedelta(seconds=total_training_time)).split('.')[0]
            )
        )

        # cleanup and closedown
        gan.close_summary_writers()
        if issubclass(Gan, ArtisticBaseGan):
            gan.remove_cached_data()

    except Exception as e:
        try:
            gan.close_summary_writers()
            if len(listdir(gan.checkpoints_path)) == 0:
                failure_cleanup(model_config, flag_to_clean_up_last_version)
        except UnboundLocalError:
            pass
        finally:
            raise e


def main(args):

    model_config, training_config, data_config = map(
        json2namespace, [args.model, args.training, args.data]
    )

    # check if the model exists already
    if model_config.new and isdir(
        join('projects', model_config.project, model_config.name)
    ):
        raise ModelAlreadyExistsException(model_config.name, model_config.project)

    run_training(model_config, training_config, data_config)


if __name__ == '__main__':
    args = parse_arguments()
    main(args)
