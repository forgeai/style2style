import sys
import cv2
import pygame
import argparse
import numpy as np
import tensorflow as tf
from os.path import join

tf.config.optimizer.set_jit(True)

import logging
import logging.config

logging.config.fileConfig('logging.conf')
logger = logging.getLogger(__name__)

sys.path.append('.')
from utils.apps import (
    Slider,
    load_gem_setup,
    interpolate_colours,
    load_image_to_pygame,
    create_gaussian_kernel,
    grayscale_to_pygame_frame,
    spray_frame_to_pygame_frame,
    canvas_frame_to_pygame_frame,
    painter_thumbnail_image_to_pygame_surface,
)
from utils.constants import PAINTING_SIGMA, PAINTING_ROW_SEATS


class PainterAppRunner:
    """
    Runs the painter app

    Args:
        model (keras.Model): the model used for painting predictions
        canvas_seed (np.array): the initial value for the canvas seed
        canvas_mask (np.array): the initial value for the canvas mask
        thumbnails (dict): maps thumbnail name to image
    """

    BUFFER_SIZE = 10

    def __init__(self, model, canvas_seed, canvas_mask, thumbnails):
        self.model = model
        self.initialize_screen((canvas_seed.shape[1], canvas_seed.shape[0]))
        self.initialize_central_panel(canvas_seed, canvas_mask)
        self.initialize_left_panel()
        self.initialize_right_panel(thumbnails)
        self.initialize_mouse_cursors()

        pygame.init()
        pygame.display.set_caption('pAInter')
        self.font = pygame.font.SysFont('arial', 16)

    def initialize_screen(self, display_shape):
        """
        Create and initialize the pygame screen

        Args:
            display_shape (tuple of 2 ints): the resolution of the display image
        """
        self.panel_width = display_shape[0] // 3
        self.left_panel_top_left = (0, 0)
        self.canvas_top_left = (self.panel_width, 0)
        self.left_panel_top_left = (self.panel_width + display_shape[0], 0)

        self.screen_shape = (display_shape[0] + 2 * self.panel_width, display_shape[1])
        self.screen = pygame.display.set_mode(self.screen_shape, flags=0, depth=32)
        self.background = load_image_to_pygame(
            join('utils', 'images', 'wood.png'), self.screen_shape
        )

    def initialize_central_panel(self, canvas_seed, canvas_mask):
        """
        Create and initialize the canvas that contains the actual painting

        Args:
            canvas_seed (np.array): the initial value for the canvas seed
            canvas_mask (np.array): the initial value for the canvas mask
        """
        self.drawing = False
        self.to_update_canvas = False
        self.undo_counter = 0
        self.canvas_height = canvas_seed.shape[0]
        self.canvas_width = canvas_seed.shape[1]
        self.num_labels = len(np.unique(canvas_mask))

        self.drawing_change = np.zeros_like(canvas_mask, dtype=np.bool)
        self.canvas_seed_initial = canvas_seed[tf.newaxis, ..., tf.newaxis]
        self.canvas_mask_initial = canvas_mask[tf.newaxis, ...]
        self.canvas_seed = np.repeat(
            self.canvas_seed_initial, repeats=self.BUFFER_SIZE, axis=0
        )
        self.canvas_mask = np.repeat(
            self.canvas_mask_initial, repeats=self.BUFFER_SIZE, axis=0
        )

        canvas_frame = self.predict_canvas(
            self.canvas_seed_initial, self.canvas_mask_initial
        )
        self.canvas_pyframe = canvas_frame_to_pygame_frame(canvas_frame)

    def initialize_left_panel(self):
        """
        Initialize the left panel tools (they manipulate the current brush)
        """
        self.margin = self.panel_width // 12
        self.brush_index = 0

        # set up items
        self.item_size = (self.panel_width - 3 * self.margin) // 2
        self.item_size += self.item_size % 2
        self.item_half_size = self.item_size // 2

        self.brush_thickness_top_left = (self.margin, self.margin)
        self.brush_top_left = (2 * self.margin + self.item_size, self.margin)

        # set up sliders
        self.slider_size = 28
        self.slider_bar_height = 6
        self.slider_bar_width = self.panel_width - 2 * self.margin
        self.knob_drawing_offset_x = self.slider_size // 2
        self.knob_drawing_offset_y = (self.slider_size - self.slider_bar_height) // 2

        self.sliders = dict(
            thickness=Slider(
                1.5,
                6.0,
                self.margin,
                self.margin + self.slider_bar_width,
                lambda_function=lambda x: x ** 2,
            ),
            mean=Slider(-0.1, 0.1, self.margin, self.margin + self.slider_bar_width),
            sigma=Slider(
                -1.0,
                1.0,
                self.margin,
                self.margin + self.slider_bar_width,
                lambda_function=lambda x: PAINTING_SIGMA * 10 ** x,
            ),
        )
        sliders_y_coordinates = np.linspace(
            2 * self.item_size + 4 * self.margin,
            self.screen_shape[1] - 2 * self.margin - self.slider_bar_height,
            len(self.sliders),
            dtype=np.int32,
        )
        self.sliders_top_left = dict(
            thickness=(self.margin, sliders_y_coordinates[0]),
            mean=(self.margin, sliders_y_coordinates[1]),
            sigma=(self.margin, sliders_y_coordinates[2]),
        )
        self.sliders_colour = dict(
            empty=(60, 60, 60),
            thickness=(240, 240, 240),
            mean=(251, 188, 83),
            sigma=(160, 160, 255),
        )
        self.sliders_icons = {
            key: load_image_to_pygame(
                join('utils', 'images', f'{key}_slider.png'), self.slider_size
            )
            for key in self.sliders.keys()
        }

        # initialize brushes
        self.slider_dragging = 'thickness'
        self.update_brushes()
        self.slider_dragging = None
        self.slider_in_cursor_area = None

    def initialize_right_panel(self, thumbnails):
        """
        Initialize the right panel tools containing brush selections

        Args:
            thumbnails (dict): maps thumbnail name to image
        """
        self.thumbnail_size = (self.panel_width - 4 * self.margin) // 3
        self.thumbnail_size += self.thumbnail_size % 2

        def index_to_top_left_coords(index):
            k = index % PAINTING_ROW_SEATS
            row_number = index // PAINTING_ROW_SEATS
            x = (
                self.panel_width
                + self.canvas_width
                + (k + 1) * self.margin
                + k * self.thumbnail_size
            )
            y = (row_number + 1) * self.margin + row_number * self.thumbnail_size
            return x, y

        self.thumbnails_name = {}
        self.thumbnails_image = {}
        self.thumbnails_top_left = {}
        self.brush_selection_map = -1 * np.ones(self.screen_shape, dtype=np.int32)

        for t_name, image in thumbnails.items():
            t_tokens = t_name.split('_')
            index = int(t_tokens[0])

            self.thumbnails_name[index] = '_'.join(t_tokens[1:])
            self.thumbnails_image[index] = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

            top_left = index_to_top_left_coords(index)
            self.brush_selection_map[
                top_left[0] : top_left[0] + self.thumbnail_size,
                top_left[1] : top_left[1] + self.thumbnail_size,
            ] = index
            self.thumbnails_top_left[index] = top_left

        self.brush_pyframe = painter_thumbnail_image_to_pygame_surface(
            self.thumbnails_image[self.brush_index], self.item_size
        )

    def initialize_mouse_cursors(self):
        """
        Initialize the icons for the different states of the mouse cursor
        """
        brush_cursor_path = join('utils', 'cursors', 'brush.xbm')
        self.brush_cursor = pygame.cursors.load_xbm(brush_cursor_path, brush_cursor_path)

        hand_open_cursor_path = join('utils', 'cursors', 'hand_open.xbm')
        self.hand_open_cursor = pygame.cursors.load_xbm(
            hand_open_cursor_path, hand_open_cursor_path
        )

        hand_closed_cursor_path = join('utils', 'cursors', 'hand_closed.xbm')
        self.hand_closed_cursor = pygame.cursors.load_xbm(
            hand_closed_cursor_path, hand_closed_cursor_path
        )

    def draw_central_panel(self):
        """
        Draw the elements of the central panel
        """
        if self.to_update_canvas:
            self.canvas_mask = np.roll(self.canvas_mask, shift=1, axis=0)
            self.canvas_seed = np.roll(self.canvas_seed, shift=1, axis=0)
            self.canvas_mask[0] = self.create_drawing_mask(
                self.canvas_mask[1], self.drawing_change, self.brush_index
            )
            self.canvas_seed[0] = self.create_drawing_seed(
                self.canvas_seed[1], self.drawing_change
            )

            canvas_frame = self.predict_canvas(self.canvas_seed[:1], self.canvas_mask[:1])
            self.canvas_pyframe = canvas_frame_to_pygame_frame(canvas_frame)

            self.to_update_canvas = False
            self.undo_counter = 0

        self.screen.blit(self.canvas_pyframe, self.canvas_top_left)

        # create a spraying effect on top of the canvas
        if self.drawing:
            drawing_spray = spray_frame_to_pygame_frame(self.drawing_change)
            self.screen.blit(drawing_spray, self.canvas_top_left)

    def draw_left_panel(self):
        """
        Draw the elements of the left panel
        """
        # brushes
        self.screen.blit(self.brush_pyframe, self.brush_top_left)
        self.screen.blit(self.brush_thickness_pyframe, self.brush_thickness_top_left)

        # sliders
        for slider_name, top_left_coords in self.sliders_top_left.items():
            slider_x = self.sliders[slider_name].get_position()
            slider_y = top_left_coords[1]
            slider_activation = self.sliders[slider_name].get_activation()
            slider_activated_width = int(slider_activation * self.slider_bar_width)
            slider_deactivated_width = self.slider_bar_width - slider_activated_width

            # empty part
            pygame.draw.rect(
                self.screen,
                self.sliders_colour['empty'],
                (slider_x, slider_y, slider_deactivated_width, self.slider_bar_height),
            )
            # filled part
            slider_filled_colour = interpolate_colours(
                self.sliders_colour[slider_name],
                self.sliders_colour['empty'],
                slider_activation,
            )
            pygame.draw.rect(
                self.screen,
                slider_filled_colour,
                (*top_left_coords, slider_activated_width, self.slider_bar_height),
            )
            # knobs
            knob_x = slider_x - self.knob_drawing_offset_x
            knob_y = slider_y - self.knob_drawing_offset_y
            self.screen.blit(self.sliders_icons[slider_name], (knob_x, knob_y))

            # text
            slider_text_colour = interpolate_colours(
                self.sliders_colour[slider_name], self.sliders_colour['empty'], 0.4
            )
            slider_text = self.font.render(
                f'{self.sliders[slider_name]():.3f}', True, slider_text_colour
            )
            slider_location = (
                int(self.panel_width * 0.4),
                slider_y - int(self.font.get_linesize() * 1.6),
            )
            self.screen.blit(slider_text, slider_location)

    def draw_right_panel(self):
        """
        Draw the elements of the right panel
        """
        for index, image in self.thumbnails_image.items():
            thumbnail_location = self.thumbnails_top_left[index]
            thumbnail_pyframe = painter_thumbnail_image_to_pygame_surface(
                image, self.thumbnail_size
            )
            self.screen.blit(thumbnail_pyframe, thumbnail_location)

    def update_brushes(self):
        """
        Update the brush based on the slider values and style selection
        """
        if self.slider_dragging == 'thickness':
            smooth_thickness = create_gaussian_kernel(
                self.item_size, self.sliders['thickness']()
            )
            self.brush_thickness = (smooth_thickness > 0.5).astype(np.float32)
            self.brush_thickness_pyframe = grayscale_to_pygame_frame(self.brush_thickness)

    def handle_hover_events(self):
        """
        Temporarily modify the appearance of different objects when the mouse hovers over.
        For example, set the icon of the mouse cursor depending on its location
        """
        if pygame.mouse.get_focused():
            mouse_x, mouse_y = pygame.mouse.get_pos()

            if self.slider_dragging is not None:
                pygame.mouse.set_cursor(*self.hand_closed_cursor)
            elif self.slider_in_cursor_area is not None:
                pygame.mouse.set_cursor(*self.hand_open_cursor)
            elif self.panel_width < mouse_x < self.screen_shape[0] - self.panel_width:
                pygame.mouse.set_cursor(*self.brush_cursor)
            else:
                pygame.mouse.set_cursor(*pygame.cursors.arrow)

            brush_index = self.brush_selection_map[mouse_x, mouse_y]
            if brush_index >= 0:
                rect_x, rect_y = self.thumbnails_top_left[brush_index]
                pygame.draw.rect(
                    self.screen,
                    (232, 232, 0),
                    (rect_x, rect_y, self.thumbnail_size, self.thumbnail_size),
                    width=2,
                )

    def handle_admin_events(self):
        """
        Handle top-level app controlling events like turning on the prediction mode
        """
        self.mouse_click = False

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            elif event.type == pygame.MOUSEBUTTONDOWN and pygame.mouse.get_pressed()[0]:
                self.mouse_click = True
            elif event.type == pygame.MOUSEBUTTONUP:
                self.slider_dragging = None
                if self.drawing:
                    self.drawing = False
                    self.to_update_canvas = True
            elif event.type == pygame.KEYDOWN:
                if pygame.key.get_mods() & pygame.KMOD_CTRL:
                    if (
                        event.key == pygame.K_z
                        and self.undo_counter < self.BUFFER_SIZE - 1
                    ):
                        self.undo_counter += 1
                        self.canvas_seed = np.roll(self.canvas_seed, shift=-1, axis=0)
                        self.canvas_mask = np.roll(self.canvas_mask, shift=-1, axis=0)
                        canvas_frame = self.predict_canvas(
                            self.canvas_seed[:1], self.canvas_mask[:1]
                        )
                        self.canvas_pyframe = canvas_frame_to_pygame_frame(canvas_frame)

                    elif event.key == pygame.K_y and self.undo_counter > 0:
                        self.undo_counter -= 1
                        self.canvas_seed = np.roll(self.canvas_seed, shift=1, axis=0)
                        self.canvas_mask = np.roll(self.canvas_mask, shift=1, axis=0)
                        canvas_frame = self.predict_canvas(
                            self.canvas_seed[:1], self.canvas_mask[:1]
                        )
                        self.canvas_pyframe = canvas_frame_to_pygame_frame(canvas_frame)

                    elif event.key == pygame.K_r:
                        self.undo_counter = 0
                        self.canvas_seed[0] = self.canvas_seed_initial
                        self.canvas_mask[0] = self.canvas_mask_initial
                        canvas_frame = self.predict_canvas(
                            self.canvas_seed[:1], self.canvas_mask[:1]
                        )
                        self.canvas_pyframe = canvas_frame_to_pygame_frame(canvas_frame)

    def handle_slider_events(self):
        """
        Handle the change in position of the sliders
        """

        def in_slider_area(mouse_x, mouse_y, slider_x, slider_y):
            return (
                abs(slider_x - mouse_x) < self.slider_size // 2
                and abs(slider_y - mouse_y) < self.slider_size // 2
            )

        mouse_x, mouse_y = pygame.mouse.get_pos()

        # detect cursor location
        self.slider_in_cursor_area = None
        for slider_name, slider in self.sliders.items():
            if in_slider_area(
                mouse_x,
                mouse_y,
                slider.get_position(),
                self.sliders_top_left[slider_name][1] + self.slider_bar_height // 2,
            ):
                self.slider_in_cursor_area = slider_name

        left_click, _, _ = pygame.mouse.get_pressed()
        if not left_click:
            return

        # change slider position
        for slider_name, slider in self.sliders.items():
            if self.slider_dragging == slider_name or (
                self.mouse_click and self.slider_in_cursor_area == slider_name
            ):
                self.slider_dragging = slider_name
                slider.set_position(mouse_x)

    def handle_brush_selection(self):
        """
        Handle the selection of brushes by checking the thumbnail clicks
        """

        def get_selected_brush(mouse_x, mouse_y):
            selected_brush = self.brush_selection_map[mouse_x, mouse_y]
            if selected_brush >= 0:
                return selected_brush
            return None

        if not self.mouse_click:
            return

        mouse_x, mouse_y = pygame.mouse.get_pos()
        selected_brush = get_selected_brush(mouse_x, mouse_y)

        if selected_brush is not None:
            self.brush_index = selected_brush
            self.brush_pyframe = painter_thumbnail_image_to_pygame_surface(
                self.thumbnails_image[selected_brush], self.item_size
            )

    def handle_drawing_events(self):
        """
        Handle the drawing updates on the canvas
        """

        def in_canvas_area(mouse_x, mouse_y):
            x_cond = self.panel_width <= mouse_x < self.screen_shape[0] - self.panel_width
            y_cond = 0 <= mouse_y < self.screen_shape[1]
            return x_cond and y_cond

        def surface_coords_to_canvas_coords(surface_x, surface_y):
            canvas_r = surface_y
            canvas_c = surface_x - self.panel_width
            return canvas_r, canvas_c

        mouse_x, mouse_y = pygame.mouse.get_pos()

        # start drawing
        if self.mouse_click:
            self.drawing = in_canvas_area(mouse_x, mouse_y)
            self.drawing_change = np.zeros(
                (self.canvas_height, self.canvas_width), dtype=np.bool
            )

        # continue drawing
        if self.drawing:
            canvas_r, canvas_c = surface_coords_to_canvas_coords(mouse_x, mouse_y)

            # top row
            canvas_tr_raw = canvas_r - self.item_half_size
            canvas_tr = max(canvas_tr_raw, 0)
            brush_tr = canvas_tr - canvas_tr_raw
            if brush_tr >= self.item_size:
                return

            # bottom row
            canvas_br_raw = canvas_tr_raw + self.item_size
            canvas_br = min(canvas_br_raw, self.canvas_height)
            brush_br = self.item_size - (canvas_br_raw - canvas_br)
            if brush_br <= 0:
                return

            # left column
            canvas_lc_raw = canvas_c - self.item_half_size
            canvas_lc = max(canvas_lc_raw, 0)
            brush_lc = canvas_lc - canvas_lc_raw
            if brush_lc >= self.item_size:
                return

            # right column
            canvas_rc_raw = canvas_lc_raw + self.item_size
            canvas_rc = min(canvas_rc_raw, self.canvas_width)
            brush_rc = self.item_size - (canvas_rc_raw - canvas_rc)
            if brush_rc <= 0:
                return

            # the changes as a result of the last strokes
            self.drawing_change[canvas_tr:canvas_br, canvas_lc:canvas_rc] = np.maximum(
                self.drawing_change[canvas_tr:canvas_br, canvas_lc:canvas_rc],
                self.brush_thickness[brush_tr:brush_br, brush_lc:brush_rc] > 0.5,
            )

    def create_drawing_seed(self, old_seed, change_mask):
        """
        Create a new seed for the model by updating only the required areas

        Args:
            old_seed (np.array): the previous input seed [H, W, C]
            change_mask (np.array): the boolean mask of the areas to update [H, W]
        Return:
            drawing_seed (np.array): the new seed [H, W, C]
        """
        new_seed = np.random.normal(
            size=change_mask.shape,
            loc=self.sliders['mean'](),
            scale=self.sliders['sigma'](),
        )
        drawing_seed = np.copy(old_seed)
        drawing_seed[..., 0][change_mask] = new_seed[change_mask]

        return drawing_seed

    def create_drawing_mask(self, old_mask, change_mask, style_index):
        """
        Create a new mask for the model by updating only the required areas

        Args:
            old_mask (np.array): the previous input mask [H, W]
            change_mask (np.array): the boolean mask of the areas to update [H, W]
            style_index (int): the index of the brush style
        Return:
            drawing_mask (np.array): the new mask [H, W]
        """
        drawing_mask = np.copy(old_mask)
        drawing_mask[change_mask] = style_index

        return drawing_mask

    def predict_canvas(self, seed, mask):
        """
        Pass seed and mask as inputs to obtain a model painting prediction

        Args:
            seed (np.array): input seed [B, H, W, C]
            mask (np.array): input mask [B, H, W]
        Return:
            prediction (tf.Tensor): model prediction [B, H, W, C]
        """
        inputs = dict(
            seed=seed, mask=tf.one_hot(mask, depth=self.num_labels, dtype=tf.float32)
        )
        return self.model(inputs)

    def run(self):
        """
        Run the camera loop for the app
        """
        while True:
            self.handle_admin_events()
            self.handle_slider_events()
            self.handle_brush_selection()
            self.handle_drawing_events()
            self.update_brushes()

            self.screen.fill([0, 0, 0])
            self.screen.blit(self.background, (0, 0))
            self.draw_central_panel()
            self.draw_left_panel()
            self.draw_right_panel()
            self.handle_hover_events()

            pygame.display.update()


def parse_arguments():
    parser = argparse.ArgumentParser(description='Select the gem')
    parser.add_argument('-g', '--gem_path', help='path to the gem folder')
    args = parser.parse_args()
    return args


def main(args):
    gem_setup = load_gem_setup(args.gem_path)

    try:
        app_runner = PainterAppRunner(
            gem_setup['model'],
            gem_setup['reconstructor'],
            gem_setup['mask'],
            gem_setup['thumbnails'],
        )
        app_runner.run()

    except (KeyboardInterrupt, SystemExit):
        pass


if __name__ == '__main__':
    args = parse_arguments()
    main(args)
