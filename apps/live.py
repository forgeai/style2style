import sys
import cv2
import pygame
import argparse
import tensorflow as tf
from os.path import join

tf.config.optimizer.set_jit(True)

import logging
import logging.config

logging.config.fileConfig('logging.conf')
logger = logging.getLogger(__name__)

sys.path.append('.')
from utils.constants import EPSILON
from utils.apps import (
    StylePointer,
    StyleSwitchCircularBuffer,
    load_gem_setup,
    load_image_to_pygame,
    create_empty_thumbnail,
    tensor_to_pygame_frame,
    opencv_frame_to_pygame_frame,
    opencv_frame_to_prediction_ready_tensor,
    live_thumbnail_tensor_to_pygame_surface,
)


class LiveAppRunner:
    def __init__(self, display_shape, camera, model):
        self.camera = camera
        self.model = model
        self.initialize_screen(display_shape)
        self.denoising = False

        pygame.init()
        pygame.display.set_caption('Live')
        self.font = pygame.font.SysFont('arial', 18)

        font_height = self.font.get_linesize()
        self.fps_location = (0, display_shape[1] - font_height)

    def initialize_screen(self, display_shape):
        """
        Create and initialize the pygame screen

        Args:
            display_shape (tuple of 2 ints): the resolution of the display image
        """
        self.screen = pygame.display.set_mode(display_shape, flags=0, depth=32)

    def handle_admin_events(self):
        """
        Handle top-level app controlling events like turning on the prediction mode
        """
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()

            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_c:
                    if self.mode == 'camera':
                        self.mode = 'prediction'
                    elif self.mode == 'prediction':
                        self.mode = 'camera'

                elif event.key == pygame.K_f:
                    self.denoising = not self.denoising

    def run(self):
        """
        Run the camera loop for the app
        """
        self.mode = 'camera'
        clock = pygame.time.Clock()

        while True:
            time_passed_milliseconds = clock.tick()
            time_passed_seconds = time_passed_milliseconds / 1000.0
            self.handle_admin_events()

            # camera
            ret, cv2_frame = self.camera.read()
            if not ret:
                logger.error('Cannot read frame from camera')
                break
            if self.denoising:
                cv2_frame = cv2.fastNlMeansDenoisingColored(cv2_frame, None, 8, 8, 5, 7)

            # AI inference
            if self.mode == 'prediction':
                model_frame = opencv_frame_to_prediction_ready_tensor(cv2_frame)
                synthesized_frame = self.model(model_frame)
                pygame_frame = tensor_to_pygame_frame(synthesized_frame)
            else:
                pygame_frame = opencv_frame_to_pygame_frame(cv2_frame)

            # drawing
            self.screen.fill([0, 0, 0])
            self.screen.blit(pygame_frame, (0, 0))

            # display fps
            fps = 1.0 / (time_passed_seconds + EPSILON)
            self.screen.blit(
                self.font.render(f'{fps:.2f}', True, (255, 255, 255)), self.fps_location,
            )

            pygame.display.update()


class LiveAppRunnerMultiStyle(LiveAppRunner):
    def __init__(self, display_shape, camera, model, thumbnails, is_dual):
        self.is_dual = is_dual
        self.thumbnails = thumbnails
        self.num_styles = len(thumbnails)
        self.thumbnails[None] = create_empty_thumbnail(thumbnails[0].numpy().shape)
        super().__init__(display_shape, camera, model)

    def initialize_screen(self, display_shape):
        """
        Create and initialize the pygame screen

        Args:
            display_shape (tuple of 2 ints): the resolution of the display image
        """
        tn_shape = self.thumbnails[0].shape
        self.tn_width = display_shape[0] // 4
        self.tn_height = int(tn_shape[0] / tn_shape[1] * self.tn_width)

        self.screen_width = display_shape[0] + 2 * self.tn_width
        self.screen_height = display_shape[1] + 2 * self.tn_height
        self.screen = pygame.display.set_mode(
            (self.screen_width, self.screen_height), flags=0, depth=32
        )

    def initialize_display_locations(self):
        """
        Initialize the variables that control where to blit stuff on the screen
        """
        font_height = self.font.get_linesize()

        self.cw_x13 = self.tn_width
        self.cw_x24 = self.screen_width - self.tn_width - font_height * 2.25
        self.cw_y12 = self.tn_height - font_height
        self.cw_y34 = self.screen_height - self.tn_height
        self.fps_location = (self.tn_width, self.screen_height - font_height)

        self.switch_arrow_location = (
            (self.screen_width - self.tn_width) // 2,
            (self.screen_height - self.tn_height) // 2,
        )
        self.switch_selected_location = (
            (self.screen_width - self.tn_width) // 2,
            self.screen_height // 4 - self.tn_height // 2,
        )
        self.switch_contender_location = (
            (self.screen_width - self.tn_width) // 2,
            3 * self.screen_height // 4 - self.tn_height // 2,
        )
        self.switch_prev_location = (
            (2 * self.screen_width - 5 * self.tn_width) // 4,
            (3 * self.screen_height - self.tn_height) // 4,
        )
        self.switch_next_location = (
            (2 * self.screen_width + 3 * self.tn_width) // 4,
            (3 * self.screen_height - self.tn_height) // 4,
        )

        self.tn_locations = {
            1: (0, 0),
            2: (self.screen_width - self.tn_width, 0),
            3: (0, self.screen_height - self.tn_height),
            4: (self.screen_width - self.tn_width, self.screen_height - self.tn_height),
        }

    def initialize_mini_utils(self):
        """
        Initilize the StylePointer and the StyleSwitchCircularBuffer objects
        together with all their utils
        """
        pointer_size = 48  # pixels
        self.pointer_half = pointer_size // 2
        self.moving_speed = 100.0  # pixels per second
        self.intensity_speed = 0.2  # amount per second
        self.sp_delta = [0, 0, 0]  # amount to change in each dimension

        self.switch_arrow_icon = load_image_to_pygame(
            join('utils', 'images', 'switch_arrow.png'), self.tn_width
        )
        self.pointer_icon = load_image_to_pygame(
            join('utils', 'images', 'butterfly.png'), pointer_size
        )

        self.switch_buffer = None
        self.style_pointer = StylePointer(
            self.screen_width,
            self.screen_height,
            self.tn_width,
            self.tn_height,
            self.num_styles,
            self.is_dual,
            self.tn_width // 2,
            self.tn_height // 2,
        )

    def initialize_thumbnails(self):
        """
        Initialize the style thumbnails display utils
        """
        self.render_full = lambda x: pygame.transform.scale(
            live_thumbnail_tensor_to_pygame_surface(x), (self.tn_width, self.tn_height)
        )
        self.render_mini = lambda x: pygame.transform.scale(
            live_thumbnail_tensor_to_pygame_surface(x),
            (self.tn_width // 2, self.tn_height // 2),
        )

        self.tn_slots = {x + 1: x for x in range(min(4, self.num_styles))}
        self.tn_images = {
            k: self.render_full(self.thumbnails[v]) for k, v in self.tn_slots.items()
        }

    def switch_styles(self):
        """
        Switch between the position of two styles as selected by the switch buffer
        """
        # locate style indices in slots
        style_1_slot = self.selected_style_slot
        style_1_index = self.tn_slots[self.selected_style_slot]

        style_2_slot = None
        style_2_index = self.switch_buffer.current()
        for k, v in self.tn_slots.items():
            if v == style_2_index:
                style_2_slot = k

        # switch style indices in the slots
        self.tn_slots[style_1_slot] = style_2_index
        if style_2_slot is not None:
            self.tn_slots[style_2_slot] = style_1_index

        # update style pointer
        self.style_pointer.update_sec2idx(self.tn_slots)

        # update thumbnail locations
        self.tn_images = {
            k: self.render_full(self.thumbnails[v]) for k, v in self.tn_slots.items()
        }

    def handle_admin_events(self):
        """
        Handle top-level app controlling events like exiting or switching styles
        """
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()

            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_c:
                    if self.mode == 'camera':
                        self.mode = 'prediction'
                    elif self.mode == 'prediction':
                        self.mode = 'camera'

                elif event.key == pygame.K_f:
                    self.denoising = not self.denoising

                elif self.mode == 'prediction' and self.num_styles > 1:
                    if event.key == pygame.K_s:
                        self.selected_style_slot = (
                            self.style_pointer.get_current_valid_style_slot()
                        )

                        if self.selected_style_slot is not None:
                            self.mode = 'switch'
                            self.switch_buffer = StyleSwitchCircularBuffer(
                                self.num_styles, self.tn_slots[self.selected_style_slot]
                            )

                elif self.mode == 'switch':
                    if event.key == pygame.K_LEFT:
                        self.switch_buffer.move_left()

                    elif event.key == pygame.K_RIGHT:
                        self.switch_buffer.move_right()

                    elif event.key == pygame.K_ESCAPE:
                        self.mode = 'prediction'

                    elif event.key == pygame.K_RETURN:
                        self.mode = 'prediction'
                        self.switch_styles()
                        self.switch_buffer = None

    def handle_style_events(self):
        """
        Handle lower level app events like changing the position of the style pointer
        for multi-style interpolation
        """
        pressed_keys = pygame.key.get_pressed()

        # small increments
        self.sp_delta[0] = pressed_keys[pygame.K_RIGHT] - pressed_keys[pygame.K_LEFT]
        self.sp_delta[1] = pressed_keys[pygame.K_DOWN] - pressed_keys[pygame.K_UP]
        self.sp_delta[2] = pressed_keys[pygame.K_d] - pressed_keys[pygame.K_a]

        # style hotkeys
        teleporting_speed = 1024
        if pressed_keys[pygame.K_1]:
            self.sp_delta[0] = -teleporting_speed
            self.sp_delta[1] = -teleporting_speed
        elif pressed_keys[pygame.K_2]:
            self.sp_delta[0] = teleporting_speed
            self.sp_delta[1] = -teleporting_speed
        elif pressed_keys[pygame.K_3]:
            self.sp_delta[0] = -teleporting_speed
            self.sp_delta[1] = teleporting_speed
        elif pressed_keys[pygame.K_4]:
            self.sp_delta[0] = teleporting_speed
            self.sp_delta[1] = teleporting_speed

        self.mode = 'prediction' if self.sp_delta == [0, 0, 0] else 'pointer'

    def run(self):
        """
        Run the camera loop for the app
        """
        self.mode = 'camera'
        clock = pygame.time.Clock()

        self.initialize_thumbnails()
        self.initialize_mini_utils()
        self.initialize_display_locations()

        while True:
            time_passed_milliseconds = clock.tick()
            time_passed_seconds = time_passed_milliseconds / 1000.0
            self.handle_admin_events()

            # switching active styles
            if self.mode == 'switch':
                self.screen.fill([0, 0, 0])
                self.screen.blit(self.switch_arrow_icon, self.switch_arrow_location)
                self.screen.blit(
                    self.tn_images[self.selected_style_slot],
                    self.switch_selected_location,
                )

                self.screen.blit(
                    self.render_full(self.thumbnails[self.switch_buffer.current()]),
                    self.switch_contender_location,
                )
                self.screen.blit(
                    self.render_mini(self.thumbnails[self.switch_buffer.prev()]),
                    self.switch_prev_location,
                )
                self.screen.blit(
                    self.render_mini(self.thumbnails[self.switch_buffer.next()]),
                    self.switch_next_location,
                )

                pygame.display.update()
                continue

            if self.mode == 'camera':
                self.sp_delta = [0, 0, 0]
            else:
                self.handle_style_events()

                # style pointer updates
                moving_distance = self.moving_speed * time_passed_seconds
                intensity_distance = self.intensity_speed * time_passed_seconds

                self.style_pointer.update_x(moving_distance * self.sp_delta[0])
                self.style_pointer.update_y(moving_distance * self.sp_delta[1])
                self.style_pointer.update_z(intensity_distance * self.sp_delta[2])
                self.style_pointer.update_condition_weights()

            # camera
            ret, cv2_frame = self.camera.read()
            if not ret:
                logger.error('Cannot read frame from camera')
                break
            if self.denoising:
                cv2_frame = cv2.fastNlMeansDenoisingColored(cv2_frame, None, 8, 8, 5, 7)

            # AI inference
            cw = self.style_pointer.get_condition_weights()
            if self.mode == 'prediction':
                model_frame = opencv_frame_to_prediction_ready_tensor(cv2_frame)
                inputs = dict(input_image=model_frame, condition_weights=cw)
                synthesized_frame = self.model(inputs)
                pygame_frame = tensor_to_pygame_frame(synthesized_frame)
            else:
                pygame_frame = opencv_frame_to_pygame_frame(cv2_frame)

            # drawing
            self.screen.fill([0, 0, 0])
            self.screen.blit(pygame_frame, (self.tn_width, self.tn_height))

            for slot, style_tn in self.tn_images.items():
                self.screen.blit(style_tn, self.tn_locations[slot])
            if self.mode == 'pointer':
                self.screen.blit(
                    self.pointer_icon,
                    (
                        self.style_pointer.x - self.pointer_half,
                        self.style_pointer.y - self.pointer_half,
                    ),
                )

            # display condition weights
            if self.mode in ('pointer', 'prediction'):
                index_shift = self.is_dual * self.num_styles

                slot_cw = cw[self.tn_slots[1] + index_shift]
                self.screen.blit(
                    self.font.render(
                        '{:.3f}'.format(slot_cw),
                        True,
                        (255 - int(255 * slot_cw), int(255 * slot_cw), 0),
                    ),
                    (self.cw_x13, self.cw_y12),
                )
                if self.num_styles > 1:
                    slot_cw = cw[self.tn_slots[2] + index_shift]
                    self.screen.blit(
                        self.font.render(
                            '{:.3f}'.format(slot_cw),
                            True,
                            (255 - int(255 * slot_cw), int(255 * slot_cw), 0),
                        ),
                        (self.cw_x24, self.cw_y12),
                    )
                if self.num_styles > 2:
                    slot_cw = cw[self.tn_slots[3] + index_shift]
                    self.screen.blit(
                        self.font.render(
                            '{:.3f}'.format(slot_cw),
                            True,
                            (255 - int(255 * slot_cw), int(255 * slot_cw), 0),
                        ),
                        (self.cw_x13, self.cw_y34),
                    )
                if self.num_styles > 3:
                    slot_cw = cw[self.tn_slots[4] + index_shift]
                    self.screen.blit(
                        self.font.render(
                            '{:.3f}'.format(slot_cw),
                            True,
                            (255 - int(255 * slot_cw), int(255 * slot_cw), 0),
                        ),
                        (self.cw_x24, self.cw_y34),
                    )

            # display fps
            fps = 1.0 / (time_passed_seconds + EPSILON)
            self.screen.blit(
                self.font.render('Fps: {:.2f}'.format(fps), True, (255, 255, 255)),
                self.fps_location,
            )

            pygame.display.update()


def parse_arguments():
    parser = argparse.ArgumentParser(description='Load model and set resolution')
    parser.add_argument('-g', '--gem_path', help='path to the gem folder')
    parser.add_argument(
        '-r',
        '--resolution',
        nargs=2,
        type=int,
        default=(640, 480),
        help='camera resolution (width and height)',
    )

    args = parser.parse_args()
    return args


def main(args):

    # model setup
    gem_setup = load_gem_setup(args.gem_path)

    # camera setup
    camera = cv2.VideoCapture(cv2.CAP_DSHOW)
    if not camera.isOpened:
        logger.error('Could not open camera')
        return

    display_shape = (args.resolution[0], args.resolution[1])
    camera.set(cv2.CAP_PROP_FRAME_WIDTH, display_shape[0])
    camera.set(cv2.CAP_PROP_FRAME_HEIGHT, display_shape[1])

    try:
        if gem_setup['mode'] == 'single':
            app_runner = LiveAppRunner(display_shape, camera, gem_setup['model'])
        elif gem_setup['mode'] == 'multi':
            app_runner = LiveAppRunnerMultiStyle(
                display_shape,
                camera,
                gem_setup['model'],
                gem_setup['style_images'],
                is_dual=False,
            )
        elif gem_setup['mode'] == 'dual':
            app_runner = LiveAppRunnerMultiStyle(
                display_shape,
                camera,
                gem_setup['model'],
                gem_setup['style_images'],
                is_dual=True,
            )
        app_runner.run()

    except (KeyboardInterrupt, SystemExit):
        pass

    finally:
        camera.release()
        cv2.destroyAllWindows()


if __name__ == '__main__':
    args = parse_arguments()
    main(args)
