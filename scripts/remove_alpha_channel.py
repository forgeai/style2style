import os
import sys
import cv2
import glob
import argparse

sys.path.append('.')
from utils.constants import IMAGE_EXTENSIONS


def traverse_data_directories(data_path):
    """
    Traverse all directories starting from the top level specified in
    data_path and process each leaf folder that contains a set of images

    Args:
        data_path (str): path to a high-level directory from which the
                            process will start to execute by following all
                            folder branches
    """
    for content_tree in os.walk(data_path):
        # check if the folder contains at least one image
        for image_extension in IMAGE_EXTENSIONS:
            if glob.glob(os.path.join(content_tree[0], '*' + image_extension)):
                perform_alpha_channel_removal(content_tree[0])
                break


def perform_alpha_channel_removal(data_path):
    """
    Turn all 32-depth images into 24-depth representation by removing
    the alpha channel

    Args:
        data_path (str): path to the original images directory
    """
    image_paths = []
    for image_extension in IMAGE_EXTENSIONS:
        image_paths += glob.glob(os.path.join(data_path, '*' + image_extension))

    for image_path in image_paths:
        image = cv2.imread(image_path, cv2.IMREAD_COLOR)
        cv2.imwrite(image_path, image)


def parse_arguments():
    parser = argparse.ArgumentParser(description='Read data paths')
    parser.add_argument('-d', '--data_path', help='path to the high level data directory')
    args = parser.parse_args()
    return args


def main(args):
    traverse_data_directories(args.data_path)


if __name__ == '__main__':
    args = parse_arguments()
    main(args)
