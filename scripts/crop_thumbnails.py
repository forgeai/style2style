import sys
import cv2
import pygame
import argparse
import numpy as np
from os import makedirs
from os.path import join

sys.path.append('.')
from utils.apps import opencv_frame_to_pygame_frame
from utils.constants import PAINTING_THUMBNAIL_SHAPE
from models.gans.paint_gan.toolset import get_label_names


def get_chopping_corners(painting_shape, mouse_x, mouse_y, size):
    """
    Return the coordinates of the chopping box if all corners are inside the
    image or None otherwise

    Args:
        painting_shape (tuple of 2 ints): height and width of the painting
        mouse_x (int): x coordinate of the cursor
        mouse_y (int): y coordinate of the cursor
        size (int): the size of the patch to be chopped
    """
    half_size = size // 2
    xl = mouse_x - half_size
    xh = mouse_x + half_size
    yl = mouse_y - half_size
    yh = mouse_y + half_size

    if xl < 0 or yl < 0 or xh > painting_shape[1] or yh > painting_shape[0]:
        return None
    return (xl, xh, yl, yh)


def crop_thumbnails_interactively(painting_set_path):
    """
    Launch a pygame app that allows the user to select patches from the painting
    to be used as style thumbnails

    Args:
        painting_set_path (str): path to the location of the model's painting set
    Return:
        thumbnails (dict): maps label name to thumbnail image
    """
    thumbnails = {}
    painting = cv2.imread(join(painting_set_path, 'painting.png'), cv2.IMREAD_COLOR)
    labels = get_label_names(join(painting_set_path, 'label_names.txt'))

    pygame.init()
    pygame.display.set_caption('Thumbnail chopper')
    font = pygame.font.SysFont('arial', 20)
    text_colour = [10, 10, 10]
    calm_colour = [205, 221, 237]
    box_colour = [224, 10, 10]

    thumbnail_size = PAINTING_THUMBNAIL_SHAPE[0]
    panel_width = 2 * thumbnail_size
    screen_shape = (painting.shape[1] + panel_width, painting.shape[0])
    thumbnail_x = screen_shape[0] - (panel_width + thumbnail_size) // 2
    thumbnail_y = (screen_shape[1] - thumbnail_size) // 2
    text_x = painting.shape[1]
    text_y = thumbnail_y // 2

    screen = pygame.display.set_mode(screen_shape, flags=0, depth=32)
    painting_surface = opencv_frame_to_pygame_frame(painting, mirror=False)
    empty_patch = np.zeros(PAINTING_THUMBNAIL_SHAPE, dtype=np.uint8)
    patch_surface = opencv_frame_to_pygame_frame(empty_patch, mirror=False)

    labels_generator = (_ for _ in labels.items())
    label_index, label_name = next(labels_generator)
    chop_size = thumbnail_size
    still_chopping = True

    while still_chopping:
        for event in pygame.event.get():
            # quit and cancel the ongoing chopping operation
            if event.type == pygame.QUIT:
                return {}

            # change patch size
            if event.type == pygame.MOUSEWHEEL:
                chop_size = max(chop_size + 2 * event.y, thumbnail_size // 4)

            # display current selection
            mouse_x, mouse_y = pygame.mouse.get_pos()
            corners = get_chopping_corners(painting.shape, mouse_x, mouse_y, chop_size)
            if corners is not None:
                cl, ch, rl, rh = corners
                patch = painting[rl:rh, cl:ch]
                patch_surface = opencv_frame_to_pygame_frame(patch, mirror=False)
                patch_surface = pygame.transform.scale(
                    patch_surface, PAINTING_THUMBNAIL_SHAPE
                )

            # save current selection and move to the next label
            if (
                corners is not None
                and event.type == pygame.MOUSEBUTTONDOWN
                and pygame.mouse.get_pressed()[0]
            ):
                thumbnails[f'{label_index}_{label_name}'] = patch
                try:
                    label_index, label_name = next(labels_generator)
                except StopIteration:
                    still_chopping = False

        screen.fill(calm_colour)
        screen.blit(painting_surface, (0, 0))
        screen.blit(
            font.render(f'{label_index}: {label_name}', True, text_colour),
            (text_x, text_y),
        )
        if corners is not None:
            screen.blit(patch_surface, (thumbnail_x, thumbnail_y))
            pygame.draw.rect(screen, box_colour, (cl, rl, ch - cl, rh - rl), width=3)
        pygame.display.update()

    return thumbnails


def save_thumbnails(saving_path, thumbnails, thumbnail_shape):
    """
    Save the thumbnails to disk as images

    Args:
        saving_path (str): path to the folder where the images will be saved
        thumbnails (dict): maps label name to thumbnail image
        thumbnail_shape (tuple of 2 ints): height and width for the thumbnail images
    """
    makedirs(saving_path, exist_ok=True)
    for image_name, image in thumbnails.items():
        thumbnail = cv2.resize(image, thumbnail_shape)
        cv2.imwrite(join(saving_path, f'{image_name}.png'), thumbnail)


def parse_arguments():
    parser = argparse.ArgumentParser(description='Set thumbnail selection configurations')
    parser.add_argument(
        '-p',
        '--painting_set_path',
        help="path to the location of the model's painting set",
    )
    parser.add_argument(
        '-s', '--saving_path', help='folder where the thumbnails will be saved',
    )
    parser.add_argument(
        '-t',
        '--thumbnail_shape',
        help='height and width for the thumbnail images',
        nargs=2,
        default=PAINTING_THUMBNAIL_SHAPE,
    )
    args = parser.parse_args()
    return args


def main(args):
    assert (
        args.thumbnail_shape[0] == args.thumbnail_shape[1]
    ), 'Thumbnail shape must be square'

    thumbnails = crop_thumbnails_interactively(args.painting_set_path)
    save_thumbnails(args.saving_path, thumbnails, args.thumbnail_shape)


if __name__ == '__main__':
    args = parse_arguments()
    main(args)
