import sys
import argparse

import logging
import logging.config

logging.config.fileConfig('logging.conf')

sys.path.append('.')
from data_pipeline.factory.image_processing import (
    refine_data_by_range,
    refine_data_by_fixed_size,
)


def parse_arguments():
    parser = argparse.ArgumentParser(description='Refine raw image data')
    parser.add_argument('-d', '--data_path', help='path to the raw data directory')
    parser.add_argument(
        '-f',
        '--fixed_dim',
        action='store_true',
        default=False,
        help='sets the size parameters for fixed (height, width);'
        + ' otherwise they are interpreted as range (min_dim, max_dim)',
    )
    parser.add_argument(
        '-s',
        '--size',
        nargs=2,
        type=int,
        help='image size parameters whose meaning depends on the -f flag',
    )
    parser.add_argument(
        '-m',
        '--mode',
        type=str,
        help='the fixed dimension resizing mode ("pad" or "crop")',
        default='pad',
    )
    args = parser.parse_args()
    return args


def main(args):
    if args.fixed_dim:
        refine_data_by_fixed_size(args.data_path, args.size, args.mode)
    else:
        refine_data_by_range(args.data_path, *args.size)


if __name__ == '__main__':
    args = parse_arguments()
    main(args)
