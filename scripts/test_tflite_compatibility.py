import sys
import argparse

import logging
import logging.config

logging.config.fileConfig('logging.conf')

sys.path.append('.')
from utils.tflite import is_tflite_compatible


def parse_arguments():
    parser = argparse.ArgumentParser(description='Input path to the keras model')
    parser.add_argument('-m', '--model_path', help='path to the model directory')
    args = parser.parse_args()
    return args


def main(args):
    logger = logging.getLogger(__name__)

    if is_tflite_compatible(args.model_path, numerical_test=True):
        logger.info(f'{args.model_path} is tflite compatible :)')


if __name__ == '__main__':
    args = parse_arguments()
    main(args)
