import sys
import argparse

import logging
import logging.config

logging.config.fileConfig('logging.conf')

sys.path.append('.')
from data_pipeline.factory.tfrecords_creation import traverse_data_directories


def parse_arguments():
    parser = argparse.ArgumentParser(description='Read data path')
    parser.add_argument('-d', '--data_path', help='path to the high level data directory')
    parser.add_argument(
        '-s',
        '--samples_per_record',
        type=int,
        help='number of samples to encapsulate in one record',
        default=32,
    )
    args = parser.parse_args()
    return args


def main(args):
    traverse_data_directories(args.data_path, args.samples_per_record)


if __name__ == '__main__':
    args = parse_arguments()
    main(args)
