import sys
import argparse

sys.path.append('.')
from utils.constants import EPSILON
from data_pipeline.factory.dataset_allocation import traverse_data_directories


def parse_arguments():
    parser = argparse.ArgumentParser(description='Read data paths')
    parser.add_argument('-d', '--data_path', help='path to the high level data directory')
    parser.add_argument(
        '-s',
        '--splits',
        type=float,
        nargs=3,
        help='(training, validation, testing) ' + 'percentages for splitting the data',
        default=[0.7, 0.2, 0.1],
    )
    args = parser.parse_args()
    return args


def main(args):
    assert all([p >= 0.0 for p in args.splits]), "Percentages must be >= 0"
    assert all([p <= 1.0 for p in args.splits]), "Percentages must be <= 1"
    assert abs(1.0 - sum(args.splits)) < EPSILON, "Percentages must add up to a whole"
    traverse_data_directories(args.data_path, args.splits)


if __name__ == '__main__':
    args = parse_arguments()
    main(args)
