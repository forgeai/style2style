import sys
import argparse
from os import makedirs
from os.path import join, isdir, basename
from shutil import copytree
from tensorflow import keras

import logging
import logging.config

logging.config.fileConfig('logging.conf')

sys.path.append('.')
from models.factory.file_handling import get_latest_version
from models.factory.component_serialization import load_component
from scripts.crop_thumbnails import crop_thumbnails_interactively, save_thumbnails
from utils.json_processor import dict2json, json2dict, json2namespace
from utils.constants import (
    CYCLE_MODELS,
    PAINT_MODELS,
    NOISE_MODELS,
    ARTISTIC_STYLE_MODELS,
    PAINTING_THUMBNAIL_SHAPE,
)

logger = logging.getLogger(__name__)


def get_component_dict(model_path, model_type, model_version, component):
    """
    Read the component config file in the dictionary format

    Args:
        model_path (str): path to the model folder
        model_type (str): type of the model (cycle_gan, artistic_cin_gm, etc.)
        model_version (int): the version of the model to use
        component (str): component name (G_X2Y, style_net, etc.)
    Return
        component_dict (dict)
    """
    component_config_path = join(
        model_path, str(model_version), 'checkpoints', component, 'model_config.json'
    )
    return json2dict(component_config_path)


def create_gem_config(gem_path, model_path, model_type, model_version, component):
    """
    Create the gem configurations file and save it in the gem folder

    Args:
        gem_path (str): the folder that contains all the gem stuff
        model_path (str): path to the model folder
        model_type (str): type of the model (cycle_gan, artistic_cin_gm, etc.)
        model_version (int): the version of the model to use
        component (str): component name (G_X2Y, style_net, etc.)
    """
    component_dict = get_component_dict(model_path, model_type, model_version, component)
    gem_dict = dict(
        name=basename(gem_path),
        model_type=model_type,
        component_name=component,
        component=component_dict,
    )
    dict2json(gem_dict, join(gem_path, 'gem_config.json'))


def copy_weights(gem_path, model_path, model_version, component):
    """
    Copy the weights of the component to the gem folder

    Args:
        gem_path (str): the folder that contains all the gem stuff
        model_path (str): path to the model folder
        model_version (int): the version of the model to use
        component (str): component name (G_X2Y, style_net, etc.)
    """
    checkpoint_path = join(model_path, str(model_version), 'checkpoints', component)
    gem_weights_path = join(gem_path, 'weights')
    copytree(checkpoint_path, gem_weights_path)


def export_as_saved_model(gem_path, gem_name):
    """
    Serialize gem using saved_model in a separate folder

    Args:
        gem_path (str): the folder that contains all the gem stuff
        gem_name (str): the name of the gem
    """
    model_weights_path = join(gem_path, 'weights')
    saved_model_path = join(gem_path, 'saved_model', gem_name)
    makedirs(saved_model_path)

    model = load_component(model_weights_path)
    keras.models.save_model(model, saved_model_path, save_format='tf')


def copy_auxiliaries(gem_path, model_path, model_type):
    """
    Copy other auxiliaries of the model to the gem folder

    Args:
        gem_path (str): the folder that contains all the gem stuff
        model_path (str): path to the model folder
        model_type (str): type of the model (cycle_gan, artistic_cin_gm, etc.)
    """
    if model_type in ARTISTIC_STYLE_MODELS:
        copytree(join(model_path, 'style_images'), join(gem_path, 'style_images'))

        if model_type in NOISE_MODELS:
            copytree(join(model_path, 'reconstructors'), join(gem_path, 'reconstructors'))

    if model_type in PAINT_MODELS:
        copytree(join(model_path, 'reconstructors'), join(gem_path, 'reconstructors'))

        model_painting_set_path = join(model_path, 'painting_set')
        gem_painting_set_path = join(gem_path, 'painting_set')
        copytree(model_painting_set_path, gem_painting_set_path)

        thumbnails_path = join(gem_painting_set_path, 'thumbnails')
        thumbnails = crop_thumbnails_interactively(model_painting_set_path)
        save_thumbnails(thumbnails_path, thumbnails, PAINTING_THUMBNAIL_SHAPE)


def check_validity(args):
    """
    Check if the arguments provided to the script are valid and the
    polishing operation can be executed safely

    Args:
        args (argparse.Namespace): the script arguments
    Return:
        (boolean): True if the use case is valid,
                   False if an incomaptibility was found
    """
    if args.version is not None and not isdir(join(args.model_path, str(args.version))):
        logger.error('Version {} does not exist'.format(args.version))
        return False

    if isdir(join('apps', 'gems', args.section, args.name)):
        logger.error('Gem "{}" already exists in {}'.format(args.name, args.section))
        return False

    model_config = json2namespace(join(args.model_path, 'model_config.json'))
    if model_config.type in CYCLE_MODELS:
        if args.component not in ('G_X2Y', 'G_Y2X'):
            logger.error('Component must be "G_X2Y" or "G_Y2X" for this model')
            return False

    elif model_config.type in ARTISTIC_STYLE_MODELS:
        if args.component != 'style_net':
            logger.error('Component must be "style_net" for this model')
            return False

    elif model_config.type in PAINT_MODELS:
        if args.component != 'paint_net':
            logger.error('Component must be "paint_net" for this model')
            return False

    return True


def parse_arguments():
    parser = argparse.ArgumentParser(description='Extract and polish a gem from a model')
    parser.add_argument('-s', '--section', help='gem section (folder where to store)')
    parser.add_argument('-n', '--name', help='name of the gem')
    parser.add_argument('-m', '--model_path', help='path to the model folder')
    parser.add_argument('-c', '--component', help='component of interest')
    parser.add_argument(
        '-v',
        '--version',
        type=int,
        default=None,
        help='version of the model; if not specified the latest is used',
    )
    args = parser.parse_args()
    return args


def main(args):

    if not check_validity(args):
        return

    logger.info('Polishing gem...')
    model_path = args.model_path
    model_config = json2namespace(join(model_path, 'model_config.json'))
    model_version = (
        get_latest_version(model_path) if args.version is None else args.version
    )
    gem_path = join('apps', 'gems', args.section, args.name)
    makedirs(gem_path)

    logger.info('\tCreating gem config...')
    create_gem_config(
        gem_path, model_path, model_config.type, model_version, args.component
    )

    logger.info('\tCopying weights...')
    copy_weights(gem_path, model_path, model_version, args.component)

    if model_config.type in PAINT_MODELS:
        logger.info('\tExporting as saved_model...')
        export_as_saved_model(gem_path, args.name)

    logger.info('\tCopying auxiliaries...')
    copy_auxiliaries(gem_path, model_path, model_config.type)

    logger.info('Gem "{}" polished succesfully!'.format(args.name))


if __name__ == '__main__':
    args = parse_arguments()
    main(args)
