import sys
import argparse
import matplotlib.pyplot as plt

sys.path.append('.')
from utils.image_processor import tensor2image
from utils.json_processor import dict2namespace, json2namespace
from data_pipeline.factory.noise_generation import (
    get_image_shape,
    generate_noise_dataset,
    generate_noise_masks_dataset,
)


def display_noise_samples(image_shape, noise_config, is_masks):
    """
    Display 8 images with noise generated by the configurations

    Args:
        image_shape (tuple): the shape for the images (rank 2)
        noise_config (namesapace): configurations for noise generation
        is_masks (bool): whether to generate binary masks or RGB images
    """
    fig = plt.figure()
    fig.subplots_adjust(hspace=0.1, wspace=0.1)
    plt.title('{} noise samples'.format(noise_config.type))
    plt.axis('off')

    noise_dataset_creator = (
        generate_noise_masks_dataset if is_masks else generate_noise_dataset
    )
    noise_dataset = noise_dataset_creator(8, image_shape, noise_config)

    for i, noise_image in enumerate(noise_dataset):
        ax = fig.add_subplot(2, 4, i + 1)
        ax.set_yticks([])
        ax.set_xticks([])
        if not is_masks:
            noise_image = tensor2image(noise_image)
        ax.imshow(noise_image)

    plt.show()


def parse_arguments():
    parser = argparse.ArgumentParser(description='Input noise configurations path')
    parser.add_argument('-d', '--data_config', help='path to the data config file')
    parser.add_argument(
        '-m',
        '--masks',
        action='store_true',
        help='create binary masks instead of RGB images',
    )
    args = parser.parse_args()
    return args


def main(args):
    data_config = json2namespace(args.data_config)
    image_shape = get_image_shape(data_config)
    noise_config = dict2namespace(data_config.noise)

    display_noise_samples(image_shape, noise_config, args.masks)


if __name__ == '__main__':
    args = parse_arguments()
    main(args)
