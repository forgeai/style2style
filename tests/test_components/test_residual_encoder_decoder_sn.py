import unittest
import numpy as np
import tensorflow as tf
from os.path import join, isfile
from tempfile import TemporaryDirectory

from utils.json_processor import dict2namespace
from models.components.residual_encoder_decoder_sn import ResidualEncoderDecoderSn
from models.factory.component_serialization import save_component, load_component


class ResidualEncoderDecoderSnTests(unittest.TestCase):
    def setUp(self):
        self.model_config = dict2namespace(
            dict(
                type='residual_encoder_decoder_sn',
                levels=4,
                upsampling='transposed_convolution',
                dropout=0.25,
                filters=[2, 4, 8, 9, 9, 9, 8, 8, 4, 2],
                kernels=[5, 3, 3, 1, 1, 1, 3, 3, 3, 5],
                output_activation=None,
                power_iterations=3,
            )
        )
        self.test_inputs = tf.random.uniform((4, 70, 123, 3), dtype=tf.float32)

    def test_component(self):
        """
        Test component building, saving, loading and inference
        """
        # building
        model_creator = ResidualEncoderDecoderSn(
            self.model_config, 'test_residual_encoder_decoder_sn'
        )
        model = model_creator.create_model()

        # inference 1
        test_output_1 = model(self.test_inputs)
        self.assertEqual(test_output_1.shape, (4, 70, 123, 2))
        self.assertEqual(test_output_1.dtype, tf.float32)

        with TemporaryDirectory() as model_path:
            # saving
            save_component(model, self.model_config, model_path)
            self.assertTrue(isfile(join(model_path, 'model_weights.h5')))
            self.assertTrue(isfile(join(model_path, 'model_config.json')))
            self.assertTrue(isfile(join(model_path, 'model_diagram.png')))
            self.assertTrue(isfile(join(model_path, 'custom_objects.json')))

            # loading
            loaded_model = load_component(model_path)

            # inference 2
            test_output_2 = loaded_model(self.test_inputs)
            self.assertTrue(
                np.allclose(test_output_1.numpy(), test_output_2.numpy(), atol=1e-6)
            )
