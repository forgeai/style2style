import unittest
from os.path import join

from utils.tflite import is_tflite_compatible


class TfliteTests(unittest.TestCase):
    def setUp(self):
        self.test_models_location = join('projects', 'test_project')
        self.G_X2Y_path = join(
            self.test_models_location, 'cycle_gan_test', '1', 'checkpoints', 'G_X2Y'
        )
        self.conditional_style_net_path = join(
            self.test_models_location,
            'artistic_cin_gm_test',
            '1',
            'checkpoints',
            'style_net',
        )
        self.unconditional_style_net_path = join(
            self.test_models_location,
            'artistic_gm_test',
            '1',
            'checkpoints',
            'style_net',
        )

    def test_cycle_gan_component_conversion(self):
        self.assertTrue(is_tflite_compatible(self.G_X2Y_path))

    def test_artistic_gm_component_conversion(self):
        self.assertTrue(is_tflite_compatible(self.unconditional_style_net_path))

    def test_artistic_cin_gm_component_conversion(self):
        self.assertTrue(is_tflite_compatible(self.conditional_style_net_path))
