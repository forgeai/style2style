import unittest
import subprocess
from shutil import rmtree
from os.path import join, basename, isfile


class GemPolishingTests(unittest.TestCase):
    def setUp(self):
        self.polish_script = join('scripts', 'polish_gem.py')
        self.test_gems_path = join('apps', 'gems', 'test_gems')
        self.cycle_gan_path = join('projects', 'test_project', 'cycle_gan_test')
        self.cycle_noise_gan_path = join(
            'projects', 'test_project', 'cycle_noise_gan_test'
        )
        self.artistic_gm_path = join('projects', 'test_project', 'artistic_gm_test')
        self.artistic_noise_gm_path = join(
            'projects', 'test_project', 'artistic_noise_gm_test'
        )
        self.artistic_noise_gan_path = join(
            'projects', 'test_project', 'artistic_noise_gan_test'
        )
        self.artistic_cin_gm_path = join(
            'projects', 'test_project', 'artistic_cin_gm_test'
        )
        self.artistic_dual_cin_gm_path = join(
            'projects', 'test_project', 'artistic_dual_cin_gm_test'
        )

    def test_polish_cycle_gan_generator(self):
        proc_output = subprocess.Popen(
            [
                'python',
                self.polish_script,
                '-s',
                basename(self.test_gems_path),
                '-n',
                'cycle_gan_generator_gem',
                '-m',
                self.cycle_gan_path,
                '-c',
                'G_X2Y',
            ],
            shell=False,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        _, error = proc_output.communicate()
        return_code = proc_output.returncode

        if return_code != 0:
            print(error)
        self.assertEqual(return_code, 0)

        gem_path = join(self.test_gems_path, 'cycle_gan_generator_gem')
        self.assertTrue(isfile(join(gem_path, 'gem_config.json')))
        self.assertTrue(isfile(join(gem_path, 'weights', 'model_weights.h5')))
        self.assertTrue(isfile(join(gem_path, 'weights', 'model_config.json')))
        self.assertTrue(isfile(join(gem_path, 'weights', 'model_diagram.png')))
        self.assertTrue(isfile(join(gem_path, 'weights', 'custom_objects.json')))

    def test_polish_cycle_noise_gan_generator(self):
        proc_output = subprocess.Popen(
            [
                'python',
                self.polish_script,
                '-s',
                basename(self.test_gems_path),
                '-n',
                'cycle_noise_gan_generator_gem',
                '-m',
                self.cycle_noise_gan_path,
                '-c',
                'G_Y2X',
            ],
            shell=False,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        _, error = proc_output.communicate()
        return_code = proc_output.returncode

        if return_code != 0:
            print(error)
        self.assertEqual(return_code, 0)

        gem_path = join(self.test_gems_path, 'cycle_noise_gan_generator_gem')
        self.assertTrue(isfile(join(gem_path, 'gem_config.json')))
        self.assertTrue(isfile(join(gem_path, 'weights', 'model_weights.h5')))
        self.assertTrue(isfile(join(gem_path, 'weights', 'model_config.json')))
        self.assertTrue(isfile(join(gem_path, 'weights', 'model_diagram.png')))
        self.assertTrue(isfile(join(gem_path, 'weights', 'custom_objects.json')))

    def test_polish_artistic_gm_style_net(self):
        proc_output = subprocess.Popen(
            [
                'python',
                self.polish_script,
                '-s',
                basename(self.test_gems_path),
                '-n',
                'artistic_gm_style_net_gem',
                '-m',
                self.artistic_gm_path,
                '-c',
                'style_net',
            ],
            shell=False,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        _, error = proc_output.communicate()
        return_code = proc_output.returncode

        if return_code != 0:
            print(error)
        self.assertEqual(return_code, 0)

        gem_path = join(self.test_gems_path, 'artistic_gm_style_net_gem')
        self.assertTrue(isfile(join(gem_path, 'gem_config.json')))
        self.assertTrue(isfile(join(gem_path, 'weights', 'model_weights.h5')))
        self.assertTrue(isfile(join(gem_path, 'weights', 'model_config.json')))
        self.assertTrue(isfile(join(gem_path, 'weights', 'model_diagram.png')))
        self.assertTrue(isfile(join(gem_path, 'weights', 'custom_objects.json')))
        self.assertTrue(isfile(join(gem_path, 'style_images', '0_flowers.jpg')))

    def test_polish_artistic_noise_gm_style_net(self):
        proc_output = subprocess.Popen(
            [
                'python',
                self.polish_script,
                '-s',
                basename(self.test_gems_path),
                '-n',
                'artistic_noise_gm_style_net_gem',
                '-m',
                self.artistic_noise_gm_path,
                '-c',
                'style_net',
            ],
            shell=False,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        _, error = proc_output.communicate()
        return_code = proc_output.returncode

        if return_code != 0:
            print(error)
        self.assertEqual(return_code, 0)

        gem_path = join(self.test_gems_path, 'artistic_noise_gm_style_net_gem')
        self.assertTrue(isfile(join(gem_path, 'gem_config.json')))
        self.assertTrue(isfile(join(gem_path, 'weights', 'model_weights.h5')))
        self.assertTrue(isfile(join(gem_path, 'weights', 'model_config.json')))
        self.assertTrue(isfile(join(gem_path, 'weights', 'model_diagram.png')))
        self.assertTrue(isfile(join(gem_path, 'weights', 'custom_objects.json')))
        self.assertTrue(isfile(join(gem_path, 'style_images', '0_flowers.jpg')))
        self.assertTrue(
            isfile(join(gem_path, 'reconstructors', 'noise_reconstructor.npy'))
        )

    def test_polish_artistic_noise_gan_style_net(self):
        proc_output = subprocess.Popen(
            [
                'python',
                self.polish_script,
                '-s',
                basename(self.test_gems_path),
                '-n',
                'artistic_noise_gan_style_net_gem',
                '-m',
                self.artistic_noise_gan_path,
                '-c',
                'style_net',
            ],
            shell=False,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        _, error = proc_output.communicate()
        return_code = proc_output.returncode

        if return_code != 0:
            print(error)
        self.assertEqual(return_code, 0)

        gem_path = join(self.test_gems_path, 'artistic_noise_gan_style_net_gem')
        self.assertTrue(isfile(join(gem_path, 'gem_config.json')))
        self.assertTrue(isfile(join(gem_path, 'weights', 'model_weights.h5')))
        self.assertTrue(isfile(join(gem_path, 'weights', 'model_config.json')))
        self.assertTrue(isfile(join(gem_path, 'weights', 'model_diagram.png')))
        self.assertTrue(isfile(join(gem_path, 'weights', 'custom_objects.json')))
        self.assertTrue(isfile(join(gem_path, 'style_images', '0_flowers.jpg')))
        self.assertTrue(
            isfile(join(gem_path, 'reconstructors', 'noise_reconstructor.npy'))
        )

    def test_polish_artistic_cin_gm_style_net(self):
        proc_output = subprocess.Popen(
            [
                'python',
                self.polish_script,
                '-s',
                basename(self.test_gems_path),
                '-n',
                'artistic_cin_gm_style_net_gem',
                '-m',
                self.artistic_cin_gm_path,
                '-c',
                'style_net',
            ],
            shell=False,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        _, error = proc_output.communicate()
        return_code = proc_output.returncode

        if return_code != 0:
            print(error)
        self.assertEqual(return_code, 0)

        gem_path = join(self.test_gems_path, 'artistic_cin_gm_style_net_gem')
        self.assertTrue(isfile(join(gem_path, 'gem_config.json')))
        self.assertTrue(isfile(join(gem_path, 'weights', 'model_weights.h5')))
        self.assertTrue(isfile(join(gem_path, 'weights', 'model_config.json')))
        self.assertTrue(isfile(join(gem_path, 'weights', 'model_diagram.png')))
        self.assertTrue(isfile(join(gem_path, 'weights', 'custom_objects.json')))
        self.assertTrue(isfile(join(gem_path, 'style_images', '0_ship.jpg')))
        self.assertTrue(isfile(join(gem_path, 'style_images', '1_sun.jpg')))

    def test_polish_artistic_dual_cin_gm_style_net(self):
        proc_output = subprocess.Popen(
            [
                'python',
                self.polish_script,
                '-s',
                basename(self.test_gems_path),
                '-n',
                'artistic_dual_cin_gm_style_net_gem',
                '-m',
                self.artistic_dual_cin_gm_path,
                '-c',
                'style_net',
            ],
            shell=False,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        _, error = proc_output.communicate()
        return_code = proc_output.returncode

        if return_code != 0:
            print(error)
        self.assertEqual(return_code, 0)

        gem_path = join(self.test_gems_path, 'artistic_dual_cin_gm_style_net_gem')
        self.assertTrue(isfile(join(gem_path, 'gem_config.json')))
        self.assertTrue(isfile(join(gem_path, 'weights', 'model_weights.h5')))
        self.assertTrue(isfile(join(gem_path, 'weights', 'model_config.json')))
        self.assertTrue(isfile(join(gem_path, 'weights', 'model_diagram.png')))
        self.assertTrue(isfile(join(gem_path, 'weights', 'custom_objects.json')))
        self.assertTrue(isfile(join(gem_path, 'style_images', '0_ship.jpg')))
        self.assertTrue(isfile(join(gem_path, 'style_images', '1_sun.jpg')))

    def tearDown(self):
        rmtree(self.test_gems_path, ignore_errors=True)
