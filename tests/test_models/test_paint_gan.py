import unittest
from os.path import join
from shutil import rmtree
from argparse import Namespace

from train import main as run_training


class PaintGanTests(unittest.TestCase):
    def setUp(self):
        self.test_models_location = join('projects', 'test_project')
        self.new_model_config = join('tests', 'configs', 'model_paint_gan.json')
        self.existing_model_config = join(
            'projects', 'test_project', 'paint_gan_test', 'model_config.json'
        )
        self.training_config = join('tests', 'configs', 'training_paint_gan.json')
        self.data_config = join('tests', 'configs', 'data_paint_gan.json')

    def test_training_new_paint_gan(self):
        args = Namespace(
            model=self.new_model_config,
            training=self.training_config,
            data=self.data_config,
        )
        run_training(args)

    def test_retraining_existing_paint_gan(self):
        args = Namespace(
            model=self.existing_model_config,
            training=self.training_config,
            data=self.data_config,
        )
        run_training(args)

    def tearDown(self):
        # remove new model
        rmtree(join(self.test_models_location, 'paint_gan_test_new'), ignore_errors=True)
        # remove new version of the existing model
        for folder in ('2', 'cached_data'):
            rmtree(
                join(self.test_models_location, 'paint_gan_test', folder),
                ignore_errors=True,
            )
