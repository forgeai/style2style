import unittest
from shutil import rmtree
from itertools import product
from argparse import Namespace
from os import listdir
from os.path import join, isfile

from train import main as run_training
from predict import main as run_prediction


class CycleGanTests(unittest.TestCase):
    def setUp(self):
        self.test_models_location = join('projects', 'test_project')
        self.new_model_config = join('tests', 'configs', 'model_cycle_gan.json')
        self.existing_model_config = join(
            self.test_models_location, 'cycle_gan_test', 'model_config.json'
        )
        self.training_config = join('tests', 'configs', 'training_cycle_gan.json')
        self.data_config_range = join('tests', 'configs', 'data_cycle_gan_range.json')
        self.data_config_fixed = join('tests', 'configs', 'data_cycle_gan_fixed.json')
        self.G_X2Y_path = join(
            self.test_models_location, 'cycle_gan_test', '1', 'checkpoints', 'G_X2Y'
        )
        self.G_Y2X_path = self.G_X2Y_path.replace('G_X2Y', 'G_Y2X')

        test_data_path = join('data', 'original', 'test_data')
        self.samples_folder_path = join(test_data_path, 'test_wycinanki')
        self.sample_image_path = join(
            test_data_path, 'test_kaszubskie', '02_07_1_007.png'
        )
        self.data_dumps_path = join(test_data_path, 'test_dumps')

    def test_training_new_cycle_gan(self):
        args = Namespace(
            model=self.new_model_config,
            training=self.training_config,
            data=self.data_config_range,
        )
        run_training(args)

    def test_retraining_existing_cycle_gan(self):
        args = Namespace(
            model=self.existing_model_config,
            training=self.training_config,
            data=self.data_config_fixed,
        )
        run_training(args)

    def test_prediction_using_existing_G_X2Y(self):
        args = Namespace(
            model_path=self.G_X2Y_path,
            data_path=self.sample_image_path,
            condition_weights=None,
            saving_path=None,
            visualize=False,
        )
        run_prediction(args)

    def test_prediction_using_existing_G_Y2X(self):
        args = Namespace(
            model_path=self.G_Y2X_path,
            data_path=self.samples_folder_path,
            condition_weights=None,
            saving_path=self.data_dumps_path,
            visualize=False,
        )
        run_prediction(args)
        for image_path in listdir(self.samples_folder_path):
            self.assertTrue(isfile(join(self.data_dumps_path, image_path)))

    def tearDown(self):
        # remove new model
        rmtree(
            join(self.test_models_location, 'cycle_gan_test_new'), ignore_errors=True,
        )
        # remove new version of the existing model
        rmtree(
            join(self.test_models_location, 'cycle_gan_test', '2'), ignore_errors=True,
        )
        # remove processed data
        for data_name, data_stage, data_format in product(
            ('kaszubskie', 'wycinanki'),
            ('processed', 'tfrecords'),
            ('range', 'fixed_pad'),
        ):
            folder_name = 'test_{}_{{{}_75-128}}'.format(data_name, data_format)
            rmtree(join('data', data_stage, folder_name), ignore_errors=True)
        # remove predicted data
        rmtree(self.data_dumps_path, ignore_errors=True)
