import unittest
from shutil import rmtree
from argparse import Namespace
from os.path import join, isfile, basename

from train import main as run_training
from predict import main as run_prediction
from utils.json_processor import dict2namespace, json2namespace
from models.factory.component_serialization import load_component
from data_pipeline.factory.noise_generation import get_image_shape, generate_noise_dataset


class CycleNoiseGanTests(unittest.TestCase):
    def setUp(self):
        self.test_models_location = join('projects', 'test_project')
        self.new_model_config = join('tests', 'configs', 'model_cycle_noise_gan.json')
        self.existing_model_config = join(
            self.test_models_location, 'cycle_noise_gan_test', 'model_config.json'
        )
        self.training_config = join('tests', 'configs', 'training_cycle_noise_gan.json')
        self.data_config_perlin = join(
            'tests', 'configs', 'data_cycle_noise_gan_perlin.json'
        )
        self.data_config_simplex = join(
            'tests', 'configs', 'data_cycle_noise_gan_simplex.json'
        )
        self.G_X2Y_path = join(
            self.test_models_location, 'cycle_noise_gan_test', '1', 'checkpoints', 'G_X2Y'
        )
        self.G_Y2X_path = self.G_X2Y_path.replace('G_X2Y', 'G_Y2X')

        test_data_path = join('data', 'original', 'test_data')
        self.sample_image_path = join(
            test_data_path, 'test_kaszubskie', '02_07_1_007.png'
        )
        self.data_dumps_path = join(test_data_path, 'test_dumps')

    def test_training_new_cycle_noise_gan(self):
        args = Namespace(
            model=self.new_model_config,
            training=self.training_config,
            data=self.data_config_perlin,
        )
        run_training(args)

    def test_retraining_existing_cycle_noise_gan(self):
        args = Namespace(
            model=self.existing_model_config,
            training=self.training_config,
            data=self.data_config_simplex,
        )
        run_training(args)

    def test_prediction_using_existing_G_X2Y_to_noise(self):
        args = Namespace(
            model_path=self.G_X2Y_path,
            data_path=self.sample_image_path,
            condition_weights=None,
            saving_path=self.data_dumps_path,
            visualize=False,
        )
        run_prediction(args)
        self.assertTrue(
            isfile(join(self.data_dumps_path, basename(self.sample_image_path)))
        )

    def test_prediction_using_existing_G_Y2X_from_noise(self):
        model = load_component(self.G_Y2X_path)

        data_config = json2namespace(self.data_config_perlin)
        image_shape = get_image_shape(data_config)
        noise_config = dict2namespace(data_config.noise)
        noise_dataset = generate_noise_dataset(8, image_shape, noise_config)

        for sample in noise_dataset.batch(2):
            output_image = model(sample)
            self.assertEqual(output_image.shape, (2, 150, 150, 3))

    def tearDown(self):
        # remove new model
        rmtree(
            join(self.test_models_location, 'cycle_noise_gan_test_new'),
            ignore_errors=True,
        )
        # remove new version of the existing model
        rmtree(
            join(self.test_models_location, 'cycle_noise_gan_test', '2'),
            ignore_errors=True,
        )
        # remove processed data
        for data_stage in ('processed', 'tfrecords'):
            rmtree(
                join('data', data_stage, 'test_kaszubskie_{fixed_crop_150-150}'),
                ignore_errors=True,
            )
        # remove predicted data
        rmtree(self.data_dumps_path, ignore_errors=True)
