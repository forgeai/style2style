import unittest
from shutil import rmtree
from argparse import Namespace
from os.path import join, isfile, basename

from train import main as run_training
from predict import main as run_prediction


class ArtisticGmTests(unittest.TestCase):
    def setUp(self):
        self.test_models_location = join('projects', 'test_project')
        self.new_model_config = join('tests', 'configs', 'model_artistic_gm.json')
        self.existing_model_config = join(
            'projects', 'test_project', 'artistic_gm_test', 'model_config.json'
        )
        self.training_config = join('tests', 'configs', 'training_artistic_gm.json')
        self.data_config = join('tests', 'configs', 'data_artistic_gm.json')
        self.style_net_path = join(
            self.test_models_location,
            'artistic_gm_test',
            '1',
            'checkpoints',
            'style_net',
        )

        test_data_path = join('data', 'original', 'test_data')
        self.sample_image_path = join(test_data_path, 'test_wycinanki', '02_04_2_020.png')
        self.data_dumps_path = join(test_data_path, 'test_dumps')

    def test_training_new_artistic_gm(self):
        args = Namespace(
            model=self.new_model_config,
            training=self.training_config,
            data=self.data_config,
        )
        run_training(args)

    def test_retraining_existing_artistic_gm(self):
        args = Namespace(
            model=self.existing_model_config,
            training=self.training_config,
            data=self.data_config,
        )
        run_training(args)

    def test_prediction_using_existing_style_net(self):
        args = Namespace(
            model_path=self.style_net_path,
            data_path=self.sample_image_path,
            condition_weights=[0.5, 0.5],
            saving_path=self.data_dumps_path,
            visualize=False,
        )
        run_prediction(args)
        self.assertTrue(
            isfile(join(self.data_dumps_path, basename(self.sample_image_path)))
        )

    def tearDown(self):
        # remove new model
        rmtree(
            join(self.test_models_location, 'artistic_gm_test_new'), ignore_errors=True
        )
        # remove new version of the existing model
        for folder in ('2', 'cached_data'):
            rmtree(
                join(self.test_models_location, 'artistic_gm_test', folder),
                ignore_errors=True,
            )
        # remove processed data
        for data_stage in ('processed', 'tfrecords'):
            folder_name = 'test_wycinanki_{fixed_crop_128-128}'
            rmtree(join('data', data_stage, folder_name), ignore_errors=True)
        # remove predicted data
        rmtree(self.data_dumps_path, ignore_errors=True)
