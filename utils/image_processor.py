import numpy as np
import tensorflow as tf


def image2tensor(image):
    """
    Convert a regular image to a tensor by normalizing the image's
    [0..1] range to the GAN's friendly float32 [-1..1]

    Args:
        image (tf.Tensor): the image tensor
    Return:
        the [-1..1] image tensor (tf.Tensor)
    """
    return image * 2 - 1


def tensor2image(tensor):
    """
    Convert an image tensor to a regular image by normalizing the tensor's
    [-1..1] range to the standard float32 [0..1]

    Args:
        tensor (tf.Tensor): the image tensor
    Return:
        the [0..1] image (tf.Tensor)
    """
    return (tensor + 1) / 2


def float2uint8(image):
    """
    Convert an image from the float32 format [0..1] to the uint8 format [0..255]

    Args:
        image (numpy.array): the image content
    Return:
        the [0..255] image (numpy.array)
    """
    return (image * 255).astype(np.uint8)


def image_path_to_tensor(image_path):
    """
    Perform all steps for reading an image from the filesystem and
    turning it into a tensor

    Args:
        image_path (str): path to the image on the disk
    Return:
        [-1..1] image tensor (tf.Tensor)
    """
    image_file = tf.io.read_file(image_path)
    image = tf.image.decode_image(image_file)
    image = tf.image.convert_image_dtype(image, tf.float32)

    return image2tensor(image)
