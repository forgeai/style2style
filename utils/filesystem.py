import importlib
from os.path import split, basename


def get_basename(path):
    """
    Get the corrected name of the current directory regardless of
    whether a path separator was appended at the end or not

    Args:
        path (str): full path to the directory
    Return
        (str): the name of the base directory
    """
    split_path = split(path)
    if split_path[-1] == '':
        return basename(split_path[0])
    else:
        return split_path[-1]


def import_dynamically(module_path, entity_name):
    """
    Import a class, function or object dynamically

    Args:
        module_path (str): path to the module to import from where
                           nodes in the tree are separated by "."
        entity_name (str): name of the python object to import
    Return
        entity (object): the imported object
    """
    module = importlib.import_module(module_path)
    return getattr(module, entity_name)
