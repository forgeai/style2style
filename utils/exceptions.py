from abc import ABCMeta


class BaseCustomException(Exception, metaclass=ABCMeta):
    """
    Base Exception for this project
    Every custom exception should inherit from this
    """

    def __init__(self, message):
        super().__init__(message)


class ModelAlreadyExistsException(BaseCustomException):
    """
    Exception to be raised when a model with the same name already exists
    in the project
    """

    def __init__(self, model_name, project_name):
        message = f'Model "{model_name}" already exists in project "{project_name}"'
        super().__init__(message)


class ModelTypeDoesNotExistException(BaseCustomException):
    """
    Exception to be raised when the requested model type does not exist
    """

    def __init__(self, model_type):
        message = (
            f'Model type "{model_type}" does not exist or '
            + 'it was not registered in the lookups'
        )
        super().__init__(message)


class InvalidAppModeException(BaseCustomException):
    """
    Exception to be raised when an unsuitable model is used in an app
    """

    def __init__(self, message):
        super().__init__(message)


class InvalidImageFormatError(Exception):
    pass
