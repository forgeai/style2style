import cv2
import pygame
import numpy as np
import tensorflow as tf
from os import listdir
from os.path import join
from scipy import signal
from collections import deque

from models.factory.component_serialization import load_component
from models.gans.paint_gan.toolset import get_labels_mask
from models.gans.artistic_base_gan.model import ArtisticBaseGan
from models.gans.artistic_base_gan.toolset import (
    load_style_images,
    load_noise_reconstructor,
)
from utils.constants import EPSILON
from utils.json_processor import json2namespace
from utils.exceptions import ModelTypeDoesNotExistException
from utils.image_processor import image2tensor, tensor2image, float2uint8


def interpolate_colours(colour_1, colour_2, alpha):
    """
    Create a new colour as a result of a weighted linear interpolation
    between two other colours

    Args:
        colour_1 (tuple): RGB values for the first colour
        colour_2 (tuple): RGB values for the second colour
        alpha (float): the amount of colour_1 to keep
    Return:
        interpolated_colour (tuple): RGB values for the output colour
    """
    assert 0 <= alpha <= 1, 'Invalid alpha in colour interpolation'
    return [int(alpha * c1 + (1 - alpha) * c2) for c1, c2 in zip(colour_1, colour_2)]


def create_gaussian_kernel(size, sigma):
    """
    Create a 2d Gaussian kernel over a square image patch where the maximum value is 1

    Args:
        size (int): the size of the square image patch
        sigma (float): the standard deviation for the Gaussian
    Return:
        kernel (np.array): the Gaussian kernel
    """
    kernel_1d = signal.gaussian(size, std=sigma).reshape(size, 1)
    kernel_2d = np.outer(kernel_1d, kernel_1d)
    kernel = (kernel_2d / kernel_2d.max()).astype(np.float32)
    return kernel


def create_empty_thumbnail(shape):
    """
    Create a black thumbnail tensor representing an empty spot

    Args:
        shape (tuple): the shape of the image thumbnail
    Return:
        thumbnail (tf.Tensor)
    """
    return -1.0 * tf.ones(shape, dtype=np.float32)


def grayscale_to_pygame_frame(np_array):
    """
    Process a single chanel numpy array to obtain a pygame surface

    Args:
        np_array (np.array): has rank 2 (height, width) and values in [0..1]
    Return:
        frame (pygame.Surface)
    """
    frame = float2uint8(np_array)
    frame = np.repeat(np.expand_dims(frame, axis=-1), 3, axis=-1)
    return pygame.surfarray.make_surface(frame)


def numpy_to_pygame_frame(np_array):
    """
    Process a numpy array to obtain a pygame surface

    Args:
        np_array (np.array): has rank 3 (height, width, channel) and values in [-1..1]
    Return:
        frame (pygame.Surface)
    """
    frame = tensor2image(np_array)
    frame = float2uint8(frame)
    return pygame.surfarray.make_surface(frame)


def tensor_to_pygame_frame(tensor):
    """
    Process a tensor to obtain a pygame surface

    Args:
        tensor (tf.Tensor): has rank 4 (batch, height, width, channel)
    Return:
        frame (pygame.Surface)
    """
    return numpy_to_pygame_frame(tensor.numpy()[0])


def opencv_frame_to_prediction_ready_tensor(frame):
    """
    Process a frame obtained from the opencv camera to obtain an input Tensor
    for the models

    Args:
        frame (cv2.Mat)
    Return:
        tensor (tf.Tensor)
    """
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    frame = np.rot90(frame)
    frame = tf.image.convert_image_dtype(frame, dtype=tf.float32)
    frame = image2tensor(frame)
    return tf.expand_dims(frame, axis=0)


def opencv_frame_to_pygame_frame(frame, mirror=True):
    """
    Process a frame obtained from the opencv camera to obtain a pygame surface

    Args:
        frame (cv2.Mat)
        mirror (bool)
    Return:
        frame (pygame.Surface)
    """
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    frame = np.rot90(frame)
    frame = pygame.surfarray.make_surface(frame)
    if not mirror:
        frame = pygame.transform.flip(frame, True, False)
    return frame


def canvas_frame_to_pygame_frame(frame):
    """
    Process a frame obtained from a canvas paint prediction to get a pygame surface

    Args:
        frame (tf.Tensor)
    Return:
        frame (pygame.Surface)
    """
    frame = tensor_to_pygame_frame(frame)
    frame = pygame.transform.rotate(frame, 90)
    return pygame.transform.flip(frame, False, True)


def spray_frame_to_pygame_frame(frame):
    """
    Process a frame obtained from a temporary canvas spray to get a pygame surface

    Args:
        frame (np.array)
    Return:
        frame (pygame.Surface)
    """
    frame = float2uint8(frame)
    frame = pygame.surfarray.make_surface(frame)
    frame = pygame.transform.rotate(frame, 90)
    frame = pygame.transform.flip(frame, False, True)
    frame.set_alpha(70)
    return frame


def live_thumbnail_tensor_to_pygame_surface(thumbnail):
    """
    Process a style image tensor to obtain a pygame surface patch
    Only useful for the live app

    Args:
        thumbnail (tf.Tensor)
    Return:
        surface (pygame.Surface)
    """
    surface = tensor2image(thumbnail)
    surface = surface.numpy()
    surface = np.rot90(surface)
    surface = np.flip(surface, axis=0)
    surface = float2uint8(surface)
    return pygame.surfarray.make_surface(surface)


def painter_thumbnail_image_to_pygame_surface(thumbnail, size):
    """
    Process a style image tensor to obtain a pygame surface patch
    Only useful for the painter app

    Args:
        thumbnail (tf.Tensor)
        size (int)
    Return:
        surface (pygame.Surface)
    """
    surface = cv2.resize(thumbnail, (size, size))
    surface = np.rot90(surface)
    surface = np.flip(surface, axis=0)
    return pygame.surfarray.make_surface(surface)


def load_thumbnails(thumbnails_path):
    """
    Load the thumbnails from disk

    Args:
        thumbnails_path (str): path to the thumbnails folder
    """
    thumbnails = {}
    for image_name in listdir(thumbnails_path):
        image_path = join(thumbnails_path, image_name)
        thumbnails[image_name[:-4]] = cv2.imread(image_path, cv2.IMREAD_COLOR)

    return thumbnails


def load_gem_setup(gem_path):
    """
    Set up the model based on its type and return it together with other
    auxiliaries like a dict of thumbnail images if used for multistyling
    or noise reconstructors if used for pattern generation from noise

    Args:
        gem_path (str): path to the model folder
    Return:
        gem_setup (dict): contains the app mode, model and other auxiliaries
    """
    model_type = json2namespace(join(gem_path, 'gem_config.json')).model_type
    gem_setup = dict(model=load_component(join(gem_path, 'weights')))

    if model_type == 'cycle_gan':
        gem_setup['mode'] = 'single'

    elif model_type == 'cycle_noise_gan':
        gem_setup['mode'] = 'single'

    elif model_type == 'artistic_gm':
        gem_setup['mode'] = 'single'
        gem_setup['style_images'] = load_style_images(join(gem_path, 'style_images'))

    elif model_type == 'artistic_noise_gm':
        gem_setup['mode'] = 'single'
        gem_setup['style_images'] = load_style_images(join(gem_path, 'style_images'))
        gem_setup['reconstructor'] = load_noise_reconstructor(
            join(gem_path, 'reconstructors')
        )

    elif model_type == 'artistic_noise_gan':
        gem_setup['mode'] = 'single'
        gem_setup['style_images'] = load_style_images(join(gem_path, 'style_images'))
        gem_setup['reconstructor'] = load_noise_reconstructor(
            join(gem_path, 'reconstructors')
        )

    elif model_type == 'artistic_cin_gm':
        gem_setup['mode'] = 'multi'
        gem_setup['style_images'] = load_style_images(join(gem_path, 'style_images'))

    elif model_type == 'artistic_dual_cin_gm':
        gem_setup['mode'] = 'dual'
        gem_setup['style_images'] = load_style_images(join(gem_path, 'style_images'))

    elif model_type == 'paint_gan':
        gem_setup['mode'] = 'single'
        gem_setup['thumbnails'] = load_thumbnails(
            join(gem_path, 'painting_set', 'thumbnails')
        )
        gem_setup['reconstructor'] = load_noise_reconstructor(
            join(gem_path, 'reconstructors')
        )
        gem_setup['mask'] = get_labels_mask(join(gem_path, 'painting_set', 'label.png'))

    else:
        raise ModelTypeDoesNotExistException(model_type)

    return gem_setup


def load_image_to_pygame(image_path, shape):
    """
    Load a png image as a pygame surface

    Args:
        image_path (str): path to the png image on disk
        shape (int or tuple of 2 ints): the size (in pixels) of the image
    Return:
        image (pygame.Surface)
    """
    if isinstance(shape, int):
        shape = (shape, shape)
    image = pygame.image.load(image_path).convert_alpha()
    return pygame.transform.scale(image, shape)


class StylePointer:
    """
    Manages the pointer used for selecting or combining multiple styles

    Args:
        width (int): the width of the whole frame
        height (int): the height of the whole frame
        tn_width (int): the width of a style thumbnail
        tn_height (int): the height of a style thumbnail
        num_styles (int): total number of styles
        is_dual (bool): whether it can also control the styling-content trade-off
        x (int): initial position on the OX axis
        y (int): initial position on the OY axis
        z (float): the amount of styling (values in range 0..1) where 1.0 is max styling
    """

    def __init__(
        self,
        width,
        height,
        tn_width,
        tn_height,
        num_styles,
        is_dual=True,
        x=0,
        y=0,
        z=1.0,
    ):
        self.x = x
        self.y = y
        self.z = z
        self.width = width
        self.height = height
        self.tn_width = tn_width
        self.tn_height = tn_height

        self.X13 = tn_width // 2
        self.X24 = self.width - self.tn_width // 2
        self.Y12 = tn_height // 2
        self.Y34 = self.height - self.tn_height // 2

        self.is_dual = is_dual
        self.num_actual_styles = num_styles
        self.max_z_value = 1.0

        if is_dual:
            num_condition_weights = 2 * num_styles
            self.min_z_value = 0
        else:
            num_condition_weights = num_styles
            self.min_z_value = ArtisticBaseGan.EMPTY_STYLE_VALUE

        self.sec2idx = {x + 1: x for x in range(min(4, self.num_actual_styles))}
        self.condition_weights = np.zeros((num_condition_weights,), dtype=np.float32)
        self.update_condition_weights()

    def __str__(self):
        return 'x = {}, y = {}, z = {}'.format(self.x, self.y, self.z)

    def get_section(self):
        """
        Localizes the 2d pointer in 1 of the 9 sections that describe its behaviour

        Return:
            section (tuple of 2 ints): values from (0, 0) to (2, 2)
        """
        section = [None, None]

        if self.x < self.tn_width:
            section[0] = 0
        elif self.x < self.width - self.tn_width:
            section[0] = 1
        else:
            section[0] = 2
        if self.y < self.tn_height:
            section[1] = 0
        elif self.y < self.height - self.tn_height:
            section[1] = 1
        else:
            section[1] = 2

        return tuple(section)

    def get_current_valid_style_slot(self):
        """
        Return the slot of the currently selected style
        If no style is selected return None

        Return:
            style_slot (int) / None
        """
        section = self.get_section()

        if section == (0, 0):
            return 1
        elif section == (2, 0) and self.num_actual_styles > 1:
            return 2
        elif section == (0, 2) and self.num_actual_styles > 2:
            return 3
        elif section == (2, 2) and self.num_actual_styles > 3:
            return 4

        return None

    def update_x(self, increment):
        """
        Update the location of the pointer on the horizontal axis

        Args:
            increment (int): number of pixels to move the pointer
        """
        section = self.get_section()

        if increment > 0:  # move to the right
            if section[0] == 0:
                self.x = self.tn_width + increment
            elif section[0] == 1:
                self.x += increment
            else:
                self.x = self.X24
        elif increment < 0:  # move to the left
            if section[0] == 2:
                self.x = self.width - self.tn_width + increment
            elif section[0] == 1:
                self.x += increment
            else:
                self.x = self.X13

    def update_y(self, increment):
        """
        Update the location of the pointer on the vertical axis

        Args:
            increment (int): number of pixels to move the pointer
        """
        section = self.get_section()

        if increment > 0:  # move down
            if section[1] == 0:
                self.y = self.tn_height + increment
            elif section[1] == 1:
                self.y += increment
            else:
                self.y = self.Y34
        elif increment < 0:  # move up
            if section[1] == 2:
                self.y = self.height - self.tn_height + increment
            elif section[1] == 1:
                self.y += increment
            else:
                self.y = self.Y12

    def update_z(self, increment):
        """
        Update the intensity of the styling

        Args:
            increment (float): intensity increment
        """
        self.z += increment
        self.z = min(max(self.z, self.min_z_value), self.max_z_value)

    def update_sec2idx(self, slot2idx):
        """
        Update the dict that shows which styles are in play

        Args:
            slot2idx (dict): the updated version
        """
        self.sec2idx = slot2idx

    def get_condition_weights(self):
        """
        Return the condition weights

        Return:
            condition weights (np.array): the style conditioning input
        """
        return self.condition_weights

    def update_condition_weights(self):
        """
        Update the value of the condition weights depending on the position and
        intensity of the style pointer
        """
        dist = lambda x1, y1, x2, y2: np.linalg.norm(
            np.asarray([x1, y1]) - np.asarray([x2, y2])
        )

        def get_weights_distribution(distances):
            reverse_distances = [1.0 / (d + EPSILON) for d in distances]
            reverse_distances_sum = sum(reverse_distances)
            return [rd / reverse_distances_sum for rd in reverse_distances]

        section = self.get_section()
        weights_distribution = np.zeros((self.num_actual_styles,), dtype=np.float32)

        # single styles
        if section == (0, 0):  # style 1
            weights_distribution[self.sec2idx[1]] = 1.0
        elif section == (2, 0):  # style 2
            if self.num_actual_styles > 1:
                weights_distribution[self.sec2idx[2]] = 1.0
            else:
                weights_distribution += ArtisticBaseGan.EMPTY_STYLE_VALUE
        elif section == (0, 2):  # style 3
            if self.num_actual_styles > 2:
                weights_distribution[self.sec2idx[3]] = 1.0
            else:
                weights_distribution += ArtisticBaseGan.EMPTY_STYLE_VALUE
        elif section == (2, 2):  # style 4
            if self.num_actual_styles > 3:
                weights_distribution[self.sec2idx[4]] = 1.0
            else:
                weights_distribution += ArtisticBaseGan.EMPTY_STYLE_VALUE

        # double styles
        elif section == (0, 1):  # styles 1 and 3
            if self.num_actual_styles < 3:  # style 3 does not exist
                weights_distribution[self.sec2idx[1]] = 1.0
            else:
                d1 = dist(self.x, self.y, self.X13, self.Y12)
                d3 = dist(self.x, self.y, self.X13, self.Y34)
                [w1, w3] = get_weights_distribution([d1, d3])
                weights_distribution[self.sec2idx[1]] = w1
                weights_distribution[self.sec2idx[3]] = w3

        elif section == (1, 0):  # styles 1 and 2
            if self.num_actual_styles < 2:  # style 2 does not exist
                weights_distribution[self.sec2idx[1]] = 1.0
            else:
                d1 = dist(self.x, self.y, self.X13, self.Y12)
                d2 = dist(self.x, self.y, self.X24, self.Y12)
                [w1, w2] = get_weights_distribution([d1, d2])
                weights_distribution[self.sec2idx[1]] = w1
                weights_distribution[self.sec2idx[2]] = w2

        elif section == (2, 1):  # styles 2 and 4
            if self.num_actual_styles < 2:  # style 2 does not exist
                weights_distribution += ArtisticBaseGan.EMPTY_STYLE_VALUE
            elif self.num_actual_styles < 4:  # style 4 does not exist
                weights_distribution[self.sec2idx[2]] = 1.0
            else:
                d2 = dist(self.x, self.y, self.X24, self.Y12)
                d4 = dist(self.x, self.y, self.X24, self.Y34)
                [w2, w4] = get_weights_distribution([d2, d4])
                weights_distribution[self.sec2idx[2]] = w2
                weights_distribution[self.sec2idx[4]] = w4

        elif section == (1, 2):  # styles 3 and 4
            if self.num_actual_styles < 3:  # style 3 does not exist
                weights_distribution += ArtisticBaseGan.EMPTY_STYLE_VALUE
            elif self.num_actual_styles < 4:  # style 4 does not exist
                weights_distribution[self.sec2idx[3]] = 1.0
            else:
                d3 = dist(self.x, self.y, self.X13, self.Y34)
                d4 = dist(self.x, self.y, self.X24, self.Y34)
                [w3, w4] = get_weights_distribution([d3, d4])
                weights_distribution[self.sec2idx[3]] = w3
                weights_distribution[self.sec2idx[4]] = w4

        else:  # a combination of all existing styles
            if self.num_actual_styles < 2:  # style 2 does not exist
                weights_distribution[self.sec2idx[1]] = 1.0
            elif self.num_actual_styles < 3:  # style 3 does not exist
                d1 = dist(self.x, self.y, self.X13, self.Y12)
                d2 = dist(self.x, self.y, self.X24, self.Y12)
                [w1, w2] = get_weights_distribution([d1, d2])
                weights_distribution[self.sec2idx[1]] = w1
                weights_distribution[self.sec2idx[2]] = w2
            elif self.num_actual_styles < 4:  # style 4 does not exist
                d1 = dist(self.x, self.y, self.X13, self.Y12)
                d2 = dist(self.x, self.y, self.X24, self.Y12)
                d3 = dist(self.x, self.y, self.X13, self.Y34)
                [w1, w2, w3] = get_weights_distribution([d1, d2, d3])
                weights_distribution[self.sec2idx[1]] = w1
                weights_distribution[self.sec2idx[2]] = w2
                weights_distribution[self.sec2idx[3]] = w3
            else:  # all styles are in play :)
                d1 = dist(self.x, self.y, self.X13, self.Y12)
                d2 = dist(self.x, self.y, self.X24, self.Y12)
                d3 = dist(self.x, self.y, self.X13, self.Y34)
                d4 = dist(self.x, self.y, self.X24, self.Y34)
                [w1, w2, w3, w4] = get_weights_distribution([d1, d2, d3, d4])
                weights_distribution[self.sec2idx[1]] = w1
                weights_distribution[self.sec2idx[2]] = w2
                weights_distribution[self.sec2idx[3]] = w3
                weights_distribution[self.sec2idx[4]] = w4

        if self.is_dual:
            self.condition_weights[: self.num_actual_styles] = (
                1.0 - self.z
            ) * weights_distribution
            self.condition_weights[self.num_actual_styles :] = (
                self.z * weights_distribution
            )
        else:
            self.condition_weights = self.z * weights_distribution


class StyleSwitchCircularBuffer:
    """
    Switches between the styles when selecting which styles to change places

    Args:
        num_styles (int): total number of styles
        current_style (int): index of the current style
    """

    def __init__(self, num_styles, current_style):
        self.index_buffer = deque(filter(lambda x: x != current_style, range(num_styles)))
        while len(self.index_buffer) < 3:
            self.index_buffer.append(None)

    def current(self):
        """
        Get currently selected style index

        Return:
            index (int)
        """
        return self.index_buffer[0]

    def prev(self):
        """
        Get the previous style index from the buffer

        Return:
            index (int)
        """
        return self.index_buffer[-1]

    def next(self):
        """
        Get the next style index from the buffer

        Return:
            index (int)
        """
        return self.index_buffer[1]

    def move_right(self):
        """
        Rotate the buffer to the left
        """
        self.index_buffer.rotate(-1)
        while self.current() is None:
            self.index_buffer.rotate(-1)

    def move_left(self):
        """
        Rotate the buffer to the right
        """
        self.index_buffer.rotate(1)
        while self.current() is None:
            self.index_buffer.rotate(1)


class Slider:
    """
    Manages the value and position of a slider

    Args:
        min_value (float): lower bound on the slider value
        max_value (float): upper bound on the slider value
        min_position (int): pixel position in UI when the value is minimum
        max_position (int): pixel position in UI when the value is maximum
        activation (float): initial position of the slider activation

    Attributes:
        position (float): specifies the slider pixel position in the UI
        value (float): specifies the actual value expressed by the slider
        activation (float): specifies the slider value in the normalized interval 0..1;
                            it is considered to be the ground truth of the current state
                            of the slider
        lambda_function (callable): the function that gets applied to the linear slider
    """

    def __init__(
        self,
        min_value,
        max_value,
        min_position,
        max_position,
        activation=0.5,
        lambda_function=lambda x: x,
    ):
        assert min_value < max_value, 'Invalid Slider bounds initializers for value'
        self.min_value = min_value
        self.max_value = max_value
        self.value_interval = self.max_value - self.min_value

        assert min_position < max_position, 'Invalid Slider bounds initializers for UI'
        self.min_position = min_position
        self.max_position = max_position
        self.position_interval = self.max_position - self.min_position

        assert 0 <= activation <= 1, 'Invalid Slider position initializer'
        self.activation = activation
        self._update_value()
        self._update_position()

        assert callable(lambda_function), 'Invalid Slider lambda function'
        self.lambda_function = lambda_function

    def __call__(self):
        """
        Retrieve the current value after applying the lambda function

        Return:
            value (float)
        """
        return self.lambda_function(self.value)

    def get_value(self):
        """
        Retrieve the current value

        Return:
            value (float)
        """
        return self.value

    def get_position(self):
        """
        Retrieve the current position

        Return:
            position (float)
        """
        return self.position

    def get_activation(self):
        """
        Retrieve the current activation

        Return:
            activation (float)
        """
        return self.activation

    def set_value(self, value):
        """
        Set the value of the slider, but keep it in the valid interval
        """
        self.value = min(max(value, self.min_value), self.max_value)
        self.activation = (self.value - self.min_value) / self.value_interval
        self._update_position()

    def set_position(self, position):
        """
        Set the position of the slider, but keep it in the valid interval
        """
        self.position = min(max(position, self.min_position), self.max_position)
        self.activation = (self.position - self.min_position) / self.position_interval
        self._update_value()

    def _update_value(self):
        """
        Update the value of the slider based on the activation
        """
        self.value = self.min_value + self.activation * self.value_interval

    def _update_position(self):
        """
        Update the position of the slider based on the activation
        """
        self.position = self.min_position + int(self.activation * self.position_interval)
