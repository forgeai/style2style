EPSILON = 1e-8
SN_POWER_ITERATIONS = 5

# ---------------------------------------------------------------------------------------#

PAINTING_SIGMA = 0.5
PAINTING_ROW_SEATS = 3
PAINTING_THUMBNAIL_SHAPE = (128, 128)

# ---------------------------------------------------------------------------------------#

IMAGE_EXTENSIONS = ('.jpg', '.jpeg', '.png')
OPTIMIZERS = ('sgd', 'adam', 'rmsprop')
OBJECTIVES = ('mean_squared_error', 'binary_crossentropy', 'wasserstein_distance')
UPSAMPLING_METHODS = ('transposed_convolution', 'nearest', 'bilinear')

# ---------------------------------------------------------------------------------------#

NON_CONDITIONAL_COMPONENTS = (
    'unet',
    'unet_in',
    'unet_sn',
    'convolutional',
    'convolutional_in',
    'convolutional_sn',
    'residual_encoder_decoder',
    'residual_encoder_decoder_in',
    'residual_encoder_decoder_sn',
)
CONDITIONAL_COMPONENTS = (
    'unet_cin',
    'unet_cdn',
    'convolutional_cin',
    'convolutional_cdn',
    'residual_encoder_decoder_cin',
    'residual_encoder_decoder_cdn',
)
ARTISTIC_STYLE_MODELS = (
    'artistic_gm',
    'artistic_noise_gm',
    'artistic_noise_gan',
    'artistic_cin_gm',
    'artistic_dual_cin_gm',
)
CYCLE_MODELS = (
    'cycle_gan',
    'cycle_noise_gan',
)
PAINT_MODELS = ('paint_gan',)
NOISE_MODELS = (
    'cycle_noise_gan',
    'artistic_noise_gm',
    'artistic_noise_gan',
)
