import numpy as np
import tensorflow as tf
import tensorflow.keras as keras

from models.factory.component_serialization import load_component


def create_input_configured_model(model, image_shape):
    """
    Configure the input shape for the model by wrapping the current model with
    an explicit width and height of the expected image inputs, and return the
    configured model

    Args:
        model (tf.keras.Model): the model to configure
        image_shape (tuple of 2 ints): the height and width of the input image
    Return:
        model (tf.keras.Model): the configured model
    """
    # non-conditional models
    if isinstance(model.input_shape, tuple):
        assert model.input_shape == (None, None, None, 3), 'Invalid input image shape'

        new_inputs = keras.layers.Input(shape=(*image_shape, 3))
        new_outputs = model(new_inputs)

    # conditional models
    elif isinstance(model.input_shape, list):
        assert len(model.input_shape) == 2, 'Unrecognized model input format'
        assert model.input_shape[0] == (None, None, None, 3), 'Invalid input image shape'
        assert len(model.input_shape[1]) == 2, 'Invalid input conditional shape'
        assert model.input_shape[1][0] is None, 'Invalid input conditional shape'

        new_inputs = [
            keras.layers.Input(shape=(*image_shape, 3)),
            keras.layers.Input(shape=(model.input_shape[1][1],)),
        ]
        new_outputs = model(new_inputs)

    # unrecognized models
    else:
        raise NotImplementedError

    return keras.Model(inputs=new_inputs, outputs=new_outputs)


def convert_model(model, input_shape):
    """
    Convert a keras model to a tflite model

    Args:
        model (tf.keras.Model): the model to convert
        input_shape (tuple of 2 ints): the static input shape the
                                       tflite model will expect
    Return:
        tflite_model: the converted model in serialized format
    """
    configured_model = create_input_configured_model(model, input_shape)
    converter = tf.lite.TFLiteConverter.from_keras_model(configured_model)
    converter.allow_custom_ops = True

    tflite_model = converter.convert()
    return tflite_model


def is_tflite_compatible(model_path, numerical_test=False):
    """
    Test the compatibility of a saved keras model to be converted
    to a tflite model

    Args:
        model_path (str): path to the model checkpoints folder
        numerical_test (bool): whether to test if the converted model
                               gives a numerically close response to
                               the initial model for a random input
    Return:
        True if compatible, some exception otherwise
    """
    model = load_component(model_path)
    tflite_model = convert_model(model, (128, 128))

    interpreter = tf.lite.Interpreter(model_content=tflite_model)
    interpreter.allocate_tensors()

    if not numerical_test:
        return True

    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()

    input_shape = input_details[0]['shape']
    input_data = np.array(np.random.random_sample(input_shape), dtype=np.float32)
    interpreter.set_tensor(input_details[0]['index'], input_data)
    interpreter.invoke()

    tflite_results = interpreter.get_tensor(output_details[0]['index'])
    tf_results = model(tf.constant(input_data))

    for tf_result, tflite_result in zip(tf_results, tflite_results):
        np.testing.assert_almost_equal(tf_result, tflite_result, decimal=3)

    return True
